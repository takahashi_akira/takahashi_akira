﻿
class ServerStartUIController {


    /**
     * 初期化処理
     */
    public static Initialize() {

        //  イベントの設定
        Util.StopPropagation();

        //
        document.getElementById('skybezie-exec-forcestart').onclick = ((e) => { this.ForceStart(); });
        //
        document.getElementById('skybezie-new-room').onclick = ((e) => { this.NewRoom(); });
        document.getElementById('skybezie-navi-new-room').onclick = ((e) => { this.NewRoom(); });

        document.getElementById('room-title').onkeypress = ((e) => {
            if (e.keyCode == 13)
                this.NewRoom();
        });
        //
        document.getElementById('skybezie-exec-continue').onclick = ((e) => { this.Continue(); });
        document.getElementById('skybezie-navi-exec-continue').onclick = ((e) => { this.Continue(); });
        //
        document.getElementById('skybezie-show-loader').onclick = ((e) => {
            //  ファイル選択ダイアログを開きます
            document.getElementById('skybezie-import-file').click();
        });

        document.getElementById('skybezie-navi-show-loader').onclick = ((e) => {
            //  ファイル選択ダイアログを開きます
            document.getElementById('skybezie-import-file').click();
        });

        //  ファイル選択時のイベント
        document.getElementById('skybezie-import-file').onchange = ((e) => { this.ImportFileChange(e); });

        //  ドラック＆ドロップ
        var target = document.getElementById('skybezie-body');
        target.addEventListener('dragover', this.dragOverHandler);
        target.addEventListener('drop', this.dropHandler);


        //  クライアントの同時起動チェックボックス
        var checkbox = document.getElementById('check-bootwithclient') as HTMLInputElement;
        checkbox.checked = LocalCache.BootWithClient;

        //  チェック変更時イベント
        checkbox.onchange = ((e) => {
            LocalCache.BootWithClient = checkbox.checked;
        });

        if (LocalCache.BootServerID) {
            //  多重起動確認
            WebRTCService.Start(new ServerStartController(), LocalCache.BootServerID, "Multiple start-up check");
        }
        else {
            this.InitializeMain();
        }
    }


    /**
     * ファイルのドロップオーバー時イベント
     * @param event
     */
    private static dragOverHandler(event) {
        event.preventDefault();
        event.dataTransfer.dropEffect = 'copy';
    }


    /**
     * ファイルのドラック時イベント
     * @param event
     */
    private static dropHandler(event) {
        event.stopPropagation();
        event.preventDefault();
        var files = event.dataTransfer.files;
        Array.prototype.forEach.call(files, function (file) {
            var reader = new FileReader();
            reader.addEventListener('load', function (event) {

                var target: any = event.currentTarget;
                var data: any = JSON.parse(target.result);
                ServerStartUIController.Import(data);

            });
            reader.readAsText(file);
        });
    }


    /**
     * 多重起動エラー
     */
    public static MultiBootError() {
        document.getElementById('singpost-forceboot').hidden = true;

        $('#error-message').text("SkyBezieは起動済みです。多重起動はできません。");
        document.getElementById('singpost-booterror').hidden = false;
    }


    /**
     * 強制起動
     * ローカルキャッシュをクリアして強制起動
     */
    public static ForceStart() {
        LocalCache.BootServerID = "";
        this.InitializeMain();
    }


    /**
     * 起動画面の表示
     */
    public static InitializeMain() {
        document.getElementById('singpost-boot').hidden = true;
        document.getElementById('singpost-start').hidden = false;
    }


    /**
     * 新しい部屋を作成
     */
    public static NewRoom() {

        var db = new SkyBezieDB();

        db.Remove(() => {

            LocalCache.RoomName = $('#room-title').val();
            this.Continue();
        });
    }


    /**
     * 前回の続きを再開
     */
    public static Continue() {
        location.href = "../Server";
    }


    /**
     * ファイル取込処理
     * @param event
     */
    public static ImportFileChange(event: any) {

        var files: FileList = event.currentTarget.files;

        var reader = new FileReader();
        reader.readAsText(files[0]);

        var data: any;

        reader.onload = (ev) => {
            data = JSON.parse(reader.result);
        }

        reader.onloadend = (ev) => {
            this.Import(data);
        }
    }


    /**
     * データ読込と再起動
     * @param data
     */
    public static Import(data: any) {

        var db = new SkyBezieDB();

        //  古いDBを削除
        db.Remove(() => {

            //  新しいDBを再構築
            var newdb = new SkyBezieDB();
            newdb.Create(() => {

                //  ファイルを読み込んでデータをセットする
                newdb.WriteAllData(data, () => {

                    LocalCache.RoomName = data.Room.title;

                    //  サーバー起動
                    this.Continue();
                });
            });
        });
    }

}