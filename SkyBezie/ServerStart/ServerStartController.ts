﻿
class ServerStartController extends ServiceController {


    constructor() {
        super();
        this.Receiver = new ServiceReceiver(this);
    };


    /**
     * LocalCacheに保持されたBootIDに接続できた場合
     * サーバーは起動済みと判断して起動できないようにする
     */
    public OnOwnerConnection() {
        ServerStartUIController.MultiBootError();
    }

};
