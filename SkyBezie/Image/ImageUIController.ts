﻿
interface OnDropImage { (file: File, src): void }

class ImageUIController {

    private static _imageRec: ImageRec;
    private static _callback: OnChangeImage;

    /**
     * 初期化処理
     */
    public static Initialize() {

        Util.StopPropagation();

        this._imageRec = new ImageRec(ImageType.Wallpaper);

        //  イベント設定
        $('#size-option-auto').click(() => { this._imageRec.backgroundsize = "auto"; this.Refresh() });
        $('#size-option-cover').click(() => { this._imageRec.backgroundsize = "cover"; this.Refresh() });
        $('#size-option-contain').click(() => { this._imageRec.backgroundsize = "contain"; this.Refresh() });
        //
        $('#image-center').click(() => { this._imageRec.backgroundposition = "center"; this.Refresh() });
        $('#image-top').click(() => { this._imageRec.backgroundposition = "top"; this.Refresh() });
        $('#image-bottom').click(() => { this._imageRec.backgroundposition = "bottom"; this.Refresh() });
        $('#image-left').click(() => { this._imageRec.backgroundposition = "left"; this.Refresh() });
        $('#image-right').click(() => { this._imageRec.backgroundposition = "right"; this.Refresh() });
        //
        $('#image-done').click(() => this.Done());
        $('#image-cancel').click(() => this.Close());

        $('#image-background').click((ev: JQueryEventObject) => {
            if (ev.target.id == 'image-background')
                this.Close();
        });

        //  ファイル選択画面の表示
        $('#image-attach').click((e) => $('#image-selector').click())

        //  ファイル選択時イベント
        $('#image-selector').change((event) => {
            var any = event.currentTarget as any;
            var files: FileList = any.files;
            this.FileToBase64(files);
        });

        //  カメラの起動
        $('#image-camera').click((e) => $('#image-camerafile').click());

        //  カメラでの画像取得時イベント
        $('#image-camerafile').change((event) => {
            var any = event.currentTarget as any;
            var files: FileList = any.files;
            this.FileToBase64(files);
        });

        //  画像エリアのイベント（ドラック＆ドロップ用）

        var element = $('#image-view');

        element.on('dragover', (e) => {
            var event = e.originalEvent as DragEvent;
            event.preventDefault();
            event.dataTransfer.dropEffect = 'copy';
            element.focus();
        }).on('drop', (e) => {
            var event = e.originalEvent as DragEvent;
            event.preventDefault();
            var files: FileList = event.dataTransfer.files;
            this.FileToBase64(files);
            this.UrtTobase64(event.dataTransfer.items);
        });
    }


    /**
     * 画像データの追加／更新
     */
    private static Done() {
        //
        //  未実装
        //
    }


    /**
     * 終了処理
     */
    private static Close() {
        window.close();
    }


    /**
     * 空の画像データ生成
     */
    private static CreateEmptyImageRec(): ImageRec {

        var result = new ImageRec("");

        result.backgroundsize = $('input[name=size-options]:checked').val();
        result.backgroundrepeat = 'no-repeat';
        result.backgroundposition = 'center';

        return result;
    }


    /**
     * 画像ドロップ時イベント
     * @param file
     * @param src
     */
    private static OnDropImage(file: File, src) {
        var rec = ImageUIController.CreateImageRec(src);
        ImageUIController.SetImage(rec);
    }


    /**
     * 画像データ生成
     * @param src
     */
    private static CreateImageRec(src): ImageRec {
        var rec = this.CreateEmptyImageRec();
        rec.src = src;
        this._imageRec = rec;
        return rec;
    }


    /**
     * 画像の再描画
     */
    private static Refresh() {
        if (this._imageRec) {
            this.SetImage(this._imageRec);
        }
    }


    /**
     * 画像の表示
     * @param image
     */
    private static SetImage(image: ImageRec) {

        if (image == null) {
            image = this.CreateEmptyImageRec();
        }

        this._imageRec = image

        var div = $('#image-view');

        div.css("width", "100%").css("height", "100%")

        if (image.src == null || image.src.length == 0) {
            div.css("background", "");
        }

        ImageRec.setCss(div, image);
    }


    /**
     * 指定されたファイルを Base64 形式に変換する
     * @param files
     */
    private static FileToBase64(files: FileList) {

        for (var i = 0, l = files.length; i < l; i++) {

            var file: File = files[i];

            if (file.type.indexOf('image/') === 0) {

                var reader = new FileReader();

                var dropImage = this.OnDropImage;

                reader.onload = function (event) {
                    var target = event.target as FileReader;
                    dropImage(file, target.result);
                };

                reader.readAsDataURL(file);
            }
        }
    }


    /**
     * 指定されたURLの画像を Base64 形式に変換する
     * @param itemList
     */
    private static UrtTobase64(itemList) {

        for (var i = 0, l = itemList.length; i < l; i++) {

            var dti: DataTransferItem = itemList[i];

            if (dti != null && dti.type == 'text/html')
                dti.getAsString((s) => { this.DataTransferItem(s) });
        }
    }


    /**
     * データ変換処理
     * @param value
     */
    private static DataTransferItem(value: string) {
        var doc: Document = new DOMParser().parseFromString(value, 'text/html');
        var result = doc.images[0].attributes.getNamedItem('src').nodeValue;
        this.OnDropImage(null, result);
    }

}