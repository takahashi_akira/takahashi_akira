﻿
class StageReceiver extends ServiceReceiver {


    private _profileMap = new Map<number, Profile>();


    /**
     * ステージ情報
     * @param sender
     */
    public GetStage(sender): ServiceRecv {
        var result = new ServiceRecv();
        result.ConnectionRecv(StageUIController.GetStore());
        result.ConnectionRecv(StageSetCache.ActorGroupInfoList);
        result.ConnectionRecv(StageSetCache.StageSetInfoList);
        return result;
    }


    /**
     * カーソル処理
     * @param sender
     */
    public VideoCursor(sender: VideoCursor): ServiceRecv {

        var result = new ServiceRecv();
        var prof = this._profileMap.get(sender.pid);
        if (prof != null)
            sender.name = prof.name;

        result.AllClientRecv(sender);
        return result;

    }


    /**
     * プロフィールリスト
     * @param sender
     */
    public ProfileList(sender: ProfileList): ServiceRecv {

        this._profileMap = new Map<number, Profile>();

        sender.List.forEach(profile => {
            this._profileMap.set(profile.pid, profile);
        });

        return null;
    }


    /**
     * アイコンの要求
     * @param conn
     * @param gi
     */
    public GetIcon(conn: PeerJs.DataConnection, gi: GetIcon): ServiceRecv {

        var result = new ServiceRecv();
        var icon = ActorImageCache.get(gi.iid);

        if (icon === null || icon === undefined) {
            //  iconが登録されていない場合、オーナー側にリクエストする
            WebRTCService.OwnerSend(gi);
        }
        else {
            //  キャッシュ済みの場合はそのままキャッシュから返す
            LogUtil.Info("cache return");
            var store = new IconStore();
            store.Icon = icon;
            result.ConnectionRecv(store);
        }

        return result;
    }


    /**
     * アイコンの設定
     * @param is
     */
    public IconStore(is: IconStore): ServiceRecv {

        var result = new ServiceRecv();

        if (ActorImageCache.set(is.Icon))
            result.AllClientRecv(is);

        return result;
    }


    /**
     * グループリスト
     * @param sender
     */
    public ActorGroupInfoList(sender: ActorGroupInfoList): ServiceRecv {

        var result = new ServiceRecv();
        result.AllClientRecv(StageSetCache.ActorGroupInfoList);
        return result;
    }


    /**
     * 
     * @param sender
     */
    public StageSetInfoList(sender: StageSetInfoList): ServiceRecv {
        var result = new ServiceRecv();
        StageSetCache.StageSetInfoList = sender as StageSetInfoList;
        result.AllClientRecv(StageSetCache.StageSetInfoList);
        return result;
    }


    /**
     * 
     * @param sender
     */
    public ImageRec(sender: ImageRec): ServiceRecv {
        StageSetCache.SetImageRec(sender);
        return null;
    }


    /**
     * 
     * @param conn
     * @param sender
     */
    public GetImageRec(conn: PeerJs.DataConnection, sender: GetImageRec): ServiceRecv {

        StageSetCache.GetImageRec(sender.uuid, (imgRec) => {
            conn.send(JSON.stringify(imgRec));
        });
        return null;
    }


    /**
     * 
     * @param sender
     */
    public MsgSotre(sender: MsgStore): ServiceRecv {

        if (StageUIController.LastMessage == null)
            return;


        var pid = Util.GetUrlPid();
        var lastMsg: MsgLog = null;

        sender.Messages.forEach(msg => {
            if (msg.pid == pid)
                lastMsg = msg;
        });

        if (lastMsg == null)
            return;

        //  最終メッセージが非表示にされた場合、字幕表示もクリアする
        if (!lastMsg.visible && lastMsg.text == StageUIController.LastMessage.text) {

            var result = new Message();
            result.pid = pid;
            result.iid = 0;
            result.text = "";
            result.isSpeech = false;

            WebRTCService.ChildSendAll(result);
            StageUIController.LastMessage = null;
        }
    }

}