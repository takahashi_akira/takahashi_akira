﻿

class StageController extends ServiceController {


    constructor() {
        super();
        this.Receiver = new StageReceiver(this);
    };

    private _peerid: string = null;
    private _isConnectServer: boolean = false;


    /**
     * 自身のPeer生成時イベント
     * @param peer
     */
    public OnPeerOpen(peer: PeerJs.Peer) {
        this._peerid = peer.id;
        this.SendStageService();
    }


    /**
     * オーナー接続時イベント
     */
    public OnOwnerConnection() {
        this._isConnectServer = true;
        this.SendStageService();
    }


    /**
     * ステージ情報通知用データを作成
     */
    private SendStageService() {

        //  peeridの取得とオーナー接続が完了している場合、オーナーにURLを通知する
        if (this._isConnectServer && this._peerid !== null && this._peerid !== undefined) {

            var sender = new StageService();
            sender.name = "Stage";
            sender.url = Util.CreateLink('../StageClient/', this._peerid);
            sender.pid = Util.GetUrlPid();
            sender.enabled = true;
            StageUIController.SetStageServiceSender(sender);
        }
    }


    /**
     * 切断時処理
     */
    public OnPeerClose() {

        if (this.IsOpen)
            StageUIController.ServerSend(false);

    }

};
