﻿
class StageUIController {

    private static _dispVideoView = false;
    private static _audioVisible: boolean = false;
    private static _videoVisible: boolean = false;
    private static _audtoSource: string = null;
    private static _videoSource: string = null;
    private static _preview: boolean = false;
    private static _sourecMap = new Map<string, string>();
    private static _stageServiceSender: StageService;
    private static _use_speech_recognition: boolean = true;

    //
    //  音声認識の最終メッセージの保持
    //
    public static LastMessage: Message = null;


    /**
     * 初期化処理
     */
    public static Initialize() {

        Util.StopPropagation();
        $('#stop_streaming').hide();

        $('#video-switch').change(function () {
            StageUIController._videoVisible = $(this).is(':checked');
            StageUIController.SetStreaming();
        });

        $('#mic-switch').change(function () {
            StageUIController._audioVisible = $(this).is(':checked');
            StageUIController.SetStreaming();
        });

        $('#preview-check').change(function () {
            StageUIController._preview = $(this).is(':checked');
            StageUIController.SetPreview();
        });

        $('#start_streaming').click(() => {
            StageUIController.SetStreaming();
            $('#start_streaming').hide();
            $('#stop_streaming').show();

            $('#mic-select').attr("disabled", 1);
            $('#webcam-select').attr("disabled", 1);
            $('#speech_recognition').attr("disabled", 1);
        });

        $('#stop_streaming').click(() => {
            //  ページをリロードして停止する
            location.reload();
        });

        $('#speech_recognition').click(() => {
            StageUIController._use_speech_recognition = $(this).is(':checked');
            this.SetSpeech();
        });

        this.InitSpeech();
    }


    /**
     * Video/Audioソースの取得とリストへのセット
     */
    public static SetMediaDevice() {

        var camList: JQuery = $('ul#webcam-list');
        var micList: JQuery = $('ul#mic-list');

        camList.empty();
        camList.append("<li class='mdl-menu__item'></li>");

        micList.empty();
        micList.append("<li class='mdl-menu__item'></li>");

        var comCount: number = 0;
        var micCount: number = 0;


        navigator.mediaDevices.enumerateDevices().then((devices) => {

            devices.forEach((device, index, array) => {

                if (device.kind === 'audioinput') {

                    micCount++;

                    var name = (device.label || 'microphone ' + micCount.toString());
                    micList.append("<li class='mdl-menu__item'>" + name + "</li>");

                    //  先頭ソースは初期値としてセットする
                    if (micCount == 1) {
                        $("#mic-select").val(name);
                        StageUIController._audtoSource = device.deviceId;
                    }

                    StageUIController._sourecMap.set(name, device.deviceId);

                }
                else if (device.kind === 'audiooutput') {

                    //  オーディオ出力は処理無

                }
                else if (device.kind === 'videoinput') {

                    //  Video(Camera)ソース
                    comCount++;

                    var name = (device.label || 'camera ' + comCount.toString());
                    camList.append("<li class='mdl-menu__item'>" + name + "</li>");

                    //  先頭ソースは初期値としてセットする
                    if (comCount == 1) {
                        $("#webcam-select").val(name);
                        StageUIController._videoSource = device.deviceId;
                    }

                    StageUIController._sourecMap.set(name, device.deviceId);
                }


            });

            $('div#webcam-select-div').addClass('is-dirty');
            $('div#mic-select-div').addClass('is-dirty');

            //  webcam リストの選択時イベント
            $('ul#webcam-list > li').on("click", StageUIController.WebCameraSelect);
            //  mic リストの選択時イベント
            $('ul#mic-list > li').on("click", StageUIController.MicSelect);

        }).catch((error) => {

            LogUtil.Error(error.name + ": " + error.message);

        });

    }


    /**
     * マイク選択時イベント
     * @param e
     */
    public static MicSelect(e) {
        var target = $(e.target);
        $(e.target).parents('.mdl-select').children('input').val(target.text());
        $(e.target).parents('.mdl-select').click();

        var src = target.text();

        if (src === "") {
            StageUIController._audtoSource = "";
        }
        else {
            StageUIController._audtoSource = StageUIController._sourecMap.get(src);
        }
    }


    /**
     * カメラ選択時イベント
     * @param e
     */
    public static WebCameraSelect(e) {
        var target = $(e.target);
        $(e.target).parents('.mdl-select').children('input').val(target.text());
        $(e.target).parents('.mdl-select').click();

        var src = target.text();

        if (src === "") {
            StageUIController._videoSource = "";
        }
        else {
            StageUIController._videoSource = StageUIController._sourecMap.get(src);
        }

        StageUIController.SetPreview();
    }


    /**
     * プレビュー
     */
    public static SetPreview() {
        WebRTCService.SetPreview(
            StageUIController._videoSource,
            StageUIController._preview,
            $("#video")
        );
    }


    /**
     * ストリーミングの開始
     */
    public static SetStreaming() {

        StageUIController._audioVisible = (StageUIController._audtoSource !== "");
        StageUIController._videoVisible = (StageUIController._videoSource !== "");

        //
        WebRTCService.SetStreaming(
            StageUIController._audioVisible,
            StageUIController._audtoSource,
            StageUIController._videoVisible,
            StageUIController._videoSource
        );

        //  オーナー 及び 接続クライアントに通知
        StageUIController.ServerSend((StageUIController._audioVisible || StageUIController._videoVisible));


        this.SetSpeech();
    }


    /**
     * ストア処理
     */
    public static GetStore(): StageInfo {
        var store = new StageInfo();
        store.dispVideoView = this._dispVideoView;

        return store;
    }


    /**
     * オーナー通知用データのセット
     * @param sender
     */
    public static SetStageServiceSender(sender: StageService) {
        this._stageServiceSender = sender;
    }


    /**
     * オーナーにストリーミングの有無を通知
     * @param enabled
     */
    public static ServerSend(enabled: boolean) {

        if (this._dispVideoView == enabled)
            return;

        this._dispVideoView = enabled;

        //  接続クライアントへの通知
        var store = new StageInfo();
        store.dispVideoView = enabled;
        WebRTCService.ChildSendAll(store);

        //  オーナー側への通知
        if (this._stageServiceSender) {
            this._stageServiceSender.enabled = enabled;
            WebRTCService.OwnerSend(this._stageServiceSender);
        }

    }


    /**
     * 
     */
    public static SetSpeech() {

        if (this._use_speech_recognition)
            this._recognition.start();
        else
            this._recognition.stop();
    }



    /*
     *  
     */
    private static _recognition = new webkitSpeechRecognition();


    /**
     * 音声認識の初期化
     */
    public static InitSpeech() {

        var win = window as any;

        win.SpeechRecognition = win.SpeechRecognition || webkitSpeechRecognition;
        this._recognition = new webkitSpeechRecognition();
        this._recognition.lang = 'ja';

        //
        this._recognition.onresult = (event: SpeechRecognitionEvent) => {

            var text = event.results.item(0).item(0).transcript;
            this._recognition.stop();

            var result = new Message();
            result.pid = Util.GetUrlPid();
            result.iid = 0;
            result.text = text;
            result.isSpeech = true;

            //  オーナーに送信
            WebRTCService.OwnerSend(result);

            //  ストリーミング受信クライアントに送信
            WebRTCService.ChildSendAll(result);

            this.LastMessage = result;
        }

        //
        this._recognition.onend = (event: any) => {

            this._recognition.start();

        }

    }

}
