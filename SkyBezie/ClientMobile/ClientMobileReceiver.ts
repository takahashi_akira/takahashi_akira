﻿
class ClientMobileReceiver extends ServiceReceiver {


    /**
     * 部屋情報
     * @param sender
     */
    public RoomInfo(sender: RoomInfo): ServiceRecv {
        ClientMobileUIController.SetRoomInfo(sender);
        return null;
    }


    /**
     * ログイン情報
     * @param sender
     */
    public LoginResult(sender: LoginResult): ServiceRecv {

        if (sender.succeed) {

            Login.Profile = sender.myProfile;
            ClientMobileUIController.LoginSetup(sender.myProfile);
            Inputbox.Pid = sender.myProfile.pid;
        }
        else {
            Login.Open(sender.message);
        }

        return null;
    }


    /**
     * メッセージグループ情報
     * @param sender
     */
    public ActorGroupInfoList(sender: ActorGroupInfoList): ServiceRecv {
        LoginList.SetGroup(sender);
        return null;
    }


}