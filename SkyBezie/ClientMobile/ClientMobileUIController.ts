﻿
class ClientMobileUIController {


    /**
     * 初期化処理
     */
    public static Initialize() {

        //  
        $('.login-main').hide();
        $('.logout-background').hide();
        $('.client-main').hide();

        Util.StopPropagation();
        Util.StopTouchmove();

        //  表示開始
        document.body.removeAttribute('hidden');

        //  $('#contents_inputbox').draggable();
    }


    /**
     * 部屋情報の設定
     * @param roomInfo
     */
    public static SetRoomInfo(roomInfo: RoomInfo) {

        Login.Initilize(roomInfo);
        $('.login-name-field').addClass('is-dirty');
        $('.login-main').fadeIn();

    }


    /**
     * ログイン後初期化処理
     * @param profile
     */
    public static LoginSetup(profile: Profile) {

        //  各種コンポーネントの初期化処理
        Inputbox.Initilize();

        this.SetClipBordEvent();

        //  画面切替
        $('.login-background').fadeOut();
        $('.client-main').fadeIn();
    }


    /**
     * ログイン後初期化処理
     */
    public static SetClipBordEvent() {

        //  クリップボードイベント
        document.addEventListener('paste', function (e: any) {

        });

    }


    /**
     * 切断時処理
     */
    public static Disconnect() {

        $('#stage_page').attr('src', null);
        $('.client-main').fadeOut();
        $('.login-background').fadeOut();
        $('.logout-background').fadeIn();
    }

}