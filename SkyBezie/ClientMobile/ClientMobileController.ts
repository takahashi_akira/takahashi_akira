﻿

class ClientMobileController extends ServiceController {


    constructor() {
        super();
        this.Receiver = new ClientMobileReceiver(this);
    };


    /**
     * オーナー接続時イベント
     */
    public OnOwnerConnection() {
        WebRTCService.OwnerSend(new Sender(SenderType.GetRoomInfo));
    }


    /**
     * オーナー終了時イベント
     */
    public OnOwnerClose() {
        ClientMobileUIController.Disconnect();
    }


    /**
     * エラー発生時イベント
     * @param err
     */
    public OnPeerError(err: Error) {
        ClientMobileUIController.Disconnect();
        LogUtil.Error('peer error');
        LogUtil.Error(err.message);
    }

}
