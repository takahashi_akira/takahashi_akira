﻿

$(function () {

    var id = Util.GetPeerID();
    var url: string = Util.CreateLink("../ClientMobile", id, null, false);

    var qrcode: any = $('#qrcode');
    qrcode.qrcode(url);

});