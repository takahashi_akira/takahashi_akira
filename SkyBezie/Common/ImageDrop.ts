﻿
interface OnDropImage { (file: File, src): void }

/*
 *  このクラスは廃止予定です
 */
class ImageDrop {

    constructor() {
    }

    _onDropImage: OnDropImage;

    InitilizeImageDrop(element: JQuery, fileselect: JQuery, ondrop: OnDropImage, tag: number = 1) {

        this._onDropImage = ondrop;


        fileselect.change((event) => {

            var any = event.currentTarget as any;
            var files: FileList = any.files;
            this.FileToBase64(files);
        });

        element.click(e => {
            fileselect.click();
        });

        element.on('dragover', (e) => {
            var event = e.originalEvent as DragEvent;
            event.preventDefault();
            event.dataTransfer.dropEffect = 'copy';
            element.focus();
        });

        element.on('dragleave', (event) => {
            //  elDrop.css('background-color', '#cfc');
        });

        element.on('drop', (e) => {
            var event = e.originalEvent as DragEvent;
            event.preventDefault();
            var files: FileList = event.dataTransfer.files;
            this.FileToBase64(files);
            this.UrtTobase64(event.dataTransfer.items);
        });

    }


    FileToBase64(files: FileList) {

        for (var i = 0, l = files.length; i < l; i++) {

            var file: File = files[i];

            if (file.type.indexOf('image/') === 0) {

                var reader = new FileReader();

                var dropImage = this._onDropImage;

                reader.onload = function (event) {
                    var target = event.target as FileReader;
                    dropImage(file, target.result);
                };

                reader.readAsDataURL(file);
            }
        }
    }


    UrtTobase64(itemList) {

        for (var i = 0, l = itemList.length; i < l; i++) {

            var dti: DataTransferItem = itemList[i];

            if (dti != null && dti.type == 'text/html')
                dti.getAsString((s) => { this.dataTransferItem(s) });
        }
    }


    dataTransferItem(value: string) {
        var doc: Document = new DOMParser().parseFromString(value, 'text/html');
        var result = doc.images[0].attributes.getNamedItem('src').nodeValue;
        this._onDropImage(null, result);
    }

}
