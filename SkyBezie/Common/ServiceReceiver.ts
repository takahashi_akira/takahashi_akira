﻿
/*
 *  ServiceReciver
 */
interface IServiceReceiver {

    Receive(conn: PeerJs.DataConnection, sender: Sender): ServiceRecv;

    Ing(sender: Ing): ServiceRecv;
    Ings(sender: Ings): ServiceRecv;
    RoomInfo(sender: RoomInfo): ServiceRecv;
    GetRoomInfo(sender): ServiceRecv;
    Login(conn: PeerJs.DataConnection, sender: Login): ServiceRecv;
    LoginResult(sender: LoginResult): ServiceRecv;
    Message(sender: Message): ServiceRecv;
    GetMessage(sender): ServiceRecv;
    MsgSotre(sender: MsgStore): ServiceRecv;
    ProfileList(sender: ProfileList): ServiceRecv;
    GetProfileList(sender): ServiceRecv;
    ProfileEdit(sender: ProfileEdit): ServiceRecv;
    IconStore(sender: IconStore): ServiceRecv;
    GetIcon(conn: PeerJs.DataConnection, sender: GetIcon): ServiceRecv;
    IconIdList(sender: IconIdList): ServiceRecv;
    WallpaperInfo(sender: WallpaperInfo): ServiceRecv;
    GetIconIdList(sender: GetIconIdList): ServiceRecv;
    StageInfo(sender: StageInfo): ServiceRecv;
    GetStage(sender): ServiceRecv;
    StageService(conn: PeerJs.DataConnection, sender: StageService): ServiceRecv;
    StageServiceList(sender: StageServiceList): ServiceRecv;
    VideoCursor(sender: VideoCursor): ServiceRecv;
    ImageRec(sender: ImageRec): ServiceRecv;
    GetImageRec(conn: PeerJs.DataConnection, sender: GetImageRec): ServiceRecv;
    StageSetInfo(sender: StageSetInfo): ServiceRecv;
    StageSetInfoList(sender: StageSetInfoList): ServiceRecv;
    GetStageSetInfoList(sender): ServiceRecv;
    ActorGroupInfo(sender: ActorGroupInfo): ServiceRecv;
    ActorGroupInfoList(sender: ActorGroupInfoList): ServiceRecv;
    GetActorGroupInfoList(sender): ServiceRecv;
    SceneInfo(sender: SceneInfo): ServiceRecv;
    GetSceneInfo(sender: GetSceneInfo): ServiceRecv;
    Layout(sender: Layout): ServiceRecv;
}


/*
 *
 */
class ServiceReceiver implements IServiceReceiver {


    private _controller: IServiceController = null;

    /**
     * コンストラクタ
     * @param serviceController
     */
    constructor(serviceController: IServiceController) {
        this._controller = serviceController;
    }


    protected get Controller(): IServiceController {
        return this._controller;
    }


    /**
     * データ取得時処理
     * @param conn
     * @param sender
     */
    public Receive(conn: PeerJs.DataConnection, sender: Sender): ServiceRecv {

        if (!sender)
            return null;

        if (!sender.type)
            return null;


        if (sender.type == SenderType.Ing) return this.Ing(sender as Ing);
        if (sender.type == SenderType.Ings) return this.Ings(sender as Ings);
        if (sender.type == SenderType.RoomInfo) return this.RoomInfo(sender as RoomInfo);
        if (sender.type == SenderType.GetRoomInfo) return this.GetRoomInfo(sender);
        if (sender.type == SenderType.Login) return this.Login(conn, sender as Login);
        if (sender.type == SenderType.LoginResult) return this.LoginResult(sender as LoginResult);
        if (sender.type == SenderType.Message) return this.Message(sender as Message);
        if (sender.type == SenderType.MsgStore) return this.MsgSotre(sender as MsgStore);
        if (sender.type == SenderType.ProfileList) return this.ProfileList(sender as ProfileList);
        if (sender.type == SenderType.GetProfileList) return this.GetProfileList(sender);
        if (sender.type == SenderType.ProfileEdit) return this.ProfileEdit(sender as ProfileEdit);
        if (sender.type == SenderType.IconStore) return this.IconStore(sender as IconStore);
        if (sender.type == SenderType.GetIcon) return this.GetIcon(conn, sender as GetIcon);
        if (sender.type == SenderType.IconIdList) return this.IconIdList(sender as IconIdList);
        if (sender.type == SenderType.WallpaperInfo) return this.WallpaperInfo(sender as WallpaperInfo);
        if (sender.type == SenderType.GetIconIdList) return this.GetIconIdList(sender as GetIconIdList);
        if (sender.type == SenderType.StageInfo) return this.StageInfo(sender as StageInfo);
        if (sender.type == SenderType.GetStage) return this.GetStage(sender);
        if (sender.type == SenderType.StageService) return this.StageService(conn, sender as StageService);
        if (sender.type == SenderType.StageServiceList) return this.StageServiceList(sender as StageServiceList);
        if (sender.type == SenderType.VideoCursor) return this.VideoCursor(sender as VideoCursor);
        if (sender.type == SenderType.ImageRec) return this.ImageRec(sender as ImageRec);
        if (sender.type == SenderType.GetImageRec) return this.GetImageRec(conn, sender as GetImageRec);
        if (sender.type == SenderType.StageSetInfo) return this.StageSetInfo(sender as StageSetInfo);
        if (sender.type == SenderType.StageSetInfoList) return this.StageSetInfoList(sender as StageSetInfoList);
        if (sender.type == SenderType.GetStageSetInfoList) return this.GetStageSetInfoList(sender);
        if (sender.type == SenderType.ActorGroupInfo) return this.ActorGroupInfo(sender as ActorGroupInfo);
        if (sender.type == SenderType.ActorGroupInfoList) return this.ActorGroupInfoList(sender as ActorGroupInfoList);
        if (sender.type == SenderType.GetActorGroupInfoList) return this.GetActorGroupInfoList(sender);
        if (sender.type == SenderType.SceneInfo) return this.SceneInfo(sender as SceneInfo);
        if (sender.type == SenderType.GetSceneInfo) return this.GetSceneInfo(sender as GetSceneInfo);
        if (sender.type == SenderType.Layout) return this.Layout(sender as Layout);

        return null;
    };

    public Ing(sender: Ing): ServiceRecv { return null; }
    public Ings(sender: Ings): ServiceRecv { return null; }
    public RoomInfo(sender: RoomInfo): ServiceRecv { return null; }
    public GetRoomInfo(sender): ServiceRecv { return null; }
    public Login(conn: PeerJs.DataConnection, sender: Login): ServiceRecv { return null; }
    public LoginResult(sender: LoginResult): ServiceRecv { return null; }
    public Message(sender: Message): ServiceRecv { return null; }
    public GetMessage(sender): ServiceRecv { return null; }
    public MsgSotre(sender: MsgStore): ServiceRecv { return null; }
    public ProfileList(sender: ProfileList): ServiceRecv { return null; }
    public GetProfileList(sender): ServiceRecv { return null; }
    public ProfileEdit(sender: ProfileEdit): ServiceRecv { return null; }
    public IconStore(sender: IconStore): ServiceRecv { return null; }
    public GetIcon(conn: PeerJs.DataConnection, sender: GetIcon): ServiceRecv { return null; }
    public IconIdList(sender: IconIdList): ServiceRecv { return null; }
    public WallpaperInfo(sender: WallpaperInfo): ServiceRecv { return null; }
    public GetIconIdList(sender: GetIconIdList): ServiceRecv { return null; }
    public StageInfo(sender: StageInfo): ServiceRecv { return null; }
    public GetStage(sender): ServiceRecv { return null; }
    public StageService(conn: PeerJs.DataConnection, sender: StageService): ServiceRecv { return null; }
    public StageServiceList(sender: StageServiceList): ServiceRecv { return null; }
    public VideoCursor(sender: VideoCursor): ServiceRecv { return null; }
    public ImageRec(sender: ImageRec): ServiceRecv { return null; }
    public GetImageRec(conn: PeerJs.DataConnection, sender: GetImageRec): ServiceRecv { return null; }
    public StageSetInfo(sender: StageSetInfo): ServiceRecv { return null; }
    public StageSetInfoList(sender: StageSetInfoList): ServiceRecv { return null; }
    public GetStageSetInfoList(sender): ServiceRecv { return null; }
    public ActorGroupInfo(sender: ActorGroupInfo): ServiceRecv { return null; }
    public ActorGroupInfoList(sender: ActorGroupInfoList): ServiceRecv { return null; }
    public GetActorGroupInfoList(sender): ServiceRecv { return null; }
    public SceneInfo(sender: SceneInfo): ServiceRecv { return null; }
    public GetSceneInfo(sender: GetSceneInfo): ServiceRecv { return null; }
    public Layout(sender: Layout): ServiceRecv { return null; }
}
