﻿
class WebRTCService {

    private static _key = 'd8c7eaec-ec4e-4389-aaeb-3a0e92c8af94';
    private static _peer: PeerJs.Peer;
    private static _server: PeerJs.DataConnection;
    private static _clients: Array<PeerJs.DataConnection> = new Array<PeerJs.DataConnection>();
    private static _service: IServiceController;


    /**
     * WebRTCServiceの起動
     * @param service
     * @param serverid
     * @param name
     */
    public static Start(service: IServiceController, serverid: string, name: string) {

        var log = "Start WebRTC : " + name;
        if (serverid)
            log += "(ServerID : " + serverid + ")";
        LogUtil.Info(log);

        this._peer = new Peer({ key: this._key, debug: 1 });
        this._service = service;

        //  終了時処理
        $(window).on('beforeunload', function (e) { WebRTCService.Close(); });

        //  ストリーミング用設定
        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

        if (serverid != null && serverid.length > 0) {
            //
            this._server = this._peer.connect(serverid);
            this.OwnerPeerSetting(service, this._server);
        }

        this.PeerSetting(service, this._peer);

    }


    /**
     * 呼出元の接続設定
     * @param service 自身のサービスコントローラー
     * @param owner 呼出元の接続情報
     */
    private static OwnerPeerSetting(service: IServiceController, owner: PeerJs.DataConnection) {

        owner.on("open", () => {
            service.OnOwnerConnection();
        });

        owner.on("error", (e) => {
            service.OnOwnerError(e);
        });

        owner.on("close", () => {
            service.OnOwnerClose();
        });

        owner.on("data", (data) => {
            if (owner.peerConnection)
                service.Recv(owner.peerConnection, data);
        });

        return owner;
    }


    /**
     * 自身のPeer設定
     * @param service 自身のサービスコントローラー
     * @param peer 自身のPeer接続
     */
    private static PeerSetting(service: IServiceController, peer: PeerJs.Peer) {

        peer.on('open', () => {
            service.OnPeerOpen(peer);
        });

        peer.on('error', (e) => {
            service.OnPeerError(e);
        });

        peer.on('close', () => {
            service.OnPeerClose();
        });

        peer.on('call', function (call) {

            //  仮実装
            var video = $('#video');

            if (video) {
                call.answer();
                if (this.existingCall) {
                    this.existingCall.close();
                }

                call.on('stream', (stream) => {
                    video.prop('src', URL.createObjectURL(stream));
                });
                this.existingCall = call;
            }
        });

        peer.on('connection', (conn) => {
            this.ChildPeerSetting(service, conn);
        });

    }


    /**
     * 自身に接続されている子Peerの設定
     * @param service 自身のサービスコントローラー
     * @param child 子のPeerID
     */
    private static ChildPeerSetting(service: IServiceController, conn: PeerJs.DataConnection) {

        //
        conn.on("open", () => {

            //  クライアントリストに追加
            WebRTCService._clients.push(conn);

            //  ストリーミングしている場合は動画を送る
            if (WebRTCService._localStream) {

                if (WebRTCService._existingCallList == null)
                    WebRTCService._existingCallList = new Array<PeerJs.MediaConnection>();

                WebRTCService._existingCallList.push(WebRTCService._peer.call(conn.peer, WebRTCService._localStream));
            }

            //  イベント通知
            service.OnChildConnection(conn);

        });

        //
        conn.on('error', (e) => {
            service.OnChildError(e);
        });

        //
        conn.on("close", () => {
            service.OnChildClose(conn);
        });

        //
        conn.on("data", (data) => {

            var recv: ServiceRecv = service.Recv(conn, data);

            if (recv) {

                //  データを送信してきたコネクションへのみ送信
                recv.GetConnectionRecv().forEach((recv) => {
                    this.ChildSend(conn, recv);
                });

                //  全接続クライアントに送信
                recv.GetAllConnectionRev().forEach((recv) => {
                    this.ChildSendAll(recv);
                });
            }

        });

    }


    /**
     *  WebRTCServiceの停止
     *  全てクライアントとの接続を切断します
     */
    public static Close() {
        this._clients.forEach((client) => {
            client.close();
        });
    }


    public static get peerid(): string {
        return this._peer.id;
    }


    /**
     * オーナーへの送信
     * @param data
     */
    public static OwnerSend(data: Sender) {

        if (!this._server)
            return;

        var senddata = JSON.stringify(data);

        if (this._server.open) {
            this._server.send(senddata);
        }
        else {
            LogUtil.Warning("Owner is not open : Send message lost : " + senddata);
        }

    }


    /**
     * 指定クライアントへの送信
     * @param conn
     * @param data
     */
    public static ChildSend(conn: PeerJs.DataConnection, data: Sender) {
        var json = JSON.stringify(data);
        conn.send(json);

        if (LogUtil.IsOutputSender(data))
            LogUtil.Info("Send : " + json.toString());
    }


    /**
     * 全接続クライアントへの送信
     * @param data
     */
    public static ChildSendAll(data: Sender) {

        var json = JSON.stringify(data);

        WebRTCService._clients.forEach(client => {

            //  開いているクライアントにのみ通知
            if (client.open) {
                client.send(json);
            }
            else {
                LogUtil.Warning("Client is not open : Send message lost : " + json);
            }


        });

        if (LogUtil.IsOutputSender(data))
            LogUtil.Info("Send[all] : " + json.toString());
    }


    private static _localStream: MediaStream;
    private static _previewStream: MediaStream;
    private static _existingCallList: Array<PeerJs.MediaConnection>;


    /**
     * プレビュー設定
     * @param videoSource
     * @param isPreView
     * @param preview
     */
    public static SetPreview(videoSource: string, isPreView: boolean = false, preview: JQuery = null) {

        if (isPreView) {
            var constraints = {
                video: { optional: [{ sourceId: videoSource }] }
            };

            navigator.getUserMedia(constraints,
                (stream) => {
                    //  プレビュー表示
                    this._previewStream = stream;
                    preview.prop('src', URL.createObjectURL(stream));
                }, () => {
                }
            );
        }
        else {

            this.StopPreview();
            preview.prop('src', '');
            return;
        }

    }


    /**
     * ストリーミング設定
     * @param isAudio
     * @param audioSource
     * @param isVideo
     * @param videoSource
     */
    public static SetStreaming(isAudio: boolean, audioSource: string, isVideo: boolean, videoSource: string) {

        this.StopPreview();
        this.VideoMute();
        this.AudioMute();

        if (isVideo || isAudio) {

            var constraints;

            if (isAudio && isVideo) {
                constraints = {
                    audio: { optional: [{ sourceId: audioSource }] },
                    video: { optional: [{ sourceId: videoSource }] }
                };
            }
            else if (isAudio) {
                constraints = {
                    audio: { optional: [{ sourceId: audioSource }] },
                };
            }
            else if (isVideo) {
                constraints = {
                    video: { optional: [{ sourceId: videoSource }] }
                };
            }

            navigator.getUserMedia(constraints,
                (stream) => {

                    //  ストリーミング開始 / 設定変更
                    this._localStream = stream;

                    //  接続済みのクライアントに動画配信開始
                    this._clients.forEach(conn => {

                        if (this._existingCallList == null)
                            this._existingCallList = new Array<PeerJs.MediaConnection>();

                        this._existingCallList.push(this._peer.call(conn.peer, this._localStream));
                    });

                    //
                    this._service.OnStreaming(true, true);

                }, () => {

                    //
                    this._service.OnStreaming(false, false);

                }
            );
        }
        else {
            this._localStream = null;

            if (this._existingCallList) {
                this._existingCallList.forEach(exc => {
                    exc.close();
                });
                this._existingCallList = null;
            }
        }

    }


    /**
     * 動画PreViewの停止
     */
    public static StopPreview() {
        if (this._previewStream)
            if (this._previewStream.getVideoTracks().length > 0)
                this._previewStream.getVideoTracks()[0].stop();
    }


    /**
     * 動画配信のミュート
     */
    public static VideoMute() {

        if (this._localStream)
            if (this._localStream.getVideoTracks().length > 0)
                this._localStream.getVideoTracks()[0].stop();
    }


    /**
     *  音声配信のミュート
     */
    public static AudioMute() {
        if (this._localStream)
            if (this._localStream.getAudioTracks().length > 0)
                this._localStream.getAudioTracks()[0].stop();
    }

}