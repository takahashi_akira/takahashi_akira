﻿

/*
 *  DBキャッシュに伴い、このクラスは廃止予定です
 */
class ActorImageCache {

    private static _cache: Map<number, Icon> = new Map<number, Icon>();

    //
    //  オーナーから送られてきた画像をキャッシュする
    //  
    public static set(pi: Icon): boolean {

        if (pi === null || pi === undefined)
            return false;

        if (pi.img === null || pi.img === undefined)
            return false;

        if (this._cache.has(pi.iid))
            this._cache.delete(pi.iid);

        this._cache.set(pi.iid, pi);
        this.setCss(pi);

        return true;
    }


    //
    //  指定画像IDの画像データを取得
    //
    public static get(iid: number): Icon {

        if (iid == 0)
            return null;

        var gpi = new GetIcon(iid);
        var icon: Icon = null;

        if (this._cache.has(iid)) {
            icon = this._cache.get(iid);

            if (icon !== null)
                return icon;
        }

        this._cache.set(iid, null);
        var gpi = new GetIcon(iid);
        WebRTCService.OwnerSend(gpi);

        return null;
    }


    //
    //  指定した画像IDデータを設定します
    //
    public static findSetCss(iid: number) {

        var pi = this.get(iid);

        if (pi) {
            this.setCss(pi);
        }
    }


    //
    //
    private static setCss(pi: Icon) {

        var div = $('div.actor-img-' + pi.iid);
        div.css("width", "100%");
        div.css("height", "100%");

        ImageRec.setCss(div, pi.img);
    }

}