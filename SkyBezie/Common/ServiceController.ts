﻿
/*
 *  Peerサービスインターフェイス
 */
interface IServiceController {

    //  自身のPeer生成時イベント
    OnPeerOpen(peer: PeerJs.Peer);

    //  Peer生成時エラー
    OnPeerError(err: Error);

    //  自身の終了時イベント
    OnPeerClose();

    //  オーナー接続イベント
    OnOwnerConnection();

    //  オーナーエラー
    OnOwnerError(err: Error);

    //  オーナー切断時イベント
    OnOwnerClose();

    //  他から接続された時のイベント
    OnChildConnection(conn: PeerJs.DataConnection);

    //  他から接続された時のエラー
    OnChildError(err: Error);

    //  他から接続が解除された時のイベント
    OnChildClose(conn: PeerJs.DataConnection);

    //  ストリーミング開始時・終了時イベント
    OnStreaming(isSucceed: boolean, isStreaming: boolean);

    //  データ取得時イベント
    Recv(conn: PeerJs.DataConnection, recv): ServiceRecv;

}


/**
 * Peerサービスコントローラーの抽象化クラス
 */
class ServiceController implements IServiceController {


    private _peer: PeerJs.Peer;
    protected Receiver: IServiceReceiver;


    constructor() {
    }


    /**
     *  Peer接続されているか？
     */
    public get IsOpen(): boolean {

        if (this._peer) {
            return this._peer.destroyed;
        }
        else {
            return false;
        }
    }


    /**
     * エラーログ出力
     * @param message
     * @param err
     */
    public LogError(message: string, err: Error) {

        var log = '';

        if (err.name && err.name.length > 0)
            log += err.name + ' : ';

        if (message && message.length > 0)
            log += message + '\n';

        if (err.message && err.message.length > 0)
            log += err.message;

        LogUtil.Error(log);
    }



    /**
     * Peer生成時イベント
     * @param peer
     */
    public OnPeerOpen(peer: PeerJs.Peer) {
        this._peer = peer;
    }


    /**
     * Peerエラー時イベント
     * @param err
     */
    public OnPeerError(err: Error) {
        this.LogError('This Peer', err);
    }


    /**
     * PeerClose時イベント
     */
    public OnPeerClose() {
        this._peer = null;
    }


    /**
     * オーナー接続イベント
     */
    public OnOwnerConnection() {
    }


    /**
     * オーナーエラー時イベント
     * @param err
     */
    public OnOwnerError(err: any) {
        this.LogError('Owner Peer', err);
    }


    /**
     * オーナー切断時イベント
     */
    public OnOwnerClose() {
    }


    /**
     * 他クライアントからの接続時イベント
     * @param conn
     */
    public OnChildConnection(conn: PeerJs.DataConnection) {
        LogUtil.Info("Connect : " + conn.label);
    }


    /**
     * Peerエラー時イベント
     * @param err
     */
    public OnChildError(err: Error) {
        this.LogError('Child Peer', err);
    }


    /**
     * 他クライアントの切断時イベント
     * @param conn
     */
    public OnChildClose(conn: PeerJs.DataConnection) {
        LogUtil.Info("Connect Close : " + conn.label);
    }


    /**
     * 動画配信処理開始・終了イベント
     * @param isSucceed
     * @param isStreaming
     */
    public OnStreaming(isSucceed: boolean, isStreaming: boolean) {

        if (isSucceed) {
            if (isStreaming) {
                LogUtil.Info("Streaming Start.");
            }
            else {
                LogUtil.Info("Streaming Stop.");
            }
        }
        else {
            LogUtil.Error("Streaming Error.");
        }
    }


    /**
     * データ取得時イベント
     * @param conn
     * @param recv
     */
    public Recv(conn: PeerJs.DataConnection, recv): ServiceRecv {

        if (recv === null)
            return;

        var sender: Sender = JSON.parse(recv) as Sender;

        if (LogUtil.IsOutputSender(sender))
            LogUtil.Info("Recv : " + recv);

        return this.Receiver.Receive(conn, sender);
    }

}


