﻿
interface OnGetImageRec { (imgRec: ImageRec): void }


class StageSetCache {

    public static ActorGroupInfoList: ActorGroupInfoList = null;
    public static StageSetInfoList: StageSetInfoList = null;

    private static _cacheDB = new SkyBezieCacheDB();


    /**
     * 画像情報のキャッシュ
     * @param imgrec
     */
    public static SetImageRec(imgrec: ImageRec) {

        this._cacheDB.Write(SkyBezieCacheDB.IMAGE, imgrec.uuid, imgrec,
            () => { WebRTCService.ChildSendAll(imgrec); }
        );

    }


    /**
     * キャッシュ画像の取得
     * @param uuid
     * @param callback
     */
    public static GetImageRec(uuid: string, callback: OnGetImageRec) {

        this._cacheDB.Read(SkyBezieCacheDB.IMAGE, uuid, (rec) => {

            if (rec) {
                callback(rec);
            }
            else {
                WebRTCService.OwnerSend(new GetImageRec(uuid));
            }
        });

    }
}