﻿
/**
 * ローカルストレージに対するデータ操作を行います。
 * このクラス以外での、ローカルストレージの使用はは、基本的に禁止です。
 */
class LocalCache {

    /**
     *  前回のログイン名称
     */
    public static set LoginName(val: string) { localStorage.setItem('login-name', val); }
    public static get LoginName(): string { return localStorage.getItem('login-name'); }


    /**
     *  部屋名称
     */
    public static set RoomName(val: string) { localStorage.setItem('room-name', val); }
    public static get RoomName(): string { return localStorage.getItem('room-name'); }


    /**
     *  ブートサーバーID
     */
    public static set BootServerID(val: string) { localStorage.setItem('serverid', val); }
    public static get BootServerID(): string {

        var result = localStorage.getItem('serverid');
        if (result)
            return result;
        else
            return "";
    }


    /**
     *  ブート時にクライアントも起動するか？
     */
    public static set BootWithClient(val: boolean) { localStorage.setItem('boot-check', (val ? "1" : "0")); }
    public static get BootWithClient(): boolean { return (localStorage.getItem('boot-check') == "1"); }

}