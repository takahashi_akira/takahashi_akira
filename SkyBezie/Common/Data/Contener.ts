﻿/**
 * 部屋情報
 */
class Room {
    title: string;
    max_profile_id: number = 1;
    max_img_id: number = 1;
    max_msg_id: number = 1;
    max_group_id: number = 1;
    max_guide_id: number = 1;
    max_scene_id: number = 1;
    msg_disp_count: number = 200;
};


/**
 * 通知情報
 */
class Sender {

    constructor(type: string) {
        this.type = type;
    }
    type: string;
}


/**
 * 通知「内容」
 */
class SenderType {
    static Room: string = "Room";
    static Ing: string = "Ing";
    static Ings: string = "Ings";
    static RoomInfo: string = "RoomInfo";
    static GetRoomInfo: string = "GetRoomInfo";
    static Login: string = "Login";
    static LoginResult: string = "LoginResult";
    static Message: string = "Message";
    static GetMessage: string = "GetMessage";
    static MsgStore: string = "MsgStore";
    static ProfileList: string = "ProfileList";
    static GetProfileList: string = "GetProfileList";
    static ProfileEdit: string = "ProfileEdit";
    static IconStore: string = "IconStore";
    static GetIcon: string = "GetIcon";
    static IconIdList: string = "IconIdList";
    static GetIconIdList: string = "GetIconIdList";
    static WallpaperInfo: string = "WallpaperInfo";
    static StageInfo: string = "StageInfo";
    static GetStage: string = "GetStage";
    static StageService: string = "StageService";
    static StageServiceList: string = "StageServiceList";
    static VideoCursor: string = "VideoCursor";
    static ImageRec: string = "ImageRec";
    static GetImageRec: string = "GetImageRec";
    static StageSetInfo: string = "StageSetInfo";
    static StageSetInfoList: string = "StageSetInfo";
    static GetStageSetInfoList: string = "GetStageSetInfoList";
    static ActorGroupInfo: string = "ActorGroupInfo";
    static ActorGroupInfoList: string = "ActorGroupInfoList";
    static GetActorGroupInfoList: string = "GetActorGroupInfoList";
    static SceneInfo: string = "SceneInfo";
    static GetSceneInfo: string = "GetSceneInfo";
    static Guide: string = "Guide";
    static Layout: string = "Layout";
}


/**
 * 
 */
class ServiceRecv {

    constructor() {
        this.connRecv = new Array<Sender>();
        this.allRecv = new Array<Sender>();
    }


    public ConnectionRecv(sender: Sender) {
        this.connRecv.push(sender);
    }

    public AllClientRecv(sender: Sender) {
        this.allRecv.push(sender);
    }

    public GetConnectionRecv(): Array<Sender> {
        return this.connRecv;
    }

    public GetAllConnectionRev(): Array<Sender> {
        return this.allRecv;
    }

    //  接続しているクライアントのみに返す
    private connRecv: Array<Sender>;

    //  全てのクライアントに返す
    private allRecv: Array<Sender>;

}


/**
 * メッセージ
 */
class Message extends Sender {

    constructor() {
        super(SenderType.Message);
    }

    pid: number = 0;
    iid: number = 0;
    text: string = "";
    isSpeech: boolean = false;
}


/**
 * メッセージログ
 */
class MsgLog {
    mid: number = 0;
    pid: number = 0;
    iid: number = 0;
    date: number = 0;
    text: string = "";
    dice: string = "";
    visible: boolean = true;
    isGuide: boolean = false;
}


/**
 * 
 */
class MsgStore extends Sender {

    constructor() {
        super(SenderType.MsgStore);
        this.Messages = new Array<MsgLog>();
    };

    Messages: Array<MsgLog>;
}


/**
 * 入力中情報
 */
class Ing extends Sender {
    constructor() {
        super(SenderType.Ing);
    }

    pid: number;
    status: boolean;
}


/**
 * 入力途中情報
 */
class Ings extends Sender {
    constructor() {
        super(SenderType.Ings);
        this.pids = new Array<number>();
    }

    pids: Array<number>;
}


/**
 * 部屋情報
 */
class RoomInfo extends Sender {
    constructor() { super(SenderType.RoomInfo); };
    roomName: string;
    dispActors: boolean;
    dispMessage: boolean;
}


/**
 * ログイン情報
 */
class LoginInfo extends Sender {
    constructor() { super(SenderType.Login); };
    name: string;
    md5: string;
}


/**
 * ログイン結果
 */
class LoginResult extends Sender {
    constructor() {
        super(SenderType.LoginResult);
    };
    succeed: boolean;
    message: string;
    myProfile: Profile;
    Profiles: ProfileList;
    store: MsgStore;
}


/**
 * プロフィール
 */
class Profile {

    constructor() {
        this.ptype = "P";
        this.pid = 0;
        this.iid = 0;
        this.owner = 0;
        this.name = '';
        this.status = '';
        this.URL = '';
        this.md5 = '';
        this.visible = true;
        this.islogin = false;
    }
    ptype: string;
    pid: number;
    iid: number;
    owner: number;
    name: string;
    status: string;
    URL: string;
    md5: string;
    islogin: boolean;
    visible: boolean;

}


/**
 * 
 */
class ActorProfile extends Profile {
    constructor() {
        super();
        this.ptype = "C";
    }
}


/**
 * 
 */
class ProfileEdit extends Sender {

    constructor() {
        super(SenderType.ProfileEdit);
        this.Profile = null;
    };

    Profile: Profile;
}


/**
 * 
 */
class ProfileList extends Sender {

    constructor() {
        super(SenderType.ProfileList);
        this.List = new Array<Profile>();
    };

    List: Array<Profile>;
}


/**
 * 
 */
class Icon {
    iid: number;
    pid: number;
    img: ImageRec;
}


/**
 * 
 */
class IconStore extends Sender {
    constructor() {
        super(SenderType.IconStore);
    }

    Icon: Icon;
}


/**
 * 
 */
class GetIcon extends Sender {
    constructor(iid: number) {
        super(SenderType.GetIcon);
        this.iid = iid;
    }
    iid: number;
}


/**
 * 
 */
class IconIdList extends Sender {
    constructor(pid: number) {
        super(SenderType.IconIdList);
        this.iidlist = new Array<number>();
    }

    pid: number;
    iidlist: Array<number>;
}


/**
 * 
 */
class GetIconIdList extends Sender {

    constructor(pid: number) {
        super(SenderType.GetIconIdList);
        this.pid = pid;
    }

    pid: number;
}


/**
 * 
 */
class WallpaperInfo extends Sender {

    constructor() {
        super(SenderType.WallpaperInfo);
    }
    Image: ImageRec;

}


/**
 * 
 */
class StageService extends Sender {
    constructor() {
        super(SenderType.StageService);
    }

    name: string;
    url: string;
    pid: number;

    /**
     * ステージの接続有無
     * ※名称リファクタリング予定
     */
    enabled: boolean;

    /*
     *  ステージの表示有無
     */
    hide: boolean;

}


/**
 * 
 */
class StageServiceList extends Sender {
    constructor() {
        super(SenderType.StageServiceList);
        this.List = new Array<StageService>();
    }

    List: Array<StageService>;
}


/**
 * 
 */
class VideoCursor extends Sender {
    constructor() {
        super(SenderType.VideoCursor);
    }
    pid: number;
    iid: number;
    name: string;
    posX: number;
    posY: number;
}


/**
 * 
 */
class ImageRec extends Sender {
    constructor(imageType: string) {
        super(SenderType.ImageRec);
        this.uuid = "";
        this.name = imageType;
        this.backgroundsize = 'auto';
        this.backgroundrepeat = 'repert';
        this.backgroundposition = 'center';
        this.visible = true;
    }

    uuid: string;
    name: string;
    src: string;
    backgroundsize: string;
    backgroundrepeat: string;
    backgroundposition: string;
    visible: boolean;

    public static setCss(div: JQuery, rec: ImageRec) {

        if (rec !== null) {
            if (rec.src == null || rec.src.length == 0) {
                div.css("background", "");
            }
            else {
                div.css("background", "url(" + rec.src + ")");
            }
            div.css("background-size", rec.backgroundsize)
            div.css("background-repeat", rec.backgroundrepeat)
            div.css("background-position", rec.backgroundposition);
        }
    }
}


/**
 * 
 */
class GetImageRec extends Sender {
    constructor(uuid: string) {
        super(SenderType.GetImageRec);
        this.uuid = uuid;
    }

    uuid: string;
}


/**
 * 
 */
class StageInfo extends Sender {
    constructor() {
        super(SenderType.StageInfo);
        this.dispVideoView = true;
    }
    dispVideoView: boolean;
    image_uuidlist: Array<string>;
}


/**
 * 
 */
class StageSetInfo extends Sender {

    constructor() {
        super(SenderType.StageSetInfo);
        this.stagesetid = "";
        this.name = "";
        this.thumbnail_uuid = "";
        this.image_uuid_list = new Array<string>();
    }

    stagesetid: string;
    name: string;
    thumbnail_uuid: string;
    image_uuid_list: Array<string>;
}


/**
 * 
 */
class StageSetInfoList extends Sender {

    constructor() {
        super(SenderType.StageSetInfoList);
        this.List = new Array<StageSetInfo>();
    }
    List: Array<StageSetInfo>;
}


/**
 * 
 */
class ActorGroupInfo extends Sender {

    constructor() {
        super(SenderType.ActorGroupInfo);
        this.groupid = -1;
        this.name = "";
        this.stageurl = "";
        this.stagesetid = "";
        this.pidlist = new Array<number>();
    }

    groupid: number;
    name: string;
    stageurl: string;
    stagesetid: string;
    pidlist: Array<number>;
}


/**
 * 
 */
class ActorGroupInfoList extends Sender {

    constructor() {
        super(SenderType.ActorGroupInfoList);
        this.List = new Array<ActorGroupInfo>();
    }
    List: Array<ActorGroupInfo>;
}


/**
 * 
 */
class SceneInfo extends ActorGroupInfo {

    constructor() {
        super();
        this.type = SenderType.SceneInfo;
        this.sceneid = -1;
    }

    sceneid: number;
}


/**
 * 
 */
class GetSceneInfo extends Sender {
    constructor(sceneid: number) {
        super(SenderType.GetSceneInfo);
        this.sceneid = sceneid;
    }

    sceneid: number;
}


/**
 * 
 */
class Guide extends Sender {

    constructor() {
        super(SenderType.Guide);
    }

    guideid: number = 0;
    key: string = "";
    value: string = "";
    visible: boolean = true;
    isYouTube: boolean = false;
    start_time: number = 0;
    end_time: number = 0;
    loop: boolean = false;
}


/*
 *  クライアントのレイアウト変更情報
 */
class Layout extends Sender {

    constructor() {
        super(SenderType.Layout);

        //  デフォルトは全て表示とする
        this.dispMenu = true;
        this.dispActors = true;
        this.dispMessage = true;
    }

    dispMenu: boolean;
    dispActors: boolean;
    dispMessage: boolean;
}


/**
 * 
 */
class ImageType {

    static ActorIcon: string = "ActorIcon";
    static Wallpaper: string = "Wallpaper";
    static Stage: string = "Stage";
    static StageSet: string = "StageSet";

    constructor(imageName: string) {
        this.Type = imageName;
    }
    private Type: string;

    get IsActorIcon(): boolean { return this.Type == ImageType.ActorIcon; }
    get IsBackground(): boolean { return this.Type == ImageType.Wallpaper; }
    get IsStage(): boolean { return this.Type == ImageType.Stage; }
    get IsStageSet(): boolean { return this.Type == ImageType.StageSet; }

    public CreateImageRec(): ImageRec {
        var result = new ImageRec(this.Type);

        if (this.IsActorIcon) {
            result.backgroundsize = 'cover';
            result.backgroundrepeat = 'no-repeat';
            result.backgroundposition = 'top';
        }
        else if (this.IsStage) {
            result.backgroundsize = 'contain';
            result.backgroundrepeat = 'no-repeat';
            result.backgroundposition = 'center';
        }
        else if (this.IsStageSet) {
            result.backgroundsize = 'contain';
            result.backgroundrepeat = 'no-repeat';
            result.backgroundposition = 'center';
        }
        else {
            result.backgroundsize = 'contain';
            result.backgroundrepeat = 'repeat';
            result.backgroundposition = 'center';
        }

        return result;
    }
}