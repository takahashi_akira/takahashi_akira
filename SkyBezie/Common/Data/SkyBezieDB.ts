﻿

class SkyBezieData {
    Room: Room;
    Profiles: Array<Profile>;
    ActorGroups: Array<ActorGroupInfo>;
    StageSets: Array<StageSetInfo>;
    Guides: Array<Guide>;
    Messages: Array<MsgLog>;
    Icons: Array<Icon>;
    Images: Array<ImageRec>;
}


interface OnLoadComplete { (data: SkyBezieData): void }
interface OnWriteComplete { (): void }
interface OnClearComplete { (): void }


class SkyBezieDB extends Database {

    public static SYSTEM: string = 'system';
    public static PROFILE: string = 'profile';
    public static ACTORGROUP: string = 'actorgroup';
    public static STAGESET: string = 'stageset';
    public static GUIDE: string = 'guide';
    public static MESSAGE: string = 'message';
    public static ICON: string = 'icon';
    public static IMAGE: string = 'image';

    constructor() {
        super('skybezie');
        this.SetStoreList(SkyBezieDB.SYSTEM);
        this.SetStoreList(SkyBezieDB.PROFILE);
        this.SetStoreList(SkyBezieDB.ACTORGROUP);
        this.SetStoreList(SkyBezieDB.STAGESET);
        this.SetStoreList(SkyBezieDB.GUIDE);
        this.SetStoreList(SkyBezieDB.MESSAGE);
        this.SetStoreList(SkyBezieDB.ICON);
        this.SetStoreList(SkyBezieDB.IMAGE);
    }


    public ReadAllData(onload: OnLoadComplete) {

        var data = new SkyBezieData();

        //
        //  コールバック地獄・・もっとスマートにしたい所
        //

        //  部屋情報の取得
        this.Read(SkyBezieDB.SYSTEM, SenderType.Room, (result: Room) => {

            data.Room = result;
            LogUtil.Info("Loading Complete : Room");

            //  プロフィールの取得
            this.ReadAll<Profile>(SkyBezieDB.PROFILE, (result: Array<Profile>) => {

                data.Profiles = result;
                LogUtil.Info("Loading Complete : Profiles");

                //  グループ情報の取得
                this.ReadAll<ActorGroupInfo>(SkyBezieDB.ACTORGROUP, (result: Array<ActorGroupInfo>) => {

                    data.ActorGroups = result;
                    LogUtil.Info("Loading Complete : ActorGroups");

                    //  ステージセット情報の取得
                    this.ReadAll<StageSetInfo>(SkyBezieDB.STAGESET, (result: Array<StageSetInfo>) => {

                        data.StageSets = result;
                        LogUtil.Info("Loading Complete : Stageset");

                        //  ガイド情報の取得
                        this.ReadAll<ActorGroupInfo>(SkyBezieDB.GUIDE, (result: Array<Guide>) => {

                            data.Guides = result;
                            LogUtil.Info("Loading Complete : Guides");

                            //  メッセージログの取得
                            this.ReadAll<MsgLog>(SkyBezieDB.MESSAGE, (result: Array<MsgLog>) => {

                                data.Messages = result;
                                LogUtil.Info("Loading Complete : Messages");

                                //  イメージファイルの取得
                                this.ReadAll<Icon>(SkyBezieDB.ICON, (result: Array<Icon>) => {

                                    data.Icons = result;
                                    LogUtil.Info("Loading Complete : Icon");

                                    //  背景画像の取得
                                    this.ReadAll<ImageRec>(SkyBezieDB.IMAGE, (result: Array<ImageRec>) => {

                                        LogUtil.Info("Loading Complete : Image");
                                        data.Images = result;

                                        onload(data);
                                    });

                                });
                            });
                        });
                    });
                });
            });
        });
    }


    /*
     * 
     */
    public ClearAllData(callback: OnClearComplete) {

        this.ClearAll(SkyBezieDB.SYSTEM, () => {
            this.ClearAll<Profile>(SkyBezieDB.PROFILE, () => {
                this.ClearAll<ActorGroupInfo>(SkyBezieDB.ACTORGROUP, () => {
                    this.ClearAll<StageSetInfo>(SkyBezieDB.STAGESET, () => {
                        this.ClearAll<Guide>(SkyBezieDB.GUIDE, () => {
                            this.ClearAll<MsgLog>(SkyBezieDB.MESSAGE, () => {
                                this.ClearAll<Icon>(SkyBezieDB.ICON, () => {
                                    this.ClearAll<ImageRec>(SkyBezieDB.IMAGE, () => {
                                        callback();
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    }



    /*
     * 
     */
    public WriteAllData(data: SkyBezieData, callback: OnWriteComplete) {

        this.Write(SkyBezieDB.SYSTEM, SenderType.Room, data.Room, () => {
            this.WriteAll<Profile>(SkyBezieDB.PROFILE, (k) => k.pid, data.Profiles, () => {
                this.WriteAll<ActorGroupInfo>(SkyBezieDB.ACTORGROUP, (s) => s.groupid, data.ActorGroups, () => {
                    this.WriteAll<StageSetInfo>(SkyBezieDB.STAGESET, (s) => s.sceneid, data.StageSets, () => {
                        this.WriteAll<Guide>(SkyBezieDB.GUIDE, (s) => s.guideid, data.Guides, () => {
                            this.WriteAll<MsgLog>(SkyBezieDB.MESSAGE, (m) => m.mid, data.Messages, () => {
                                this.WriteAll<Icon>(SkyBezieDB.ICON, (c) => c.iid, data.Icons, () => {
                                    this.WriteAll<ImageRec>(SkyBezieDB.IMAGE, (i) => i.uuid, data.Images, () => {
                                        callback();
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    }
}
