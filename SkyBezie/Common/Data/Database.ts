﻿
interface CreateCallBack { (): void };
interface RemoveCallBack { (): void };
interface ReadCallBack<T> { (T): void };
interface WriteGetKey<T> { (T): string };
interface WriteCallBack<T> { (): void };
interface DeleteCallBack<T> { (): void };
interface ClearCallBack<T> { (): void };
interface OnConnect { (): void };
interface OnError { (ev: Event): void };

class Database {

    protected _dbname: string;
    protected _db: IDBDatabase;
    protected _storelist: Array<string>;


    constructor(name: string) {
        this._dbname = name;
        this._storelist = new Array<string>();
    }


    protected SetStoreList(name: string) {
        this._storelist.push(name);
    }


    private OnError(e: Event) {
        LogUtil.Error((<IDBRequest>event.target).error.toString());
    }


    Create(onCreate: CreateCallBack) {
        var rep: IDBOpenDBRequest = window.indexedDB.open(this._dbname, 2);
        rep.onupgradeneeded = (e) => this.CreateStore(e);
        rep.onsuccess = (es) => { onCreate(); };
        rep.onerror = this.OnError;
    }


    private CreateStore(event: IDBVersionChangeEvent) {
        this._db = (<IDBRequest>event.target).result;
        this._storelist.forEach((s) => this._db.createObjectStore(s));
    }


    public Remove(onRemove: RemoveCallBack) {

        if (this._db)
            this._db.close();

        var req = window.indexedDB.deleteDatabase(this._dbname);
        req.onsuccess = (es) => { onRemove(); };
        req.onerror = this.OnError;
    }


    public Connect(onconnect: OnConnect) {
        var rep: IDBOpenDBRequest = window.indexedDB.open(this._dbname, 2);

        rep.onupgradeneeded = (e) => {
            this.CreateStore(e);
        }

        rep.onerror = this.OnError;

        rep.onsuccess = (event) => {
            this._db = (<IDBRequest>event.target).result;
            onconnect();
        };
    }


    Write<T, K>(name: string, key: K, data: T, callback: WriteCallBack<T> = null) {

        var trans = this._db.transaction(name, 'readwrite');
        var store = trans.objectStore(name);

        if (key) {
            var request = store.put(data, key);
            request.onerror = this.OnError;
            if (callback != null) {
                request.onsuccess = (event) => { callback(); }
            }
        }
        else {
            LogUtil.Error("Write key error : Store " + name);
        }
    }


    Delete<T, K>(name: string, key: K, callback: DeleteCallBack<T> = null) {
        var trans = this._db.transaction(name, 'readwrite');
        var store = trans.objectStore(name);
        var request = store.delete(key);
        request.onerror = this.OnError;

        if (callback != null) {
            request.onsuccess = (event) => { callback(); }
        }
    }


    WriteAll<T>(name: string, getkey: WriteGetKey<T>, datalist: Array<T>, callback: WriteCallBack<T> = null) {

        var writefunc = (data) => {

            if (data == undefined) {
                if (callback != null)
                    callback();
            }
            else {
                this.Write(name, getkey(data), data, () => {
                    writefunc(datalist.pop());
                });
            }
        };

        writefunc(datalist.pop());
    }


    Read<T, K>(name: string, key: K, callback: ReadCallBack<T>, onerror: OnError = null) {

        var trans = this._db.transaction(name, 'readonly');
        var store = trans.objectStore(name);
        var request = store.get(key);

        if (onerror)
            request.onerror = this.OnError;
        else
            request.onerror = onerror;

        request.onsuccess = (event) => {

            var result: T = (<IDBRequest>event.target).result as T;
            callback(result);
        };

    }

    ReadAll<T>(name: string, callback: ReadCallBack<Array<T>>) {

        this._db.onerror = this.OnError;
        var trans = this._db.transaction(name, 'readonly');
        var store = trans.objectStore(name);
        var request = store.openCursor();

        var result: Array<T> = new Array<T>();

        request.onerror = this.OnError;
        request.onsuccess = (event) => {
            var cursor = <IDBCursorWithValue>(<IDBRequest>event.target).result;

            if (cursor) {
                var msg = cursor.value as T;
                if (msg)
                    result.push(cursor.value);

                cursor.continue();
            }
            else {
                callback(result);
            }
        };

    }

    ClearAll<T>(name: string, callback: ClearCallBack<T>) {

        var trans = this._db.transaction(name, 'readwrite');
        var store = trans.objectStore(name);
        var request = store.openCursor();

        store.clear();

        request.onerror = this.OnError;
        request.onsuccess = (event) => {
            callback();
        };

    }

}