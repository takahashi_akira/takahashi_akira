﻿


interface OnWriteComplete { (): void }
interface OnClearComplete { (): void }


class SkyBezieCacheDB extends Database {

    public static IMAGE: string = 'image';

    constructor() {
        super('skybezie-cache');
        this.SetStoreList(SkyBezieCacheDB.IMAGE);

        this.Connect(() => {
            LogUtil.Info('skybezie-cache connect');
        });
    }

}
