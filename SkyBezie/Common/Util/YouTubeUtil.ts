﻿
interface OnYouTube { (player: YT.Player): void }


class YouTubeUtil {

    public static Player: YT.Player;
    public static VideoID: string;
    public static Callback: OnYouTube;
    public static IsAPIReady: boolean = false;


    /**
     * YouTubeのID部分を取得します
     * @param url
     */
    public static GetYouTubeID(url: string): string {

        var mat = url.match(/[\/?=]([a-zA-Z0-9\-\_]{11})[&\?]?/);

        if (mat != null && mat.length > 0)
            return mat[1];
        else
            return "";
    }


    /**
     * YouTubeの埋込URLに変換します
     * @param id
     * @param isInputMode
     */
    public static ToEmbedYouTubeID(guide: Guide, isInputMode: boolean): string {

        if (guide === null || !guide.isYouTube)
            return "";

        var id = guide.value;

        if (id != null && id.length >= 0) {

            var url = "";

            url += "https://www.youtube.com/embed/" + id;
            url += "?autoplay=" + (isInputMode ? "1" : "0");
            url += "&rel=0";

            if (isInputMode) {
                if (guide.start_time >= 0) {
                    url += "&start=" + guide.start_time;
                }

                if (guide.end_time >= 0) {
                    url += "&end=" + guide.end_time;
                }

                if (guide.loop) {
                    url += "&loop=1&playlist=" + id;
                }
            }

            return url;
        }
        else {
            return "";
        }

    }


    /**
     * YouTubePlayerクラスを取得します
     * @param videoid
     * @param onYouTube
     */
    public static GetPlayer(videoid: string, onYouTube: OnYouTube) {

        this.VideoID = videoid;
        this.Callback = onYouTube;

        if (this.IsAPIReady) {
            this.CreatePlayer();
        }
        else {
            var tag = document.createElement('script');
            tag.src = "https://www.youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        }
    }


    /**
     * プレイヤークラスを生成します
     */
    public static CreatePlayer() {

        var divid = 'youtube-player-' + Util.CreateUuid();

        if (YouTubeUtil.Player)
            YouTubeUtil.Player.clearVideo();

        $("#youtube-player").empty().append("<div id='" + divid + "'></div>");

        YouTubeUtil.Player = new YT.Player(divid, {
            height: '0',
            width: '0',
            videoId: YouTubeUtil.VideoID,
            events: {
                'onReady': (event: YT.EventArgs) => {

                    if (YouTubeUtil.Callback) {
                        YouTubeUtil.Callback(event.target);
                    }

                }
            }
        });
    }

}

function onYouTubeIframeAPIReady() {
    YouTubeUtil.IsAPIReady = true;
    YouTubeUtil.CreatePlayer();
}
