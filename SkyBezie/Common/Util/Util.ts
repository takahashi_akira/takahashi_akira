﻿
/**
 * 
 */
class Util {

    /**
     * 日付の表示変換
     * @param date
     */
    public static ToDispDate(date: Date) {
        return date.getFullYear()
            + "-" + ("0" + (date.getMonth() + 1)).slice(-2)
            + "-" + ("0" + date.getDate()).slice(-2)
            + " " + ("0" + date.getHours()).slice(-2)
            + ":" + ("0" + date.getMinutes()).slice(-2)
            + ":" + ("0" + date.getSeconds()).slice(-2)
    }

    /**
     * UUIDの生成
     */
    public static CreateUuid(): string {

        var result: string = "";

        for (var i = 0; i < 32; i++) {

            var random: number = Math.random() * 16 | 0;

            if (i == 8 || i == 12 || i == 16 || i == 20)
                result += "-"

            result += (i == 12 ? 4 : (i == 16 ? (random & 3 | 8) : random)).toString(16);
        }
        return result;
    }


    /**
     * XSS対策及び改行コード変換
     * @param msg
     */
    public static ToHtml(msg: string): string {
        msg = this.htmlspecialchars(msg);

        //  HTMLリンクをリンクにする
        var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        msg = msg.replace(exp, "<a target='_blank' href='$1'>$1</a>");

        return msg.replace(/\n/g, '</br>');
    }


    /**
     * XSS対策
     * @param ch
     */
    public static htmlspecialchars(ch: string): string {
        ch = ch.replace(/&/g, "&amp;");
        ch = ch.replace(/"/g, "&quot;");
        ch = ch.replace(/'/g, "&#039;");
        ch = ch.replace(/</g, "&lt;");
        ch = ch.replace(/>/g, "&gt;");
        return ch;
    }


    /**
     * ページへのデフォルトドロップイベントが発生されなくする
     */
    public static StopPropagation() {
        $(document).on('drop dragover', (e) => {
            e.stopPropagation();
            e.preventDefault();
        });
    }


    /**
     * タッチパネルイベントの抑制（スライドでのページ遷移の抑制）
     */
    public static StopTouchmove() {

        if ((window as any).TouchEvent) {

            if (window.addEventListener) {

                function TouchEventFunc(e) {
                    //  e.preventDefault();
                }

                // タッチしたまま平行移動すると実行されるイベント
                document.addEventListener("touchmove", TouchEventFunc);

            }

        }

    }


    /**
     * URLのPeerIDを取得します
     */
    public static GetPeerID(): string {
        return this.GetArgs('peer');
    }


    /**
     * URLのPidを取得します
     */
    public static GetUrlPid(): number {

        var pid = this.GetArgs('pid');

        if (pid)
            return Number(pid);
        else
            return 0;
    }


    /**
     * URLの引数を取得します
     * @param value
     */
    public static GetArgs(value: string): string {

        return this.GetUrlArgs(location.href, value);
    }


    /**
     * URLの引数を取得します
     * @param url
     * @param value
     */
    public static GetUrlArgs(url: string, value: string): string {

        if (url == null || url.length == 0)
            return null;

        var data = url.split('?');

        if (data.length < 2)
            return null;

        var data2 = data[1].split('&');

        for (var i in data2) {
            var data3 = data2[i].split('=');

            if (data3[0] === value)
                return data3[1];
        }

        return null;
    }


    /**
     * ServerURLの生成
     * @param html
     * @param id
     * @param pid
     * @param useOwner
     */
    public static CreateLink(html: string, id: string, pid: number = null, useOwner: boolean = true): string {

        var path = window.location.pathname;
        var lastpos = path.lastIndexOf('/');

        if (lastpos >= 0)
            path = path.substring(0, lastpos + 1);

        var result = window.location.protocol + "//" + window.location.host + path + html;

        //  相対パスを絶対パスに変換させる
        var e: any = document.createElement('span')
        e.insertAdjacentHTML('beforeend', '<a href="' + result + '" />');
        result = e.firstChild.href;

        if (id)
            result += "?peer=" + id;

        if (useOwner) {
            if (id != WebRTCService.peerid)
                result += "&owner=" + WebRTCService.peerid;

            var root = this.GetArgs('owner');
            if (root)
                result += "&root=" + root;

            if (pid)
                result += "&pid=" + pid.toString();
        }

        return result;

    }

}
