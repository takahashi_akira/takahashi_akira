﻿

interface OnImageResize { (img: ImageRec): void }

class ImageUtil {

    /**
     * 画像データのリサイズ
     * @param baseImage
     * @param width
     * @param height
     * @param callback
     */
    public static ImgB64Resize(baseImage: ImageRec, width: number, height: number, callback: OnImageResize) {

        var imgB64_src = baseImage.src;

        // Image Type
        var img_type = imgB64_src.substring(5, imgB64_src.indexOf(";"));

        // Source Image
        var img = new Image();

        img.onload = function () {

            // New Canvas
            var canvas: HTMLCanvasElement = document.createElement('canvas');
            canvas.width = img.width;
            canvas.height = img.height;

            // Draw (Resize)
            var ctx = canvas.getContext('2d');
            ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

            // Destination Image
            var imgB64_dst = canvas.toDataURL(img_type);

            baseImage.src = imgB64_dst;
            callback(baseImage);
        };
        img.src = imgB64_src;
    }


    /**
     * 画像データの圧縮
     * @param baseImage
     * @param callback
     */
    public static ImgB64Compress(baseImage: ImageRec, callback: OnImageResize) {

        //  再起処理
        if (baseImage.src.length > (1024 * 128)) {
            ImageUtil.ImgB64CompressLoop(baseImage, (n) => {
                ImageUtil.ImgB64Compress(n, callback);
            });
        }
        else {
            callback(baseImage);
        }
    }


    /**
     * 画像データのリサイズ
     * @param baseImage
     * @param callback
     */
    public static ImgB64CompressLoop(baseImage: ImageRec, callback: OnImageResize) {

        var imgB64_src = baseImage.src;

        // Image Type
        var img_type = imgB64_src.substring(5, imgB64_src.indexOf(";"));

        // Source Image
        var img = new Image();

        img.onload = function () {

            var W: number = img.width / 2;
            var H: number = img.height / 2;

            var width: number = img.width;
            var height: number = img.height;

            var canvas: HTMLCanvasElement = document.createElement('canvas');
            canvas.width = W;
            canvas.height = H;
            var ctx = canvas.getContext('2d');

            var canvasc2 = document.createElement("canvas");
            var ctx2 = canvasc2.getContext("2d")

            canvasc2.width = width;
            canvasc2.height = height;

            var optw: number = ImageUtil.getOptSize(width, W);
            var opth: number = ImageUtil.getOptSize(height, H);
            ctx2.drawImage(img, 0, 0, width, height, 0, 0, optw, opth);
            width = optw;
            height = opth;

            ctx.drawImage(canvasc2, 0, 0, width, height, 0, 0, W, H);

            // Destination Image
            var imgB64_dst = canvas.toDataURL(img_type);

            baseImage.src = imgB64_dst;
            callback(baseImage);
        };
        img.src = imgB64_src;
    }


    /**
     * 
     * @param orig
     * @param target
     */
    private static getOptSize(orig: number, target: number): number {
        return target * Math.pow(2, Math.floor(Math.log(orig / target) / Math.LN2) - 1);
    }

}