﻿

interface OnChangeImage { (imageRec: ImageRec): void }


/*
 *  このクラスは廃止予定です
 */
class ImageDialog extends ImageDrop {

    static _imageType: ImageType;
    static _imageRec: ImageRec;
    static _callback: OnChangeImage;

    constructor(element: JQuery) {
        super();

        $.get('../Common/ImageDialog/Layout.html', (html) => {
            element.append(html);
            $('#image-dialog-clear').click(() => ImageDialog.Claer());
            $('#image-dialog-delete').click(() => ImageDialog.DeleteClose());
            $('#image-dialog-change').click(() => ImageDialog.ChangeClose());
            $('#image-dialog-cancel').click(() => ImageDialog.Close());
            $('#image-dialog-size').change((e) => ImageDialog.statusChange());
            $('#image-background-repeat').change((e) => ImageDialog.statusChange());
            $('#image-background-position').change((e) => ImageDialog.statusChange());
            this.InitilizeImageDrop($('#image-selector-drop'), $('#image-selector-file'), this.OnDropImage);
        }).done();
    };


    //
    static Open(imageType: string, image: ImageRec, onchange: OnChangeImage) {

        $('#image-selector-file').val('');

        this._imageType = new ImageType(imageType);
        this._callback = onchange;
        var dialog = document.querySelector('dialog');
        var editer = document.getElementById('image-dialog');

        if (editer) {
            (editer as any).showModal();
            this.SetImage(image);
        }
    }


    static SetImage(image: ImageRec) {

        if (image == null) {
            image = this._imageType.CreateImageRec();
        }

        this._imageRec = image

        var div = $('#image-selector-drop');

        div.css("width", "100%").css("height", "100%")

        $('#image-dialog-size').val(image.backgroundsize);
        $('#image-background-repeat').val(image.backgroundrepeat);
        $('#image-background-position').val(image.backgroundposition);

        if (image.src == null || image.src.length == 0) {
            div.css("background", "");
        }
        ImageRec.setCss(div, image);
    }

    //
    static Claer() {
        this.SetImage(this._imageType.CreateImageRec());
    }

    //
    private static statusChange() {

        var dummy = this._imageType.CreateImageRec();
        dummy.src = this._imageRec.src;
        this.setBackGroundStatus(dummy);
        this.SetImage(dummy);
    }

    //
    private static setBackGroundStatus(rec: ImageRec) {
        rec.backgroundsize = $('#image-dialog-size').val();
        rec.backgroundrepeat = $('#image-background-repeat').val();
        rec.backgroundposition = $('#image-background-position').val();
    }


    //
    static DeleteClose() {

        this._imageRec.visible = false;
        ImageDialog._callback(this._imageRec);
        this.Close();
    }


    //
    static ChangeClose() {
        ImageDialog._callback(this._imageRec);
        this.Close();
    }


    //
    static Close() {
        var dialog = document.querySelector('dialog');
        var editer = document.getElementById('image-dialog');

        if (editer)
            (editer as any).close();
    }


    //
    static CreateImageRec(src): ImageRec {
        var rec = this._imageType.CreateImageRec();
        rec.src = src;
        this.setBackGroundStatus(rec);
        return rec;
    }


    //  画像ドロップ時イベント
    OnDropImage(file: File, src) {
        var rec = ImageDialog.CreateImageRec(src);
        ImageDialog.SetImage(rec);
    }

}