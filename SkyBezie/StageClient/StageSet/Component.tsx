﻿/// <reference path="../../Lib/typings/tsd.d.ts" />


class StageSetController {

    public static _element: HTMLElement = document.getElementById('stage_set');
    public static _groupInfoMap = new Map<number, ActorGroupInfo>();
    public static _stageSetInfoMap = new Map<string, StageSetInfo>();
    public static _groupId: number = -1;


    /**
     * グループIDの保持
     * @param id
     */
    public static SetGroupID(id: number) {
        this._groupId = id;
        this.render();
    }


    /**
     * グループ情報を設定します
     * @param group
     */
    public static SetGroup(group: ActorGroupInfoList) {
        group.List.forEach(n => {
            this._groupInfoMap.set(n.groupid, n);
        });
        this.render();
    }


    /**
     * ステージセット情報の設定
     * @param stageset
     */
    public static SetStageSet(stageset: StageSetInfoList) {

        stageset.List.forEach(info => {
            this._stageSetInfoMap.set(info.stagesetid, info);
        });

        this.render();
    }


    /**
     * 
     */
    public static render() {

        var clear = new ImageRec(ImageType.StageSet);
        ReactDOM.render(<StageSetComponent imgrec={clear} />, this._element);

        if (this._groupId < 0)
            return;

        //  グループ情報取得
        var group = this._groupInfoMap.get(this._groupId);
        if (!group)
            return;

        //  ステージセットの取得
        var stage = this._stageSetInfoMap.get(group.stagesetid);
        if (!stage)
            return;

        //  画像情報の取得
        if (stage.image_uuid_list && stage.image_uuid_list.length > 0) {
            var uuid = stage.image_uuid_list[0];
            var imgrec = StageSetCache.GetImageRec(uuid, (imgRec) => {
                ReactDOM.render(<StageSetComponent imgrec={imgRec} />, this._element);
            });
        }

    }

}

interface StageSetProp {
    imgrec: ImageRec
}


class StageSetComponent extends React.Component<StageSetProp, any> {

    /**
     * 
     */
    public render() {

        var img = this.props.imgrec;

        if (img.src !== null && img.src !== undefined) {

            var style = {
                container: {
                    height: '100%',
                    width: '100%',
                    background: 'url(' + img.src + ')',
                    "background-repeat": img.backgroundrepeat,
                    "background-size": img.backgroundsize,
                    "background-position": img.backgroundposition,
                }
            }


            return (
                <div style={style.container}>
                </div>
            );
        }
        else {
            return (<div></div>);
        }

    }

}
