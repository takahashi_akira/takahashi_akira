﻿
class StageClientReceiver extends ServiceReceiver {


    /**
     * 
     * @param sender
     */
    public VideoCursor(sender: VideoCursor): ServiceRecv {
        StageClientUIController.DispVideoMouseCursor(sender);
        return null;
    }


    /**
     * 
     * @param sender
     */
    public StageInfo(sender: StageInfo): ServiceRecv {

        var video = $('video#video');

        if (video) {
            if (sender.dispVideoView) {
                video.show();
            }
            else {
                video.hide();
            }
        }
        return null;
    }


    /**
     * 
     * @param sender
     */
    public IconStore(sender: IconStore): ServiceRecv {

        ActorImageCache.set(sender.Icon);
        return null;
    }


    /**
     * 
     * @param sender
     */
    public ActorGroupInfoList(sender: ActorGroupInfoList): ServiceRecv {
        StageSetController.SetGroup(sender);
        return null;
    }


    /**
     * 
     * @param sender
     */
    public StageSetInfoList(sender: StageSetInfoList): ServiceRecv {
        StageSetController.SetStageSet(sender);
        return null;
    }


    /**
     * 
     * @param sender
     */
    public ImageRec(sender: ImageRec): ServiceRecv {

        StageSetCache.SetImageRec(sender);
        StageSetController.render();
        return null;
    }


    /**
     * 
     * @param msg
     */
    public Message(msg: Message): ServiceRecv {

        if (msg != null) {
            SubtitlesDisplay.Create(msg);
        }

        return null;
    }

}