﻿

class VideoDispOffset {
    dispWidth: number = 0;
    dispHeight: number = 0;
    offsetRight: number = 0;
    offsetTop: number = 0;
    ratio: number = 0;
}


/**
 * 
 */
class StageClientUIController {

    static _video: HTMLVideoElement;

    //
    public static Initialize() {
        Util.StopPropagation();

        this.StageSetting();
        this.VideoSetting();

        //  NewTab押下時の場合は別タブで開く
        $('#new-tab').click((e) => {
            var url = window.location.href;

            if (!url.indexOf("mute")) { url += "&mute=1"; }
            url += "&allout=1";

            window.open(url);
        });


        //  別タブで開かれたステージはサブメニューは表示しない
        if (Util.GetArgs("allout")) {
            $('#submenu').hide();
        }

    }


    /**
     * 
     */
    public static StageSetting() {
        var input_groupid = document.getElementById('groupid') as HTMLInputElement;
        var btm = document.getElementById('change') as HTMLButtonElement;

        btm.onclick = ((ev) => {
            var groupid = Number.parseInt(input_groupid.value);
            this.SetStageSetting(groupid);
        });
    }


    /**
     * 
     * @param groupid
     */
    public static SetStageSetting(groupid: number) {

        StageSetController.SetGroupID(groupid);

    }


    /**
     * ビデオ表示設定
     */
    public static VideoSetting() {
        this._video = document.getElementById('video') as HTMLVideoElement;

        var mute = Util.GetArgs("mute");

        if (mute != null && mute.length > 0)
            this._video.muted = true;

        $('#video').resize((ev) => { this.videoResize(); });
        window.onresize = ((ev) => { this.videoResize(); });
    }


    /**
     *
     */
    public static videoResize() {

        var vdo = this.GetVideoDispOffset(this._video);

        $('#stage_set')
            .css("height", vdo.dispHeight)
            .css("width", vdo.dispWidth)
            .css("top", vdo.offsetTop)
            .css("left", vdo.offsetRight);

        this._baseCursorMap.forEach((cur, key) => {
            this.DispVideoMDispVideoMouseCursorMain(cur);
        });
    }


    /**
     * ビデオのマウスカーソル設定
     */
    public static VideoMouseCursorSetting() {

        var _mouseon: boolean = true;

        var CSP = 'item_port';

        var cursorport = document.getElementById(CSP) as HTMLElement;

        cursorport.onmouseover = (ev: MouseEvent) => {
            _mouseon = true;
        }

        cursorport.onmousemove = (ev: MouseEvent) => {

            var id = ((ev.target) as HTMLElement).id;

            if (_mouseon)
                this.VideoMouseCursor(this._video, cursorport, ev);
        };

        cursorport.onmouseout = (ev: MouseEvent) => {

            var id = ((ev.target) as HTMLElement).id;

            _mouseon = false;
            this.VideoMouseCursor(this._video, cursorport, ev);
        };
    }


    /**
     * ビデオカーソル生成
     * @param video
     * @param cursorpost
     * @param mv
     */
    public static VideoMouseCursor(video: HTMLVideoElement, cursorpost: HTMLElement, mv: MouseEvent) {

        var vdo = this.GetVideoDispOffset(video);

        //  offsetXY → ClientXYに変更（CursorのDiv上の移動イベントを取得したい為）
        var posX: number = Math.round((mv.clientX - vdo.offsetRight) / vdo.ratio);
        var posY: number = Math.round((mv.clientY - vdo.offsetTop) / vdo.ratio);

        var sender = this.CreateVideoCursor();

        if (posX >= 0 && posX <= video.videoWidth && posY >= 0 && posY <= video.videoHeight) {
            sender.posX = posX;
            sender.posY = posY;
        }
        else {
            sender.posX = -1;
            sender.posY = -1;
        }

        if (this._busy) {
            this._queue = sender;
        }
        else {
            this._busy = true;
            WebRTCService.OwnerSend(sender);
        }
    }


    /**
     * ビデオカーソル生成
     */
    private static CreateVideoCursor(): VideoCursor {

        var sender = new VideoCursor();

        //  表示しているClientページから設定されるIDがある場合、それを優先する
        var pid = (document.getElementById('pid') as HTMLInputElement).value;
        var iid = (document.getElementById('iid') as HTMLInputElement).value;

        if (pid && pid.length > 0) {
            sender.pid = Number.parseInt(pid);
            sender.iid = Number.parseInt(iid);
        }
        else {
            //  無い場合は起動元のIDを設定する
            sender.pid = Util.GetUrlPid();
            sender.iid = 0;
        }

        return sender;
    }


    private static _busy: boolean = false;
    private static _queue: VideoCursor = null;
    private static _cursorMap = new Map<number, DispCursor>();
    private static _baseCursorMap = new Map<number, VideoCursor>();


    /**
     * ステージ上にマウスカーソルを表示
     * @param cursor
     */
    public static DispVideoMouseCursor(cursor: VideoCursor) {
        this._baseCursorMap.set(cursor.pid, cursor);
        this.DispVideoMDispVideoMouseCursorMain(cursor);
    }


    /**
     * ステージ上にマウスカーソルを表示
     * @param cursor
     */
    private static DispVideoMDispVideoMouseCursorMain(cursor: VideoCursor) {

        var vdo = this.GetVideoDispOffset(this._video);

        var cursorX = Math.round(cursor.posX * vdo.ratio + vdo.offsetRight);
        var cursorY = Math.round(cursor.posY * vdo.ratio + vdo.offsetTop);

        var dispCursor = new DispCursor(cursor.pid, cursor.iid, cursor.name, cursorX, cursorY);
        this._cursorMap.set(cursor.pid, dispCursor);

        var cursorArray = new Array<DispCursor>();
        this._cursorMap.forEach((value, key) => cursorArray.push(value));

        CursorDisplay.Create(document.getElementById("cursor_port"), cursorArray);

        //  ququeがある場合、最後に送信する
        if (this._queue !== null && this._queue.pid === cursor.pid) {
            WebRTCService.OwnerSend(this._queue);
            this._queue = null;
        }
        else {
            this._busy = false;
        }
    }


    /**
     * Videoの実表示エリア計算
     * @param video
     */
    protected static GetVideoDispOffset(video: HTMLVideoElement): VideoDispOffset {

        var videoWidth: number = video.videoWidth;
        var videoHeight: number = video.videoHeight;
        var videoAspect: number = videoWidth / videoHeight;

        var divWidth: number = video.clientWidth;
        var divHeight: number = video.clientHeight;
        var divAscpet: number = divWidth / divHeight;

        var result = new VideoDispOffset();

        if (divAscpet > videoAspect) {
            //  divが横に長い場合・・・
            result.dispHeight = divHeight;
            result.dispWidth = result.dispHeight * videoAspect;
            result.offsetTop = 0;
            result.offsetRight = (divWidth - result.dispWidth) / 2;
            result.ratio = result.dispHeight / videoHeight;
        }
        else {
            //  divが縦に長い場合
            result.dispWidth = divWidth;
            result.dispHeight = result.dispWidth / videoAspect;
            result.offsetRight = 0;
            result.offsetTop = (divHeight - result.dispHeight) / 2;
            result.ratio = result.dispWidth / videoWidth;
        }

        return result;
    }

}