﻿/// <reference path="../../Lib/typings/tsd.d.ts" />

/**
 * 字幕表示
 */
class SubtitlesDisplay {

    /**
     * 字幕の表示
     * @param msg
     */
    public static Create(msg: Message) {

        var element = document.getElementById('subtitles') as HTMLElement;

        ReactDOM.render(<SubtitlesComponent Message={msg} />, element);
    }

}

interface SubtitlesItemrProp {
    Message: Message;
}

/**
 * 字幕表示メイン
 */
class SubtitlesComponent extends React.Component<SubtitlesItemrProp, any>{


    /**
     * 
     */
    public render() {

        var msg = this.props.Message.text;

        if (msg == null || msg.length == 0) {
            return (<div></div>);
        }
        else {
            return (<div className='subtitles-text'>{msg}</div>);
        }
    }
}