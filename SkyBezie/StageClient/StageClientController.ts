﻿

class StageClientController extends ServiceController {

    /**
     * コンストラクタ
     */
    constructor() {
        super();
        this.Receiver = new StageClientReceiver(this);
    };


    /**
     * 自身のPeer生成時イベント
     */
    public OnPeerOpen(peer: PeerJs.Peer) {
        var link = Util.CreateLink('index.html', peer.id);
        $('#stage-background').text(link);
    }


    /**
     * オーナー接続時イベント
     */
    public OnOwnerConnection() {

        //  ステージ情報を取得
        WebRTCService.OwnerSend(new Sender(SenderType.GetStage));

        //  カーソル表示の初期化はOwnerとの接続後に開始する。
        StageClientUIController.VideoMouseCursorSetting();
    }

};
