﻿/// <reference path="../../Lib/typings/tsd.d.ts" />


class DispCursor {

    constructor(pid: number, iid: number, name: string, x: number, y: number) {
        this.pid = pid;
        this.iid = iid;
        this.name = name;
        this.posX = x;
        this.posY = y;
    }

    pid: number = 0;
    iid: number = 0;
    posX: number = 0;
    posY: number = 0;
    name: string = null;
}


class CursorDisplay {

    /**
     * カーソル表示
     * @param element
     * @param value
     */
    public static Create(element: HTMLElement, value: Array<DispCursor>) {

        ReactDOM.render(<CursorListComponent CursorList={value} />, element);


        value.forEach((cur) => {
            ActorImageCache.findSetCss(cur.iid);
        });

    }
}

interface CursorListItemrProp {
    cursor: DispCursor;
}

/**
 * カーソル表示
 */
class CursorListItemComponent extends React.Component<CursorListItemrProp, any>{


    /**
     * 
     */
    public render() {

        var cursor = this.props.cursor;
        //  var a = ActorList.FindProfile(cursor.pid);

        if (cursor.posX >= 0 && cursor.posY >= 0) {
            var style = {
                container: {
                    left: cursor.posX,
                    top: cursor.posY,
                    width: "48px",
                    height: "48px",
                }
            }

            var icon = ActorImageCache.get(cursor.iid);

            var imgstyle = {
                container: {
                    "background": "url(../Common/Img/cursor.png)",
                    "background-size": "cover",
                    "background-repeat": "no-repeat",
                    "background-position": "center",
                    "height": "100%",
                    "width": "100%"
                }
            }

            if (icon && icon.img) {

                var rec = icon.img;
                imgstyle = {
                    container: {
                        background: "url(" + rec.src + ")",
                        "background-size": rec.backgroundsize,
                        "background-repeat": rec.backgroundrepeat,
                        "background-position": rec.backgroundposition,
                        "height": "100%",
                        "width": "100%"
                    }
                }
            }

            var imgClassName = 'actor-img-' + cursor.iid.toString();
            var idName = 'cursor-' + cursor.pid.toString();

            return (
                <div>
                    <div id={idName} className='cursor' style={style.container}>
                        <div className={imgClassName} classID={imgClassName} style={imgstyle.container}>
                            <p className="cursor_name">{cursor.name}</p>
                        </div>
                    </div>
                </div>
            );
        }
        else {
            return (<div></div>);
        }
    }
}



interface CursorListProp {
    CursorList: Array<DispCursor>;
}


class CursorListComponent extends React.Component<CursorListProp, any> {

    /**
     * カーソルの描画
     */
    public render() {

        var commentNodes = this.props.CursorList.map((cur) => {
            return (<CursorListItemComponent cursor={cur} />);
        });

        return (
            <div>
                {commentNodes}
            </div>
        );
    }

}
