﻿


class Login {


    //
    private static _profile: Profile = null;


    //
    private static _roomInfo: RoomInfo = null;


    /**
     * 初期化処理
     * @param roomInfo
     */
    public static Initilize(roomInfo: RoomInfo) {

        Login._roomInfo = roomInfo;
        Login.Open();

        $('#login-button').click(() => this.Login(this.Get()));
        $('#login-name').keydown((e) => this.OnKeyDown(e));
        $('#login-password').keydown((e) => this.OnKeyDown(e));

        //  前回のログインユーザーを表示する
        $('#login-name').val(LocalCache.LoginName);
    };


    public static get Succeed() {
        return (this._profile != null);
    }


    public static get Profile() {
        return this._profile;
    }


    public static set Profile(Profile: Profile) {
        this._profile = Profile;
    }


    /**
     * キー押下時イベント
     * エンターキー押下が検出された場合ログイン処理を実行する
     * @param e
     */
    public static OnKeyDown(e: JQueryKeyEventObject) {

        if (e.keyCode == 13) {
            this.Login(this.Get());
        }
    }


    /**
     * ログイン情報の取得
     */
    public static Get(): LoginInfo {
        var result = new LoginInfo();
        result.name = $('#login-name').val();

        //  MD5に変換する
        result.md5 = $('#login-password').val();
        return result;
    };


    /**
     * ログイン情報の設定
     * @param info
     */
    public static Set(info: LoginInfo) {
        $('#login-name').val(info.name);
    };


    /**
     * ログインダイアログの表示
     * @param message
     */
    public static Open(message: string = null) {

        this._profile = null;

        var dialog = document.querySelector('dialog');
        var editer = document.getElementById('login-dialog');

        if (this._roomInfo.roomName && this._roomInfo.roomName.length > 0) {
            var txt = '「' + this._roomInfo.roomName + "」に招待されています。";

            $('#login-room-name').text(txt);
        }

        if (message != null && message.length > 0)
            $('#login-error').text(message);

        if (editer) {
            (editer as any).showModal();
        }

    }


    /**
     * ダイアログのクローズ
     */
    public static Close() {
        var editer = document.getElementById('login-dialog');

        if (editer)
            (editer as any).close();
    }


    /**
     * ログイン処理
     * @param loginfo 
     */
    public static Login(loginfo: LoginInfo) {

        LocalCache.LoginName = loginfo.name;
        WebRTCService.OwnerSend(loginfo);

        this.Close();
    };

}