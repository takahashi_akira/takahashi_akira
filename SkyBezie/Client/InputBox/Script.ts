﻿class Inputbox {

    public static Pid: number = 0;

    //
    public static Initilize() {

        ActorList.Initilize();

        $('#input-box-inputing').text('　'); //  レイアウト調整用

        var textbox = $('#input-box-text');

        //  アイコンクリック時イベント
        $('#input-box-select-img').click((e) => {
            ActorIconList.Open();
        });

        //  Sendボタンクリック時イベント
        $('#input-send').click((e) => {
            if (this.IsInput()) {
                this.Send();
                textbox.focus();
            }
        });


        textbox.keypress((e) => {


            if (e.ctrlKey) {

                var org_event: any = e.originalEvent;

                if (org_event.code == 'KeyM') {
                    //  ActorEditer.Open(ActorList.selcharid);
                    e.preventDefault();
                }
                else if (org_event.code == 'KeyI') {

                    ActorIconList.Open();
                    e.preventDefault();
                }
            }
            else if (e.keyCode == 13) {
                if (e.shiftKey) {
                    $.noop();
                } else if (this.IsInput()) {
                    e.preventDefault();
                    this.Send();
                }
            }
            else if (e.keyCode == 27) {
            }
        });

        textbox.keydown((e) => {

            if (e.keyCode == 9) {
                e.returnValue = false;

                this.ClearInputing();

                if (e.shiftKey)
                    ActorList.MoveNext(-1);
                else
                    ActorList.MoveNext(1);

                return false;
            }
            else if (e.keyCode == 27) {
                e.returnValue = false;
                $('#input-box-text').val('');
            }
        });

        textbox.keyup((e) => {

            this.SendInputing();

        });

    };


    public static IsInput(): boolean {
        return ($('#input-box-text').val().replace(/\s/g, "").length > 0);
    }

    //
    public static Send() {
        WebRTCService.OwnerSend(this.Get());
        this.Clear();
    }


    public static Clear() {
        $('#input-box-text').val('');
    };

    //
    public static Get(): Message {
        var result = new Message();
        result.pid = this.Pid;
        result.iid = ActorIconList.SelImageId;
        result.text = $('#input-box-text').val();
        return result;
    };

    //
    public static SetCharactor(char: Profile) {
        $('#input-box-charname').text(char.name);
        $('#input-box-text').focus();
    }

    public static Focus() {
        $('#input-box-text').focus();
    }


    //
    public static ClearInputing() {
        var ips = new Ing();
        ips.pid = this.Pid;
        ips.status = false;
        WebRTCService.OwnerSend(ips);
    }


    //
    public static SendInputing() {
        var ips = new Ing();
        ips.pid = this.Pid;
        ips.status = ($('#input-box-text').val().length > 0);
        WebRTCService.OwnerSend(ips);
    }


    //  
    public static SetActorImage(profile: Profile) {
        this.Pid = profile.pid;
        $('#input-box-select-img').empty().append("<div class='actor-img-" + profile.iid + "'></div>");
        ActorImageCache.findSetCss(profile.iid);
        ActorIconList.Set(profile);
    }


    //  入力中の表示
    public static SetInputing(ings: Ings) {

        var disp: string = "";

        ings.pids.forEach((id) => {
            disp += " " + ActorList.FindProfile(id).name;
        });

        if (disp.length > 0)
            disp = "入力中 " + disp;
        else
            disp = "　";

        $('#input-box-inputing').text(disp);
    }

}