﻿/// <reference path="../../Lib/typings/tsd.d.ts" />


/**
 *  ステージ
 */
class StageList {

    private static _element: HTMLElement;
    private static _value: StageServiceList;


    /**
     * ステージの表示
     * @param element
     * @param value
     */
    public static Create(element: HTMLElement, value: StageServiceList) {

        this._element = element;
        this._value = value;

        ReactDOM.render(<StageListComponent stageList={value} />, element);
    }


    /**
     * ステージの再描画
     */
    public static Reflash() {

        if (this._element && this._value)
            this.Create(this._element, this._value);

    }

}


/**
 *  ステージリスト
 */
interface StageListProp {
    stageList: StageServiceList;
}


/**
 *  ステージリストコンポーネント
 */
class StageListComponent extends React.Component<StageListProp, any> {


    /**
     * ステージの描画
     */
    public render() {

        //  表示するステージの件数
        var dispCount = 0;

        this.props.stageList.List.map((stg) => {
            if (stg.enabled && !stg.hide) { dispCount++; }
        });

        //  一件のみの場合は最大表示にする
        var isOne = (dispCount <= 1);


        var commentNodes = this.props.stageList.List.map((stage) => {
            return (<StageItemComponent stage={stage} isOne={isOne} />);
        });


        return (
            <div className="mdl-grid">
                {commentNodes}
            </div>
        );

    }

}


/**
 *  
 */
interface StageItemrProp {
    stage: StageService,
    isOne: boolean
}


/**
 *  
 */
class StageItemComponent extends React.Component<StageItemrProp, any>{


    /**
     * ステージアイテムの描画
     */
    public render() {

        var stage = this.props.stage;
        var actor = ActorList.FindProfile(stage.pid);
        var url = this.GetStageUrl();

        var cellSize = (this.props.isOne ? "12" : "6");
        var cellClass = "mdl-cell mdl-cell--" + cellSize + "-col";
        var frameClass = "frame-" + cellSize + (ClientUIController.Layout.dispMenu ? "" : "-nomenu");

        return (
            <div className={cellClass} hidden={this.props.stage.hide}>
                <iframe className={frameClass} src={url}></iframe>
            </div >
        );
    }


    /**
     * 自身のクライアントステージか？
     */
    public isMineStage(): boolean {
        return (WebRTCService.peerid == Util.GetUrlArgs(this.props.stage.url, 'root'));
    }


    /**
     *  ステージURL
     */
    public GetStageUrl(): string {

        var curUrl = this.props.stage.url + "&pid=" + Login.Profile.pid;

        //  自分のステージの場合、ミュートにする
        if (this.isMineStage()) {
            curUrl += "&mute=1";
        }

        return curUrl;
    }

}
