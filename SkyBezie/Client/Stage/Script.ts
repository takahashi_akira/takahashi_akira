﻿
class Stage {


    private static _profile: Profile = null;
    private static _group: ActorGroupInfo = null;


    /**
     * 
     * @param profile
     */
    public static SetProfile(profile: Profile) {
        this._profile = profile;
        this.SetStageIcon();
    }


    /**
     * 
     * @param info
     */
    public static SetGroup(info: ActorGroupInfo) {
        this._group = info;
        this.SetStageIcon();
    }



    /**
     * 
     */
    public static SetStageIcon() {
        if (this._group == null || this._profile == null)
            return;

        var frame = document.getElementById('stage') as HTMLIFrameElement;

        if (!frame)
            return;

        if (frame.getAttribute('src') !== this._group.stageurl) {
            //  frame.setAttribute('src', info.stageurl);
        }

        var doc = frame.contentWindow.document;

        var textpid = doc.getElementById('pid') as HTMLInputElement;
        var textiid = doc.getElementById('iid') as HTMLInputElement;
        var textgid = doc.getElementById('groupid') as HTMLInputElement;
        var button = doc.getElementById('change') as HTMLButtonElement;

        if (textpid)
            textpid.value = this._profile.pid.toString();

        if (textiid)
            textiid.value = this._profile.iid.toString();

        if (textgid)
            textgid.value = this._group.groupid.toString();

        if (button)
            button.click();
    }


}