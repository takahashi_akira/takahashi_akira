﻿
/**
 * 
 */
class ClientController extends ServiceController {


    /**
     * 
     */
    constructor() {
        super();
        this.Receiver = new ClientReceiver(this);
    };


    /**
     * 
     */
    public OnOwnerConnection() {
        WebRTCService.OwnerSend(new Sender(SenderType.GetRoomInfo));
    }


    /**
     * 
     */
    public OnOwnerClose() {
        ClientUIController.Disconnect();
    }


    /**
     * 
     * @param err
     */
    public OnPeerError(err: Error) {
        ClientUIController.Disconnect();
        LogUtil.Error('peer error');
        LogUtil.Error(err.message);
        LogUtil.FatalError(err.message);
    }

}
