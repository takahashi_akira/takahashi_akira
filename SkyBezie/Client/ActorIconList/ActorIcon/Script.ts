﻿
class ActorIcon {

    private _profile: Profile;
    public _iid: number;

    constructor(div: JQuery, profile: Profile, iid: number) {

        this._profile = profile;
        this._iid = iid;

        var html: string = "";
        html += "<div class='actoricon' id='actoricon-{iid}' tabIndex='0'>";
        html += "<div class='actoricon-img' id='actoricon-img-{iid}'>";
        html += "<div class='actor-img-{iid}'></div></div></div>";
        html = html.replace(/{pid}/g, profile.pid.toString());
        html = html.replace(/{iid}/g, iid.toString());

        div.append(html);

        var icon = $('#actoricon-' + iid.toString());

        icon.dblclick((e) => {
            ActorIcon.OpenEditDialog(this._profile.pid, this._iid);
        });

        icon.keypress((e) => {

            if (e.keyCode == 13) {
                ActorIconList.Close();
                e.preventDefault();
            }
            else if (e.keyCode == 37) {
                //  ←
                e.shiftKey = true;
                e.keyCode = 9;
            }
            else if (e.keyCode == 39) {
                //  →
                e.keyCode = 9;
            }
            else {
                //  その他のキー入力でも閉じる
                ActorIconList.Close();
                e.preventDefault();
            }

        });

        icon.focusin(() => {
            ActorList.SetImage(this._profile.pid, this._iid);
            ActorIconList.Selection = this;
        });
    }


    //
    //
    public static OpenNewDialog(pid: number) {
        this.OpenEditDialog(pid, 0);
    }


    //
    //
    public static OpenEditDialog(pid: number, iid: number) {

        var imgRec: ImageRec = null;

        if (iid != 0) {
            var actorImage = ActorImageCache.get(iid);
            imgRec = actorImage.img;
        }

        ImageDialog.Open(ImageType.ActorIcon, imgRec, (result) => {

            //  設定された画像は圧縮してから登録
            ImageUtil.ImgB64Compress(result, (rsrc) => {

                var image = new Icon();
                image.pid = pid;
                image.iid = iid;
                image.img = rsrc;

                var store = new IconStore();
                store.Icon = image;
                WebRTCService.OwnerSend(store);
            });

        });
    }


    public get Element(): JQuery {
        return $('#actoricon-' + this._iid.toString());
    }

    public Select() {
        this._profile.iid = this._iid;

        this.Element.css('border', '4px solid #263ABF');
        ActorList.SetImage(this._profile.pid, this._iid);
    }

    public Focus() {
        this.Element.focus();
    }

    public ClearSelect() {
        this.Element.css('border', '4px solid #333');
    }


}