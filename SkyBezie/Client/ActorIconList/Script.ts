﻿
class ActorIconList {

    private static _profile: Profile;
    private static _boxlist: Array<ActorIcon> = new Array<ActorIcon>();
    private static _selection: ActorIcon = null;
    private static _visible = false;


    public static Initilize() {

        $('#act-iconlist-background').click((ev: JQueryEventObject) => {
            if (ev.target.id == 'act-iconlist-background')
                ActorIconList.Close();
        });

        $('#iconlist-close').click((e) => this.Close());

        $('#act-icon-update').click(() => {
            ActorIcon.OpenEditDialog(this._profile.pid, this._selection._iid);
        });

    };

    public static Open() {

        var label = "'" + this._profile.name + "' のアイコンの選択・編集"
        $("#act-iconlist-label").text(label);

        $("div#actoriconlist").width($('div#contents_inputbox').width() - 25);
        var width = $("div#actoriconlist").width();

        $('#act-iconlist-background').show();

        if (this._selection)
            this._selection.Focus();
    }


    public static Close() {
        $('#act-iconlist-background').hide();
        Inputbox.Focus();
    }

    public static get SelImageId(): number {
        if (this._selection)
            return this._selection._iid;
        else
            return 0;
    }


    private static _preid: number = 0;


    public static Set(profile: Profile) {

        if (!Login.Profile)
            return;

        this._profile = profile;

        if (this._preid != this._profile.pid) {
            var send = new GetIconIdList(profile.pid);
            WebRTCService.OwnerSend(send);
            this._preid = this._profile.pid;
        }
    }

    public static SetProfileList(list: IconIdList) {

        var cur: ActorIcon;

        var div = $('#actoriconlist-box');
        div.empty();

        this._boxlist = new Array<ActorIcon>();

        list.iidlist.sort((a: number, b: number) => {
            //  Numberのソート
            if (a == b) return 0; else return ((a > b) ? 1 : -1);
        }).forEach((iid) => {
            var icon = new ActorIcon(div, this._profile, iid);

            this._boxlist.push(icon);

            if (iid == this._profile.iid)
                cur = icon;
        });

        //  追加ボタン
        div.append("<div class='icon' id='newIcon'></div>");
        $('#newIcon').click((e) => { ActorIcon.OpenNewDialog(this._profile.pid); });

        if (cur) {
            this.Selection = cur;
        }
        else if (this._boxlist.length > 0) {
            this.Selection = this._boxlist[0];
        }
        else {
            this.Selection = null;
        }

        //  画像の再描画
        this._boxlist.forEach((box) => {
            ActorImageCache.findSetCss(box._iid);
        });

    }


    //  設定：選択キャラクターボックス
    public static set Selection(sel: ActorIcon) {
        this._boxlist.forEach(box => box.ClearSelect());
        this._selection = sel;

        if (sel) {
            sel.Select();
        }
        else {
            //  アイコンが全て削除された場合
            ActorList.SetImage(this._profile.pid, 0);
        }
    }

}