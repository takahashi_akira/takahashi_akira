﻿
class ClientReceiver extends ServiceReceiver {


    /**
     * 文字入力中ステータス
     * @param sender
     */
    public Ings(sender: Ings): ServiceRecv {
        Inputbox.SetInputing(sender);
        return null;
    }


    /**
     * 部屋情報
     * @param sender
     */
    public RoomInfo(sender: RoomInfo): ServiceRecv {
        ClientUIController.SetRoomInfo(sender);
        return null;
    }


    /**
     * タイムラインメッセージ
     * @param sender
     */
    public MsgSotre(sender: MsgStore): ServiceRecv {

        //  ログイン前は処理しない
        if (Login.Succeed) {
            if (sender.Messages.length > 0) {
                Timeline.SetMessages(sender.Messages, true);
            }
        }

        return null;
    }


    /**
     * ログイン情報
     * @param sender
     */
    public LoginResult(sender: LoginResult): ServiceRecv {

        if (sender.succeed) {

            Login.Profile = sender.myProfile;
            ClientUIController.LoginSetup(sender.myProfile);

            this.ProfileList(sender.Profiles);

            if (sender.store.Messages.length > 0)
                Timeline.SetMessages(sender.store.Messages, false);

            ActorList.selcharid = sender.myProfile.pid;
        }
        else {
            Login.Open(sender.message);
        }

        return null;
    }


    /**
     * 接続者のプロフィール情報
     * @param sender
     */
    public ProfileList(sender: ProfileList): ServiceRecv {

        ActorList.Set(sender);
        LoginList.SetProfile(sender);
        return null;
    }


    /**
     * 配信ステージリスト
     * @param sender
     */
    public StageServiceList(sender: StageServiceList): ServiceRecv {

        var element = document.getElementById('contents_stagelist');

        //  ダイアログを閉じる
        $('#stage_page').trigger("click");

        StageList.Create(element, sender);
        return null;
    }


    /**
     * アイコン情報　※廃止予定（画像データに統合予定）
     * @param sender
     */
    public IconStore(sender: IconStore): ServiceRecv {

        ActorImageCache.set(sender.Icon);
        return null;
    }


    /**
     * アイコン一覧　※廃止予定
     * @param sender
     */
    public IconIdList(sender: IconIdList): ServiceRecv {
        ActorIconList.SetProfileList(sender);
        return null;
    }


    /**
     * 壁紙情報　※廃止予定（画像データに統合予定）
     * @param sender
     */
    public WallpaperInfo(sender: WallpaperInfo): ServiceRecv {

        if (sender.Image != null) {
            ImageRec.setCss($('#contents_main'), sender.Image);
        }

        return null;
    }


    /**
     * メッセージグループ情報
     * @param sender
     */
    public ActorGroupInfoList(sender: ActorGroupInfoList): ServiceRecv {
        LoginList.SetGroup(sender);
        return null;
    }


    /**
     * レイアウト設定
     * @param sender
     */
    public Layout(sender: Layout): ServiceRecv {

        //
        ClientUIController.Layout = sender;

        //  メニューの表示制御
        var elementHeader = document.getElementById("skybezie-header");
        this.SetDisplay(elementHeader, sender.dispMenu);

        //  接続メンバーの表示制御
        var elementActors = document.getElementById("skybezie-actors-cell");
        this.SetDisplay(elementActors, sender.dispActors);

        //  タイムラインの表示制御
        var elementTimeline = document.getElementById("skybezie-timeline-cell");
        this.SetDisplay(elementTimeline, sender.dispMessage);

        //  タイムラインのCSS設定
        document.getElementById("timeline").className = (sender.dispMenu ? "timeline" : "timeline_nomenu");

        //  
        elementActors.className = (sender.dispActors ? "mdl-cell mdl-cell--2-col" : "");
        elementTimeline.className = (sender.dispMessage ? "mdl-cell mdl-cell--2-col" : "");

        //
        var stagecell = 12;
        if (sender.dispActors) stagecell -= 2;
        if (sender.dispMessage) stagecell -= 2;

        document.getElementById("skybezie-stage-cell").className = "mdl-cell mdl-cell--" + stagecell.toString() + "-col";

        //
        StageList.Reflash();

        return null;
    }


    /**
     * 表示・非表示設定のプロパティ
     * @param isDisp
     */
    private SetDisplay(element: HTMLElement, isDisp: boolean) {
        element.style.display = (isDisp ? "block" : "none");
    }

}