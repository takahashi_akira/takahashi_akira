﻿
class Actor {

    private _profile: Profile;

    constructor(div: JQuery, profile: Profile, iid: number) {

        this._profile = profile;

        var html: string = "";
        html += "<div class='actor' id='actor-{pid}'>";
        html += "<div class='actor-name' id='actor-name-{pid}'>{name}";
        html += "<div class='actor-status' id='actor-status-{pid}'>{status}</div>";
        html += "</div></div>";

        html = html.replace(/{pid}/g, profile.pid.toString());
        html = html.replace(/{iid}/g, iid.toString());
        html = html.replace(/{name}/g, Util.ToHtml(profile.name));
        html = html.replace(/{status}/g, Util.ToHtml(profile.status));
        div.append(html);

        this.element
            .click((e) => this.selection(profile.pid))
        //  .dblclick((e) => { ActorEditer.Open(this._profile.pid); });
    }


    get Profile(): Profile {
        return this._profile;
    }


    set Profile(profile: Profile) {
        this._profile = profile;
        $('#actor-status-' + this._profile.pid).text(Util.ToHtml(profile.status));
    }

    selection(pid: number) {
        ActorList.selcharid = pid;
    }

    set ImageId(iid: number) {

        if (this._profile) {
            this._profile.iid = iid;
        }
    }

    get element(): JQuery {
        return $('#actor-' + this._profile.pid);
    }

    get nameElement(): JQuery {
        return $('#actor-name-' + this._profile.pid);
    }

    select() {
        ActorList.selcharid = this.Profile.pid;
        Inputbox.SetCharactor(this._profile);
        this.nameElement.css('background-color', '#000066');
    }

    clearSelect() {
        this.nameElement.css('background-color', '#555555');
    }

    remove() {
        this.element.remove();
    }

}