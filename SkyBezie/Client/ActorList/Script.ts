﻿
class ActorList {

    private static _selection_pid: number;
    private static _profilelist: ProfileList;
    private static _boxlist: Array<Actor> = new Array<Actor>();

    static Initilize() {
        //  $('#newActor').click((e) => { ActorEditer.Open(0) });
    }


    //
    static Set(profiles: ProfileList) {

        if (!Login.Profile)
            return;

        //  選択アクターの保持
        var selid = this._selection_pid;

        //  プロフィールリストは
        //  他ユーザーを含む全アクターの情報が通知される
        this.profileMarge(ActorList._profilelist, profiles);
        ActorList._profilelist = profiles;


        profiles.List.forEach(p => {

            //  表示するのは自分のデータのみ
            if (p.owner == Login.Profile.pid) {

                //  アクターが追加または削除された場合
                //  選択アクターを変更する
                var changeid = this.createActorBox(p, $('#actorlist-box'));

                //  0が返された場合は変更無し
                if (changeid > 0)
                    selid = changeid;
            }
        });

        //  コンボボックスの修正
        Inputbox.SetActorImage(this.FindProfile(selid));


        //  選択キャラクターの復元
        this.selcharid = selid;
    }


    //
    private static profileMarge(preList: ProfileList, newList: ProfileList) {

        if (!preList)
            return;

        //  選択アイコンはオーナー側に保存されていないので保持されているデータから取得する
        preList.List.forEach((p) => {

            if (p.owner == Login.Profile.pid) {
                newList.List.forEach((n) => {
                    if (p.pid == n.pid)
                        n.iid = p.iid;
                });
            }
        });
    }


    //  
    private static createActorBox(profile: Profile, div: JQuery): number {


        var preIndex = this.FindActorBoxIndex(profile.pid);

        if (preIndex < 0) {

            //  未追加かつ未削除の場合は、Actor表示を追加する
            if (profile.visible) {
                this._boxlist.push(new Actor(div, profile, profile.iid));
                ActorImageCache.findSetCss(profile.iid);
                return profile.pid;
            }
            else {
                return 0;
            }
        }
        else {

            if (profile.visible) {
                //  プロフィールの更新の場合、書換える
                this._boxlist[preIndex].Profile = profile;
                return 0;
            }
            else {
                //  削除された場合、要素を非表示にしリストから削除する
                this._boxlist[preIndex].remove();
                this._boxlist.splice(preIndex, 1);
                //  削除後の選択値は、要素の先頭とする
                return this._boxlist[0].Profile.pid;
            }
        }
    }


    //
    private static FindActorBoxIndex(pid: number): number {

        var pos: number = 0;
        var result: number = -1;

        this._boxlist.forEach((box) => {

            if (box.Profile.pid == pid)
                result = pos;

            pos++;
        });

        return result;
    }



    //  設定：選択キャラクターID
    static set selcharid(pid: number) {

        if (pid == this._selection_pid) {
            return;
        }
        else {

            this._boxlist.forEach((box) => {

                box.clearSelect();
                if (box.Profile.pid == pid) {
                    this._selection_pid = pid;
                    box.select();
                    Inputbox.SetActorImage(box.Profile);
                    LoginList.SetPid(pid);
                }

            });
        }
    }


    //  取得：選択キャラクターID
    static get selcharid(): number {
        return this._selection_pid;
    }


    //  キャラクター番号からキャラクターを取得します。
    static FindProfile(pid: number): Profile {

        var result: Profile = null;

        this._profilelist.List.forEach((p) => {
            if (p.pid == pid)
                result = p;
        });

        return result;
    }


    //  選択キャラクターを次へ移動します
    static MoveNext(movecnt: number = 1) {

        var selid = this.selcharid;
        var pos: number = 0;
        var nowpos: number = 0;

        this._boxlist.forEach((box) => {
            if (box.Profile.pid == selid) nowpos = pos;
            pos++;
        });

        nowpos += movecnt + this._boxlist.length;
        nowpos = nowpos % this._boxlist.length;

        var nextid = this._boxlist[nowpos].Profile.pid;

        this.selcharid = nextid;
    }


    static SetImage(pid: number, iid: number) {

        var profile = this.FindProfile(pid);
        profile.iid = iid;

        this._boxlist.forEach((box) => {
            if (box.Profile.pid == pid)
                box.ImageId = iid;
        });

        Inputbox.SetActorImage(profile);
        Stage.SetProfile(profile);

    }

}