﻿
class ClientUIController {


    /**
     * レイアウト情報
     */
    public static Layout: Layout = new Layout();


    /**
     * 初期化処理
     */
    public static Initialize() {

        //  
        $('.login-main').hide();
        $('.logout-background').hide();
        $('.client-main').hide();

        Util.StopPropagation();
        Util.StopTouchmove();

        //  表示開始
        document.body.removeAttribute('hidden');

        //  $('#contents_inputbox').draggable();
    }


    /**
     * 部屋情報の設定
     * @param roomInfo
     */
    public static SetRoomInfo(roomInfo: RoomInfo) {

        Login.Initilize(roomInfo);
        $('.login-name-field').addClass('is-dirty');
        $('.login-main').fadeIn();

    }


    /**
     * ログイン後初期化処理
     * @param profile
     */
    public static LoginSetup(profile: Profile) {

        this.IconSetup();

        //  各種コンポーネントの初期化処理
        Inputbox.Initilize();
        ActorIconList.Initilize();

        //  ステージ設定の取得
        WebRTCService.OwnerSend(new Sender(SenderType.GetStage));

        //  ステージ登録ページの表示
        var url = Util.CreateLink('../Stage/', Util.GetPeerID(), Login.Profile.pid);
        $('#stage_page').attr('src', url);

        var mobileUrl = Util.CreateLink('../InviteMobile/', Util.GetPeerID(), Login.Profile.pid);
        LogUtil.Info(mobileUrl);
        $('#mobile_page').attr('src', mobileUrl);

        this.SetClipBordEvent();

        //  画面切替
        $('.login-background').fadeOut();
        $('.client-main').fadeIn();
    }


    /**
     * アイコンの設定とイベント設定
     */
    public static IconSetup() {

        ////  ボイス切替
        //var icon = new ToggleIcon('icon_speech'
        //    , '../Common/Img/ic_volume_up_white_48dp.png'
        //    , '../Common/Img/ic_volume_off_white_48dp.png'
        //    , (isOn) => {
        //        Timeline.IsSpeech = isOn;
        //    });

        ////  
        //var icon = new ToggleIcon('icon_speechrec'
        //    , '../Common/Img/ic_mic_white_48dp.png'
        //    , '../Common/Img/ic_mic_off_white_48dp.png'
        //    , (isOn) => {
        //        if (isOn)
        //            this._recognition.start();
        //        else
        //            this._recognition.stop();
        //    });
    }


    /**
     * ログイン後初期化処理
     */
    public static SetClipBordEvent() {

        //  クリップボードイベント
        document.addEventListener('paste', function (e: any) {

        });

    }


    /**
     * 切断時処理
     */
    public static Disconnect() {

        $('#stage_page').attr('src', null);
        $('.client-main').fadeOut();
        $('.login-background').fadeOut();
        $('.logout-background').fadeIn();
    }

}