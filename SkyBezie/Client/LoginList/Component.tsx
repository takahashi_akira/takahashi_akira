﻿/// <reference path="../../Lib/typings/tsd.d.ts" />

/**
 *
 */
class LoginList {

    public static _element: HTMLElement = document.getElementById('contents_loginuser');
    public static _selectPid: number = -1;
    public static _profileMap = new Map<Number, Profile>();
    public static _groupInfoMap = new Map<number, ActorGroupInfo>();


    /**
     * プロフィールリストの設定
     * @param value
     */
    public static SetProfile(value: ProfileList) {
        this._profileMap = new Map<number, Profile>();
        value.List.forEach(n => this._profileMap.set(n.pid, n));

        this.render();
    }


    /**
     * 選択しているアクターのIDを設定します
     * @param pid
     */
    public static SetPid(pid: number) {
        this._selectPid = pid;
        this.render();
    }


    /**
     * グループ情報を設定します
     * @param group
     */
    public static SetGroup(group: ActorGroupInfoList) {
        group.List.forEach(p => {
            p.pidlist.forEach(s => {
                this._groupInfoMap.set(s, p);
            });
        });

        this.render();
    }


    /**
     * グループメンバーの描画
     */
    public static render() {

        var list = new ProfileList();
        var group = this._groupInfoMap.get(this._selectPid);
        if (group) {
            group.pidlist.forEach(pid => {
                var profile = this._profileMap.get(pid);
                if (profile)
                    list.List.push(profile);
            });
        }
        ReactDOM.render(<LoginListComponent group={group} profiles={list} />, this._element);

        Stage.SetGroup(group);
    }


    /**
     * 指定メンバーがログインしているか？
     * @param pid
     */
    public static IsLogin(pid: number): boolean {

        var info = this._profileMap.get(pid);
        return info.islogin;

    }

}


/**
 *
 */
interface LoginListProp {
    group: ActorGroupInfo;
    profiles: ProfileList;
}


/**
 *
 */
interface LoginUserProp {
    profile: Profile;
}


/**
 *
 */
class LoginListComponent extends React.Component<LoginListProp, any> {

    /**
     * グループメンバーの表示
     */
    public render() {

        var groupName = "";

        if (this.props.group)
            groupName = this.props.group.name;

        var userNodes = this.props.profiles.List.map((profile) => {
            if (profile.visible && profile.islogin)
                return (<LoginUserComponent profile={profile} />);
        });

        return (
            <div className="mdl-list">
                <div className="mdl-list__item">
                    【　{groupName}　】
                </div>
                {userNodes}
            </div>
        );
    }
}



/*
 *
 */
class LoginUserComponent extends React.Component<LoginUserProp, any>{

    /**
     * グループメンバーの行表示
     */
    public render() {

        var p = this.props.profile;

        return (
            <div className="mdl-list__item">
                <span className="mdl-list__item-primary-content">
                    <i className="material-icons mdl-list__item-avatar">person</i>
                    <span>{p.status}</span>
                    <span>{p.name}</span>
                </span>
            </div>
        );
    }
}
