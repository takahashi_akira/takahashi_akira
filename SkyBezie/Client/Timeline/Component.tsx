﻿/// <reference path="../../Lib/typings/tsd.d.ts" />


/**
 *  タイムライン描画クラス
 */
class Timeline {


    private static _element: HTMLElement = document.getElementById('timeline');

    public static IsSpeech: boolean = true;


    /**
     * タイムラインにメッセージを表示します
     * @param messages メッセージリスト
     * @param isSpeech スピーチ対象となるメッセージか？ 
     */
    public static SetMessages(messages: Array<MsgLog>, isSpeech: boolean = false) {

        var iidmap = new Map<number, string>();


        var filler = this.Filter(messages, isSpeech);
        ReactDOM.render(<Timeline_MsgItemListComponent Messages={filler} />, this._element);

        //  アイコン表示の為のCSS設定、最後にまとめて表示させる
        iidmap.forEach((v, iid) => {
            ActorImageCache.findSetCss(iid);
        });

        //  チャット欄の最後を表示する
        this.MoveLast();
    }


    private static _msgMap = new Map<number, MsgLog>();
    private static _lastMid: number = 0;


    /**
     * メッセージフィルター
     * @param messages メッセージリスト
     * @param isSpeech スピーチ対象となるメッセージか？ 
     */
    public static Filter(messages: Array<MsgLog>, isSpeech: boolean): Array<MsgLog> {

        messages.forEach(msg => {

            if (this._lastMid < msg.mid) {

                if (this.IsSpeech && isSpeech) {

                    //  現状では音声ガイドのみ対応
                    if (msg.isGuide)
                        this.Speech(msg);
                }

                this._lastMid = msg.mid;
            }

            if (this._msgMap.has(msg.mid))
                this._msgMap.delete(msg.mid);

            this._msgMap.set(msg.mid, msg);

        });

        var result = new Array<MsgLog>();

        var displayMinMid = (this._lastMid - Const.TIMELINE_MAX_MSG_COUNT);

        this._msgMap.forEach((msg, mid) => {
            if (mid >= displayMinMid) {
                result.push(msg);
            }
        });

        return result;
    }


    /**
     * メッセージの削除
     * @param mid
     */
    public static RemoveMsg(mid: number) {

        if (this._msgMap.has(mid)) {

            var msg = this._msgMap.get(mid);
            msg.visible = false;
            var store = new MsgStore();
            store.Messages = new Array<MsgLog>();
            store.Messages.push(msg);
            WebRTCService.OwnerSend(store);
        }
    }


    /**
     * スクロールを最終行に移動
     */
    public static MoveLast() {
        $('#timeline').scrollTop(4294967295);
    }


    /**
     * 音声出力処理
     * @param msg メッセージ
     */
    private static Speech(msg: MsgLog) {
        var syaberi = new SpeechSynthesisUtterance();
        syaberi.volume = 0.8;
        syaberi.rate = 1.24;
        syaberi.pitch = 1.00;
        syaberi.text = msg.text;
        syaberi.lang = "ja-JP";;
        syaberi.onend = function (event) { }
        speechSynthesis.speak(syaberi);
    }

}

/**
 *  メッセージプロパティ
 */
interface Timeline_MsgListProp {
    Messages: Array<MsgLog>
}


/**
 * 
 */
class Timeline_MsgGroup {
    Pid: number;
    Iid: number;
    Messages: Array<MsgLog>;
}

/**
 *  
 */
interface Timeline_MsgItemProp {
    MsgGroup: Timeline_MsgGroup;
}


/**
 *  
 */
class Timeline_MsgItemListComponent extends React.Component<Timeline_MsgListProp, any>{

    /**
     *  描画処理
   　*/
    public render() {

        var pid = -1;
        var iid = -1;
        var groups = new Array<Timeline_MsgGroup>();
        var group: Timeline_MsgGroup;

        this.props.Messages.forEach((msg) => {

            if (msg.visible) {

                if (msg.pid != pid || msg.iid != iid) {
                    pid = msg.pid;
                    iid = msg.iid;
                    group = new Timeline_MsgGroup();
                    group.Pid = pid;
                    group.Iid = iid;
                    group.Messages = new Array<MsgLog>();
                    groups.push(group);
                }

                group.Messages.push(msg);
            }

        });


        var msgList = groups.map((groups) => {
            return (<Timeline_MsgItemComponent MsgGroup={groups} />);
        });


        return (
            <div id='messages'>
                {msgList}
            </div>
        );
    }
}


/**
 * 
 */
class Timeline_MsgItemComponent extends React.Component<Timeline_MsgItemProp, any> {


    /**
     *  描画処理
   　*/
    public render() {

        var loginPid = Login.Profile.pid;

        var pid = this.props.MsgGroup.Pid;
        var iid = this.props.MsgGroup.Iid;

        var user = ActorList.FindProfile(pid);
        var name = Util.ToHtml(user ? user.name : "Guide");

        var msgtext = this.props.MsgGroup.Messages.map((msg) => {

            var mid = 'timeline-message-' + msg.mid;

            if (loginPid == pid) {
                return (
                    <p className='timeline-message'>{msg.text}
                        <button className='mdl-button mdl-js-button mdl-button--icon mdl-button--colored' onClick={this.OnClick}>
                            <i className='material-icons' id={mid}>close</i>
                        </button>
                    </p>
                );
            }
            else {
                return (<p className='timeline-message' id={mid}>{msg.text}</p>);
            }
        });

        //  var text = Util.ToHtml(this.props.Message.text);
        //  画像が設定されている場合は表示する
        var image_div = (iid == 0 ? (<div></div>) : (<div className='timeline-img-box'><div className='actor-img-{iid}'></div></div>));

        return (
            <div className='timeline-flex'>
                {image_div}
                <div>
                    <div>
                        <label className='timeline-name'>
                            {name}
                        </label>
                    </div>
                    <div className='timeline-balloon'>
                        {msgtext}
                    </div>
                </div>
            </div>
        );
    }


    /**
     * メッセージの「✕」ボタン押下時イベント
     * @param event
     */
    public OnClick(event: MouseEvent) {
        if (event) {
            var element = event.target as HTMLElement;
            var mid = element.id.replace("timeline-message-", "");
            Timeline.RemoveMsg(Number.parseInt(mid));
        }
    }
}
