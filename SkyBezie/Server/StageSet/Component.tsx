﻿/// <reference path="../../Lib/typings/tsd.d.ts" />


/*
 *  シーン一覧、描画クラス
 */
class StageSetDisplay {

    public static Create(element: HTMLElement, value: Array<StageSetInfo>) {
        ReactDOM.render(<StageSetListComponent List={value} />, element);
    }
}

interface StageSetInfoListProp {
    List: Array<StageSetInfo>
}

/*
 *  シーン一覧コンポーネント
 */
class StageSetListComponent extends React.Component<StageSetInfoListProp, any> {

    render() {
        var commentNodes = this.props.List.map((cur) => {

            var imgRec = StageSetImage.GetImageRec(cur.thumbnail_uuid);

            return (<StageSetListItemComponent StageSet={cur} Image={imgRec} />);
        });

        return (
            <div className='StageSet_box_list'>
                {commentNodes}
            </div>
        );
    }

}


/*
 *
 */
interface StageSetInfotProp {
    StageSet: StageSetInfo,
    Image: ImageRec
}


/*
 *
 */
class StageSetListItemComponent extends React.Component<StageSetInfotProp, any>{

    render() {

        var StageSet = this.props.StageSet;

        var style = {
            container: {
                background: "url(" + this.props.Image.src + ")",
                "background-size": "cover"
            }
        }
        return (
            <div className='StageSet_box' onDoubleClick={this.onDoubleClick.bind(this) } >
                <label className='StageSet_label'>{StageSet.name}</label>
                <div className='StageSet_image' style={style.container}>
                </div>
            </div>
        );
    }

    private onDoubleClick() {
        StageSetImage.OpenDialog(this.props.StageSet.stagesetid);
    }
}