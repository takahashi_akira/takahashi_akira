﻿
class ServerReceiver extends ServiceReceiver {


    /**
     * 入力状態
     * @param sender
     */
    public Ing(sender: Ing): ServiceRecv {

        var result = new ServiceRecv();
        result.AllClientRecv(ServerDataStore.InputMap.SetIng(sender as Ing));
        return result;
    }


    /**
     * メッセージ
     * @param sender
     */
    public Message(sender: Message): ServiceRecv {

        var msglog = ServerDataStore.SetMessage(sender);

        //  音声ガイドの確認
        this.SetVoideGuide(sender);

        ServerDataStore.DB.Write(SkyBezieDB.SYSTEM, SenderType.Room, ServerDataStore.Room);

        var result = new ServiceRecv();
        result.AllClientRecv(ServerDataStore.MsgList);
        return result;
    }


    /**
     * 音声ガイドメッセージの追加
     * @param msg
     */
    public SetVoideGuide(msg: Message) {

        if (msg == null || msg.text.length == 0)
            return;

        GuideList.GetGuideList().map(g => this.CheckSetGuide(msg, g));
        YouTubeList.GetGuideList().map(g => this.CheckSetGuide(msg, g));
    }


    /**
     * 音声ガイドメッセージの追加
     * @param msg
     * @param guide
     */
    public CheckSetGuide(msg: Message, guide: Guide) {

        if (guide === null)
            return;

        if (guide.key == null || guide.key.length == 0 || guide.value.length == 0)
            return;

        if (guide.key == msg.text) {
            var msglog = ServerDataStore.SetGuide(guide);
        }
    }


    /**
     * 部屋情報の取得
     * @param sender
     */
    public GetRoomInfo(sender): ServiceRecv {
        var result = new ServiceRecv();
        var roomInfo = new RoomInfo();
        roomInfo.roomName = ServerDataStore.Room.title;
        result.ConnectionRecv(roomInfo);
        return result;
    }


    /**
     * ログイン処理
     * @param conn
     * @param sender
     */
    public Login(conn: PeerJs.DataConnection, sender: Login): ServiceRecv {

        var result = new ServiceRecv();
        var loginResult = ServerDataStore.Login(sender as LoginInfo);

        result.ConnectionRecv(loginResult);

        if (loginResult.succeed) {

            ServerDataStore.SetLoginInfo(conn.label, loginResult.myProfile);

            ActorGroup.Create();
            GuideList.Create();
            result.AllClientRecv(ServerDataStore.ProfileList);
            result.AllClientRecv(ActorGroup.GetActorGroupList());
            result.ConnectionRecv(Wallpaper.GetStoreImage());
            result.ConnectionRecv(ServerDataStore.Layout);

        }

        return result;
    }


    /**
     * プロフィールの編集処理
     * @param sender
     */
    public ProfileEdit(sender: ProfileEdit): ServiceRecv {

        var result = new ServiceRecv();
        var pe = sender as ProfileEdit;

        ServerDataStore.SetProfile(pe.Profile);
        ServerDataStore.DB.Write(SkyBezieDB.SYSTEM, SenderType.Room, ServerDataStore.Room);
        ServerDataStore.DB.Write(SkyBezieDB.PROFILE, pe.Profile.pid, pe.Profile);
        ActorGroup.Create();
        GuideList.Create();

        result.AllClientRecv(ServerDataStore.ProfileList);
        result.AllClientRecv(ActorGroup.GetActorGroupList());

        return result;
    }


    /**
     * プロフィールリストの取得
     * @param sender
     */
    public GetProfileList(sender): ServiceRecv {
        var result = new ServiceRecv();
        result.ConnectionRecv(ServerDataStore.ProfileList);
        return result;
    }


    /**
     * メッセージの取得
     * @param sender
     */
    public GetMessage(sender): ServiceRecv {
        var result = new ServiceRecv();
        result.ConnectionRecv(ServerDataStore.MsgList);
        return result;
    }


    /**
     * メッセージ
     * @param sender
     */
    public MsgSotre(sender: MsgStore): ServiceRecv {
        var result = new ServiceRecv();

        sender.Messages.forEach(log => {
            ServerDataStore.UpdateMsgLog(log);
        });

        result.AllClientRecv(sender);

        return result;
    }


    /**
     * アイコン設定
     * @param sender
     */
    public IconStore(sender: IconStore): ServiceRecv {
        var result = new ServiceRecv();

        //  画像ファイルの登録
        var pis = sender as IconStore;
        result.AllClientRecv(ServerDataStore.SetImage(pis));
        result.ConnectionRecv(ServerDataStore.GetIconIdList(pis.Icon.pid));

        return result;
    }


    /**
     * アイコンの取得
     * @param conn
     * @param sender
     */
    public GetIcon(conn: PeerJs.DataConnection, sender: GetIcon): ServiceRecv {

        var gpi = sender as GetIcon;
        var img = ServerDataStore.GetImage(gpi.iid, conn);

        return null;
    }


    /**
     * ステージの設定
     * @param conn
     * @param sender
     */
    public StageService(conn: PeerJs.DataConnection, sender: StageService): ServiceRecv {

        var result = new ServiceRecv();
        var stage = sender as StageService;

        //  Stage管理と通知
        result.AllClientRecv(ServerDataStore.SetStageService(conn.label, stage));

        //  ステージ側には各種情報を返す
        result.ConnectionRecv(ServerDataStore.ProfileList);         //  プロフィール
        result.ConnectionRecv(ActorGroup.GetActorGroupList());      //  グループ情報

        StageLayout.Create();

        return result;

    }


    /**
     * ステージの取得
     * @param sender
     */
    public GetStage(sender): ServiceRecv {
        var result = new ServiceRecv();
        result.ConnectionRecv(ServerDataStore.GetStageService());
        return result;
    }


    /**
     * 
     * @param sender
     */
    public StageSetInfoList(sender: StageSetInfoList): ServiceRecv {
        var result = new ServiceRecv();
        result.AllClientRecv(ActorGroup.GetActorGroupList());
        return result;
    }


    /**
     * 
     * @param conn
     * @param sender
     */
    public GetImageRec(conn: PeerJs.DataConnection, sender: GetImageRec): ServiceRecv {

        ServerDataStore.DB.Read<ImageRec, string>(SkyBezieDB.IMAGE, sender.uuid, (imgrec) => {
            WebRTCService.ChildSendAll(imgrec);
        });

        return null;
    }


    /**
     * 
     * @param sender
     */
    public GetIconIdList(sender: GetIconIdList): ServiceRecv {

        var result = new ServiceRecv();
        var rq = sender as GetIconIdList;
        result.ConnectionRecv(ServerDataStore.GetIconIdList(sender.pid));
        return result;
    }

}
