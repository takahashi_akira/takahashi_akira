﻿
/**
 * 
 */
class ServerController extends ServiceController {


    /**
     *
     */
    constructor() {
        super();
        this.Receiver = new ServerReceiver(this);
    };


    /**
     * 自身のPeer生成時イベント
     * ※サーバー用のPeerID取得時イベント
     * @param peer
     */
    public OnPeerOpen(peer: PeerJs.Peer) {

        LogUtil.Info("OnPeerOpen(" + peer.id + ")");

        //  サーバーを起動している事を LocalCacheに保持する
        LocalCache.BootServerID = peer.id;

        //  Peer接続成功後、データベースの読込を開始する
        ServerDataStore.Initilize();
    }


    /**
     * Peerエラー
     * @param err
     */
    public OnPeerError(err: Error) {

        LogUtil.Error(err.name);
        LogUtil.Error(err.message);
    }


    /**
     * 
     */
    public OnPeerClose() {

        //  サーバーが正常終了した場合、接続情報はクリアする
        LocalCache.BootServerID = '';
    }


    /**
     * オーナーエラー
     * @param err
     */
    public OnOwnerError(err: any) {
        LogUtil.Error("OnServerError : " + err.ToString());
    }


    /**
     * 切断時イベント
     * @param conn
     */
    public OnChildClose(conn: PeerJs.DataConnection) {
        super.OnChildClose(conn);
        ServerDataStore.RemoveConnection(conn.label);
    }

};
