﻿

/**
 * 
 */
class InputingMap extends Map<number, boolean>{


    constructor() {
        super();
    }


    /**
     * 入力途中情報
     * @param ing
     */
    public SetIng(ing: Ing): Ings {

        var result: Ings = new Ings();

        if (this.has(ing.pid)) {
            if (!ing.status) this.delete(ing.pid);
        }
        else {
            if (ing.status) this.set(ing.pid, true);
        }

        this.forEach((v, id) => { result.pids.push(id); });

        return result;
    }


    /**
     * 入力途中で切断した場合の処理
     * @param list
     * @param profile
     */
    public ReomveIng(list: ProfileList, profile: Profile) {

        if (list == null || profile == null)
            return;

        list.List.forEach((p) => {

            if (p.owner == profile.pid) {
                if (this.has(p.pid)) {
                    //  切断したユーザーが持つPidが入力途中の場合
                    //  入力中を解除する。
                    var ing = new Ing();
                    ing.pid = p.pid;
                    ing.status = false;
                    WebRTCService.ChildSendAll(this.SetIng(ing));
                }
            }

        });
    }

}