﻿/// <reference path="../../Lib/typings/tsd.d.ts" />


/**
 * ガイド描画クラス
 */
class YouTubeList {


    public static _linkList: Array<Guide>;


    /**
     * 初期化処理
     * @param guideList
     */
    public static Initilize(guideList: Array<Guide>) {

        this._linkList = new Array<Guide>();

        guideList.forEach(n => {
            if (n.isYouTube)
                this._linkList.push(n);
        });

        YouTubeList.Create();


        //  画像エリアのイベント（ドラック＆ドロップ用）

        var element = $('#scroll-tab-youtube');

        element.on('dragover', (e) => {
            var event = e.originalEvent as DragEvent;
            event.preventDefault();
            event.dataTransfer.dropEffect = 'copy';
            element.focus();
        }).on('drop', (e) => {

            var event = e.originalEvent as DragEvent;
            event.preventDefault();

            var items = event.dataTransfer.items;

            var i = 0;
            for (i = 0; i < items.length; i++) {
                var item = items[i];
                if (item.type == "text/uri-list") {
                    item.getAsString(this.DropUrl);
                }
            }
        });


    }


    /**
     * 
     */
    public static Create() {

        var element: HTMLElement = document.getElementById('youtube-list');
        ReactDOM.render(<YouTubeComponent guides={this._linkList}/>, element, () => {
        });
    }


    /**
     * YouTubeガイドの追加
     */
    public static AddNew() {
        var guide = new Guide();
        guide.guideid = ServerDataStore.Room.max_guide_id++;
        ServerDataStore.SetRoomInfo();

        this._linkList.push(guide);
        this.UpdateRecv(guide);

        $('#youtube-gide-list').scrollTop(4294967295);
    }


    /**
     * URLのドロップ時処理
     * @param url
     */
    public static DropUrl(url: string) {
        var guide = new Guide();
        guide.guideid = ServerDataStore.Room.max_guide_id++;
        guide.isYouTube = true;
        guide.value = YouTubeUtil.GetYouTubeID(url);

        //  動画情報を取得する
        YouTubeUtil.GetPlayer(guide.value, (player) => {

            var vd = player.getVideoData();

            if (vd) {
                guide.key = vd.title;
                guide.end_time = player.getDuration();
            }

            ServerDataStore.SetRoomInfo();
            YouTubeList._linkList.push(guide);
            YouTubeList.UpdateRecv(guide);

        });
    }


    /**
     * ガイドの更新
     * @param info
     */
    public static UpdateRecv(info: Guide) {
        ServerDataStore.DB.Write<Guide, number>(SkyBezieDB.GUIDE, info.guideid, info);
        YouTubeList.Create();
    }


    /**
     * ガイドの削除
     * @param info
     */
    public static DeleteRecv(info: Guide) {

        ServerDataStore.DB.Delete<Guide, number>(SkyBezieDB.GUIDE, info.guideid);
        YouTubeList.Create();
    }


    /**
     * ガイド情報の取得
     */
    public static GetGuideList(): Array<Guide> {
        return this._linkList;
    }


}

interface YouTubeProp {
    guides: Array<Guide>
}


/**
 *  ガイド一覧コンポーネント
 */
class YouTubeComponent extends React.Component<YouTubeProp, any> {


    /**
     * ガイド一覧の表示
     */
    public render() {

        var guideNodes = this.props.guides.map((cur) => {

            //  ガイドリストの表示
            return (<YouTubeItemComponent info={cur} />);
        });

        return (
            <ul className="mdl-list">
                {guideNodes}
            </ul>
        );
    }

}


/**
 * ガイド一覧プロパティ
 */
interface YouTubeItemProp {
    info: Guide;
}


/**
 * ガイド一覧ステータス
 */
interface YouTubeItemStat {
    visible: boolean;
    guide_id: string;
    guidekey: string;
    youtubeid: string;
    start_time: number;
    end_time: number;
    loop: boolean;
    info: Guide;
}


/**
 * YouTubeガイドコンポーネント
 */
class YouTubeItemComponent extends React.Component<YouTubeItemProp, YouTubeItemStat>{


    /**
     * コンストラクタ
     * @param props
     * @param context
     */
    public constructor(props?: GuideItemProp, context?: any) {
        super(props, context);

        //  初期化はコンストラクタで行う
        this.state = {
            visible: true,
            guide_id: Resource.GUIDE + this.props.info.guideid,
            guidekey: this.props.info.key,
            youtubeid: this.props.info.value,
            start_time: this.props.info.start_time,
            end_time: this.props.info.end_time,
            loop: this.props.info.loop,
            info: this.props.info
        };
    }


    /**
     * YouTubeガイドの行の描画
     */
    public render() {

        var GuideList = this.props.info;

        var url: string = "";

        //  削除した場合のYouTubeのiFrameは除去する
        if (this.state.visible)
            url = YouTubeUtil.ToEmbedYouTubeID(this.state.info, false);

        return (
            <li className="mdl-list__item" hidden={!this.state.visible}>
                <span className="mdl-list__item-primary-content">

                    <button className="mdl-button guide-button" tabIndex="-1" onClick={this.onVideoClick.bind(this) } >
                        <i className="material-icons mdl-list__item-avatar">video_library</i>
                    </button>

                    <iframe width="320" height="180" src={url} frameborder="0" allowfullscreen></iframe>

                    <ul className="mdl-list youtube-control">

                        <li className="mdl-list_item">
                            <span class="mdl-list__item-primary-content">
                                <button className="mdl-button mdl-js-button mdl-button--raised mdl-button--colored " hidden={this.state.loop} tabIndex="-1" onClick={this.onLoopOn.bind(this) } >
                                    <i className="material-icons bottom">replay</i>
                                    <span> Repeat OFF </span>
                                </button>
                                <button className="mdl-button mdl-js-button mdl-button--raised mdl-button--accent" hidden={!this.state.loop} tabIndex="-1" onClick={this.onLoopOff.bind(this) } >
                                    <i className="material-icons bottom">loop</i>
                                    <span> Repeat ON   </span>
                                </button>
                            </span>
                        </li>

                        <li className="mdl-list_item">
                            <div className="mdl-textfield mdl-js-textfield guide-textbox" spellcheck="false">
                                <input className="mdl-textfield__input guide-keyword-font" placeholder="キーワード" type="text" value={this.state.guidekey} onChange={(event: React.FormEvent) => { this.changeKeyword(event); } }/>
                            </div>
                        </li>

                        <li className="mdl-list_item">
                            <span class="mdl-list__item-primary-content">
                                <div className="mdl-textfield mdl-js-textfield guide-textbox" spellcheck="false">
                                    <input className="mdl-textfield__input" placeholder="開始秒" type="text" pattern="-?[0-9]*(\\.[0-9]+)?" value={this.state.start_time} onChange={(event: React.FormEvent) => { this.changeStartTime(event); } }/>
                                </div>
                            </span>
                        </li>

                        <li className="mdl-list_item">
                            <span class="mdl-list__item-primary-content">
                                <div className="mdl-textfield mdl-js-textfield guide-textbox" spellcheck="false">
                                    <input className="mdl-textfield__input" placeholder="終了秒" type="text" pattern="-?[0-9]*(\\.[0-9]+)?" value={this.state.end_time} onChange={(event: React.FormEvent) => { this.changeEndTime(event); } }/>
                                </div>
                            </span>
                        </li>

                    </ul>
                </span>
                <span className="mdl-list__item-secondary-content">
                    <button className="mdl-button mdl-js-button mdl-js-ripple-effect" tabIndex="-1" onClick={this.onDeleteClick.bind(this) } >
                        <i className="material-icons">delete_forever</i>
                    </button>
                </span>
            </li >
        );
    }


    /**
     * テキスト変更時処理
     * @param event
     */
    private changeKeyword(event) {
        this.setState({
            visible: true,
            guide_id: this.state.guide_id,
            guidekey: event.target.value,
            youtubeid: this.state.youtubeid,
            start_time: this.state.start_time,
            end_time: this.state.end_time,
            loop: this.state.loop,
            info: this.state.info
        });
        this.props.info.key = event.target.value;

        YouTubeList.UpdateRecv(this.state.info);
    }


    /**
     * 開始秒の変更
     * @param event
     */
    private changeStartTime(event) {
        this.setState({
            visible: true,
            guide_id: this.state.guide_id,
            guidekey: this.state.guidekey,
            youtubeid: this.state.youtubeid,
            start_time: event.target.value,
            end_time: this.state.end_time,
            loop: this.state.loop,
            info: this.state.info
        });
        this.props.info.start_time = event.target.value;

        YouTubeList.UpdateRecv(this.state.info);
    }


    /**
     * 終了秒の変更
     * @param event
     */
    private changeEndTime(event) {
        this.setState({
            visible: true,
            guide_id: this.state.guide_id,
            guidekey: this.state.guidekey,
            youtubeid: this.state.youtubeid,
            start_time: this.state.start_time,
            end_time: event.target.value,
            loop: this.state.loop,
            info: this.state.info
        });
        this.props.info.end_time = event.target.value;

        YouTubeList.UpdateRecv(this.state.info);
    }


    /**
     * ループボタン押下時（ON時)
     * @param event
     */
    private onLoopOn(event) {
        this.setState({
            visible: true,
            guide_id: this.state.guide_id,
            guidekey: this.state.guidekey,
            youtubeid: this.state.youtubeid,
            start_time: this.state.start_time,
            end_time: this.state.end_time,
            loop: true,
            info: this.state.info
        });
        this.props.info.loop = true;

        YouTubeList.UpdateRecv(this.state.info);
    }


    /**
     * ループボタン押下時（OFF時）
     * @param event
     */
    private onLoopOff(event) {
        this.setState({
            visible: true,
            guide_id: this.state.guide_id,
            guidekey: this.state.guidekey,
            youtubeid: this.state.youtubeid,
            start_time: this.state.start_time,
            end_time: this.state.end_time,
            loop: false,
            info: this.state.info
        });
        this.props.info.loop = false;

        YouTubeList.UpdateRecv(this.state.info);
    }


    /**
     * 「Video」クリック時処理
     * @param ev
     */
    private onVideoClick(ev) {

        ServerDataStore.SetGuide(this.state.info);
        WebRTCService.ChildSendAll(ServerDataStore.MsgList);

    }


    /**
     * 「DELETE」クリック時処理
     * @param ev
     */
    private onDeleteClick(ev) {
        this.setState({
            visible: false,     //  ガイドデータは削除せず非表示とするだけ
            guide_id: this.state.guide_id,
            guidekey: this.state.guidekey,
            youtubeid: this.state.youtubeid,
            start_time: this.state.start_time,
            end_time: this.state.end_time,
            info: this.state.info,
            loop: this.state.loop
        });

        YouTubeList.DeleteRecv(this.state.info);
    }

}


