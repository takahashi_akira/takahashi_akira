﻿

class Wallpaper {

    public static IMAGE_KEY: string = "Wallpeper";
    public static _info: WallpaperInfo = new WallpaperInfo();


    /**
     * 初期化処理
     */
    public static Initilize() {

        ServerDataStore.DB.Read(SkyBezieDB.IMAGE, this.IMAGE_KEY, (r) => {
            this._info.Image = r;
        });
    }

    /**
     * イメージ設定
     */
    public static GetStoreImage(): WallpaperInfo {
        return this._info;
    }


    /**
     * 
     */
    public static OpenDialog() {

        ImageDialog.Open(ImageType.Wallpaper, this._info.Image, (r) => {

            this._info.Image = r;
            this._info.Image.uuid = this.IMAGE_KEY;
            ServerDataStore.DB.Write(SkyBezieDB.IMAGE, this.IMAGE_KEY, r);
            WebRTCService.ChildSendAll(this._info);
        });

    }

}