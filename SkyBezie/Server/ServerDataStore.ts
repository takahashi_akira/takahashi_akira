﻿

class ServerDataStore {

    /*
     *  仮配置
     *  ※クライアント毎のレイアウト設定を可能にし、保存できるようにするか検討する
     */
    public static Layout: Layout = new Layout();

    public static DB: SkyBezieDB;
    public static RoomTitle: string;
    public static Room: Room;
    public static MsgList: MsgStore;
    public static ProfileList: ProfileList;
    public static InputMap: InputingMap = new InputingMap();
    public static StageMap: Map<string, StageService> = new Map<string, StageService>();
    public static ConnectionMap: Map<string, Profile> = new Map<string, Profile>();
    public static ImageMap: Map<number, Icon> = new Map<number, Icon>();


    /**
     * 初期化処理
     */
    public static Initilize() {
        this.DB = new SkyBezieDB();
        this.DB.Connect(() => ServerDataStore.DataLoader());
    }


    /**
     * データ読込処理
     */
    public static DataLoader() {

        this.RoomTitle = LocalCache.RoomName;

        this.DB.ReadAllData((data) => {

            //  部屋情報の設定
            if (!data.Room)
                data.Room = new Room();

            this.Room = data.Room;
            if (this.RoomTitle)
                this.Room.title = this.RoomTitle;

            ServerUIController.SetRoomInfo(this.Room);

            //  プロフィール
            this.ProfileList = new ProfileList();
            if (data.Profiles)
                this.ProfileList.List = data.Profiles;

            this.ProfileList.List.forEach((p) => p.islogin = false);

            //  メッセージログ
            this.MsgList = new MsgStore();
            if (data.Messages)
                this.MsgList.Messages = data.Messages;


            //  アイコン
            this.ImageMap = new Map<number, Icon>();

            if (data.Icons) {
                data.Icons.forEach((r: Icon) => {
                    this.ImageMap.set(r.iid, r);
                });
            }

            //  ActorGroup
            if (!data.ActorGroups) {
                data.ActorGroups = new Array<ActorGroupInfo>();
            }
            ActorGroup.Initilize(data.ActorGroups);

            //  Guide
            if (!data.Guides) {
                data.Guides = new Array<Guide>();
            }
            GuideList.Initilize(data.Guides);
            YouTubeList.Initilize(data.Guides);

            //  ステージレイアウト
            StageLayout.Initilize();

            //  壁紙
            Wallpaper.Initilize();


            //  データの読込完了後、クライアントの表示を開始する
            LogUtil.Info("OnLoad SkyBezie Client");

            //  Client
            var clientURL = Util.CreateLink('../Client/', LocalCache.BootServerID);
            $('#client-frame').attr("src", clientURL);

            //  招待用ページ
            var inviteURL = Util.CreateLink('../Invite/', LocalCache.BootServerID);
            $('#invite-frame').attr("src", inviteURL);

            //  Azuereサーバーに登録する
            ApiUtil.RoomPush(this.Room.title, LocalCache.BootServerID);


            if (LocalCache.BootWithClient) {
                window.open(clientURL);
            }

        });

    }


    /**
     * プロフィールの生成
     * @param login
     */
    public static CreateNewProfile(login: LoginInfo): Profile {

        var profile = new Profile();
        profile.pid = this.Room.max_profile_id++;
        profile.owner = profile.pid;
        profile.name = login.name;
        profile.md5 = login.md5;
        profile.status = "";
        profile.URL = "";

        this.ProfileList.List.push(profile);
        this.DB.Write(SkyBezieDB.SYSTEM, SenderType.Room, this.Room);
        this.DB.Write(SkyBezieDB.PROFILE, profile.pid, profile);

        return profile;
    }


    /**
     * ログイン名称からプロフィールを取得
     * @param login
     */
    public static findProfile(login: LoginInfo): LoginResult {

        var result: LoginResult = null;

        this.ProfileList.List.forEach(profile => {

            if (profile.ptype == "P" && profile.name == login.name) {

                result = new LoginResult();

                if (profile.md5 == login.md5) {
                    result.succeed = true;
                    result.myProfile = profile;
                    result.message = "";
                }
                else {
                    result.succeed = false;
                    result.message = "登録済みの名前またはパスワードが違います";
                }

                return result;
            }
        });

        //  存在しない場合はnull
        return result;
    }


    /**
     * アクター情報の登録・更新をします
     * @param actor
     */
    public static SetProfile(actor: Profile) {

        if (actor.pid == 0) {
            //  アクターの追加
            actor.pid = this.Room.max_profile_id++;
            this.ProfileList.List.push(actor);
        }
        else {
            var selectactor = this.GetProfile(actor.pid);
            selectactor.name = actor.name;
            selectactor.status = actor.status;
            selectactor.URL = actor.URL;
            selectactor.visible = actor.visible;
        }
    }


    /**
     * 登録されているプロフィールを取得
     * @param pid
     */
    public static GetProfile(pid: number): Profile {

        var sel: Profile = null;

        this.ProfileList.List.forEach((actor) => {
            if (actor.pid == pid) {
                sel = actor;
            }
        });

        return sel;
    }


    /**
     * 登録されているプロフィール一覧を取得
     */
    public static GetProfiles(): Array<Profile> {
        return this.ProfileList.List;
    }


    /**
     * 
     */
    public static SetRoomInfo() {
        this.DB.Write(SkyBezieDB.SYSTEM, SenderType.Room, this.Room);
    }


    /**
     * 画像データを取得します
     * @param iid
     * @param conn
     */
    public static GetImage(iid: number, conn: PeerJs.DataConnection): IconStore {

        if (this.ImageMap.has(iid)) {

            var pis = new IconStore();
            pis.Icon = this.ImageMap.get(iid);
            conn.send(JSON.stringify(pis));

            return pis;
        }
        else {
            this.DB.Read(SkyBezieDB.ICON, iid, (pi: Icon) => {

                if (pi != null) {
                    var pis = new IconStore();
                    pis.Icon = pi;
                    conn.send(JSON.stringify(pis));
                }
            });

            return null;
        }
    }


    /**
     * 画像データを設定します
     * @param pis
     */
    public static SetImage(pis: IconStore): IconStore {

        var pi = pis.Icon;

        if (pi.iid == 0) {
            //  新しい画像の登録の場合
            pi.iid = this.Room.max_img_id++;
            this.DB.Write(SkyBezieDB.SYSTEM, SenderType.Room, this.Room);
        }

        this.DB.Write(SkyBezieDB.ICON, pi.iid, pi);
        this.SetIcon(pi);

        return pis;
    }


    /**
     * アイコンを設定します
     * @param pi
     */
    public static SetIcon(pi: Icon) {

        if (this.ImageMap.has(pi.iid))
            this.ImageMap.delete(pi.iid);

        this.ImageMap.set(pi.iid, pi);
    }


    /**
     * 指定されたpidの画像ファイルID一覧を取得します。
     * ※Idのリストのみであり、画像データは取得しません。
     * @param pid
     */
    public static GetIconIdList(pid: number): IconIdList {

        var result = new IconIdList(pid);

        this.ImageMap.forEach((v, iid) => {
            if (v.pid == pid && v.img.visible)
                result.iidlist.push(iid);
        });

        return result;
    }


    /**
     * ログイン
     * @param login
     */
    public static Login(login: LoginInfo): LoginResult {

        var result: LoginResult = null;

        if (login == null || login.name.length == 0) {
            result = new LoginResult();
            result.succeed = false;
            result.message = "名前を入力してください";
            return result;
        }

        result = this.findProfile(login);

        //  存在しない場合は新規登録
        if (result == null) {

            var profile = this.CreateNewProfile(login);

            result = new LoginResult();
            result.succeed = true;
            result.myProfile = profile;
        }

        if (result.succeed) {
            result.Profiles = ServerDataStore.ProfileList;

            //  メッセージログは件数を絞る

            var allmsg = this.MsgList.Messages;

            if (this.Room.msg_disp_count >= allmsg.length) {
                result.store = this.MsgList;
            }
            else {
                result.store = new MsgStore();
                for (var i = allmsg.length - this.Room.msg_disp_count; i < allmsg.length; i++) {
                    result.store.Messages.push(allmsg[i]);
                }
            }

        }

        return result;
    }


    /**
     * メッセージ登録
     * @param msg
     */
    public static SetMessage(msg: Message): MsgLog {

        var log = new MsgLog();

        log.mid = this.Room.max_msg_id++;
        log.pid = msg.pid;
        log.iid = msg.iid;
        log.text = msg.text;
        log.date = Date.now();
        log.visible = true;
        log.dice = this.DiceConvert(log.text);

        this.MsgList.Messages.push(log);
        this.DB.Write(SkyBezieDB.MESSAGE, log.mid, log);

        return log;
    }


    /**
     * 音声ガイド登録
     * @param guide
     */
    public static SetGuide(guide: Guide): MsgLog {
        var log = new MsgLog();

        log.mid = this.Room.max_msg_id++;
        log.pid = -guide.guideid;
        log.iid = 0;
        log.text = guide.value;
        log.date = Date.now();
        log.visible = true;
        log.isGuide = true;
        log.dice = this.DiceConvert(log.text);


        if (guide.isYouTube) {
            log.text = Resource.YOUTUBE + "\n" + guide.value;
            log.isGuide = false;
            this.CreateEmbedStage(YouTubeUtil.ToEmbedYouTubeID(guide, true));
        }

        this.MsgList.Messages.push(log);
        this.DB.Write(SkyBezieDB.MESSAGE, log.mid, log);
        this.DB.Write(SkyBezieDB.SYSTEM, SenderType.Room, this.Room);

        return log;
    }


    /**
     * メッセージの更新
     * @param log
     */
    public static UpdateMsgLog(log: MsgLog): MsgLog {


        this.MsgList.Messages.forEach(msg => {

            if (log.mid === msg.mid) {
                msg.text = log.text;
                msg.visible = log.visible;
                msg.date = log.date;
            }

        });

        this.DB.Write(SkyBezieDB.MESSAGE, log.mid, log);

        return log;
    }


    /**
     * ログイン情報の設定
     * @param connlabel
     * @param profile
     */
    public static SetLoginInfo(connlabel: string, profile: Profile) {
        this.ConnectionMap.set(connlabel, profile);
        this.SetProfileLoginFlg();
    }



    /**
     * 切断情報
     * @param connlabel
     */
    public static RemoveConnection(connlabel: string) {

        this.InputMap.ReomveIng(this.ProfileList, this.ConnectionMap.get(connlabel));

        this.ConnectionMap.delete(connlabel);
        this.RemoveStageService(connlabel);
        this.SetProfileLoginFlg();

        WebRTCService.ChildSendAll(this.ProfileList);
    }


    /**
     * ログイン状態をプロフィールにセット
     */
    public static SetProfileLoginFlg() {
        this.ProfileList.List.forEach((p) => { p.islogin = false; });
        this.ConnectionMap.forEach((v, k) => { v.islogin = true; });
    }


    /**
     * ステージ管理
     * @param connlabel
     * @param stage
     */
    public static SetStageService(connlabel: string, stage: StageService): StageServiceList {

        if (this.StageMap.has(connlabel)) {
            if (!stage.enabled) {
                this.StageMap.delete(connlabel);
            }
            else {
                //  Url変更のケースもあるので再セットする
                this.StageMap.set(connlabel, stage);
            }
        }
        else {
            if (stage.enabled)
                this.StageMap.set(connlabel, stage);
        }

        return this.GetStageService();
    }


    /**
     * 
     */
    public static GetStageService(): StageServiceList {
        var list = new StageServiceList();
        this.StageMap.forEach((v, u) => list.List.push(v));

        return list;
    }


    /**
     * ステージ情報の更新通知処理
     */
    public static ChangeStageService() {
        var list = this.GetStageService();
        WebRTCService.ChildSendAll(list);
        StageLayout.Create();
    }


    /**
     * ステージサービスの削除
     * @param connlabel
     */
    public static RemoveStageService(connlabel: string) {

        //  ステージ登録されている場合、切断情報を全クライアントに通知する
        if (this.StageMap.has(connlabel)) {

            this.StageMap.delete(connlabel);

            this.ChangeStageService();
        }
    }


    /**
     * YouTube等の埋込ページステージの生成
     * @param url
     */
    public static CreateEmbedStage(url: string) {

        var stage = new StageService();
        stage.pid = 0;
        stage.url = url;
        stage.name = Resource.YOUTUBE;
        stage.enabled = true;

        //  Stage管理と通知
        var stages = ServerDataStore.SetStageService(Resource.YOUTUBE, stage);

        //  各クライアントにYouTubeの通知
        this.ChangeStageService();
    }


    /**
     * ダイスロールがあった場合に変換します
     * @param text
     */
    public static DiceConvert(text: string): string {

        var re = new RegExp('^[0-9０-９][dDｄＤ][0-9０-９]+');
        var ma: RegExpMatchArray = text.match(re);

        if (ma) {
            var dicestr: string = ma[0];
            //  全角を半角に変換
            dicestr = dicestr.replace(/[Ａ-Ｚａ-ｚ０-９]/g, (s: string) => String.fromCharCode(s.charCodeAt(0) - 65248));

            var split: string[] = dicestr.toLowerCase().split('d');
            var diceCount: number = Number(split[0]);
            var diceTarget: number = Number(split[1]);

            var total: number = 0;
            var work: string = '';

            for (var i = 0; i < diceCount; i++) {
                var rand = Math.floor(Math.random() * diceTarget) + 1;
                total += rand;
                work += ',' + rand.toString();
            }

            var diceresult: string = dicestr + ' → ' + total.toString();

            if (diceCount > 1) {
                diceresult += '[' + work.substr(1) + ']';
            }

            return diceresult;
        }
        else {
            return '';
        }

    }


}
