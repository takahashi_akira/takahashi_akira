﻿/// <reference path="../../Lib/typings/tsd.d.ts" />


/*
 *  グループ描画クラス
 */
class ActorGroup {


    public static _groupList: Array<ActorGroupInfo>;


    /**
     * 初期化処理
     * @param groupList
     */
    public static Initilize(groupList: Array<ActorGroupInfo>) {

        this._groupList = groupList;

        if (this._groupList == null || this._groupList.length == 0) {
            //  グループが未設定の場合、「接続メンバ」を作成する
            var lounge = new ActorGroupInfo();
            lounge.groupid = 0;
            lounge.name = "接続メンバ";
            this._groupList.push(lounge);
            WebRTCService.ChildSendAll(this.GetActorGroupList());
        }

        ActorGroup.Create();
    }


    /**
     * 生成処理（描画処理）
     */
    public static Create() {

        var element: HTMLElement = document.getElementById('actor-group-list');
        ReactDOM.render(<GroupComponent groups={this._groupList}/>, element, () => {
        });
    }


    /**
     * グループの新規追加
     */
    public static AddNew() {
        var group = new ActorGroupInfo();
        group.groupid = ServerDataStore.Room.max_group_id++;
        group.name = "Group" + group.groupid;
        ServerDataStore.SetRoomInfo();

        this._groupList.push(group);
        this.UpdateRecv(group);
    }


    /**
     * グループの更新
     * @param info
     */
    public static UpdateRecv(info: ActorGroupInfo) {
        ServerDataStore.DB.Write<ActorGroupInfo, number>(SkyBezieDB.ACTORGROUP, info.groupid, info);
        ActorGroup.Create();
        WebRTCService.ChildSendAll(this.GetActorGroupList());
    }


    /**
     * グループの削除
     * @param info
     */
    public static DeleteRecv(info: ActorGroupInfo) {

        ServerDataStore.DB.Delete<ActorGroupInfo, number>(SkyBezieDB.ACTORGROUP, info.groupid);
        ActorGroup.Create();
    }


    /**
     * グループ情報の取得
     */
    public static GetActorGroupList(): ActorGroupInfoList {
        var result = new ActorGroupInfoList();
        result.List = this._groupList;
        return result;
    }

}

interface ActorGroupInfoListProp {
    groups: Array<ActorGroupInfo>
}


/**
 * グループ一覧コンポーネント
 */
class GroupComponent extends React.Component<ActorGroupInfoListProp, any> {


    /**
     * グループ一覧の描画
     */
    public render() {

        var nogroup = this.GetPidMap();
        var lounge: ActorGroupInfo = null;

        var groupNodes = this.props.groups.map((cur) => {

            if (cur.groupid == 0) {
                lounge = cur;
            } else {
                //  グループに含まれるキャラクターは除外する
                cur.pidlist.forEach((pid) => { nogroup.delete(pid); });
                //  グループリストの表示
                return (<GroupListComponent info={cur} />);
            }
        });

        //  どのグループにも所属していないアクターはラウンジに配置する
        if (lounge == null) {
            lounge = new ActorGroupInfo();
            lounge.groupid = 0;
            lounge.name = "ラウンジ";
        }

        lounge.pidlist = new Array<number>();
        nogroup.forEach((v, k) => { lounge.pidlist.push(k); });

        return (
            <div className='mdl-grid'>
                <GroupListComponent info={lounge} />
                {groupNodes}
            </div>
        );
    }

    /**
     * 
     */
    private GetPidMap(): Map<number, boolean> {

        var result = new Map<number, boolean>();
        var actorList = ServerDataStore.GetProfiles().forEach((p) => { result.set(p.pid, true); });
        return result;
    }

}


/**
 * グループ一覧プロパティ
 */
interface GroupListProp {
    info: ActorGroupInfo;
}


/**
 * グループ一覧ステータス
 */
interface GroupListStat {
    visible: boolean;
    groupname: string;
    stageid: string;
    stagesetid: string;
    info: ActorGroupInfo;
}


/**
 *  グループメンバーコンポーネント
 */
class GroupListComponent extends React.Component<GroupListProp, GroupListStat>{


    /**
     * 
     * @param props
     * @param context
     */
    constructor(props?: GroupListProp, context?: any) {
        super(props, context);

        if (props.info.groupid < 0) {
            props.info.groupid = ServerDataStore.Room.max_group_id;
            props.info.name = "グループ " + props.info.groupid;
            ServerDataStore.Room.max_group_id++;
            ServerDataStore.SetRoomInfo();
        }

        //  初期化はコンストラクタで行う
        this.state = {
            visible: true,
            groupname: props.info.name,
            stageid: props.info.stageurl,
            stagesetid: props.info.stagesetid,
            info: this.props.info
        };
    }


    /**
     * グループリストの描画
     */
    public render() {

        var actorgroup = this.props.info;
        var actorList = ServerDataStore.GetProfiles();


        var isCloseHidden: boolean = false;

        //  ラウンジの削除は許可しない
        if (this.props.info.groupid == 0)
            isCloseHidden = true;

        var actorNodes = this.props.info.pidlist.map((pid) => {

            isCloseHidden = true;  //  アクターが設定されているグループは削除不可
            var prof = ServerDataStore.GetProfile(pid);

            if (prof.visible) {
                var owner = ServerDataStore.GetProfile(prof.owner);
                return (<GroupMemberComponent profile={prof} owner={owner} />);
            }
        });

        return (
            <div className='group-panel mdl-cell mdl-cell--4-col mdl-cell--6-col-tablet mdl-cell--6-col-phone mdl-card mdl-shadow--3dp' hidden={!this.state.visible} onDrop={this.onDrop.bind(this) }>
                <div className="group-title mdl-card__title mdl-shadow--2dp">
                    <div className="mdl-textfield mdl-js-textfield" spellcheck="false">
                        <input className="mdl-textfield__input" type="text" value={this.state.groupname} onChange={(event: React.FormEvent) => { this.changeTitle(event); } }/>
                    </div>
                    <button className="mdl-button mdl-js-button mdl-js-ripple-effect" tabIndex="-1" onClick={this.onClick.bind(this) } hidden={isCloseHidden} >
                        <i className="material-icons">delete_forever</i>
                    </button>
                </div>

                <div className="mdl-card__actions mdl-card--border">
                    {actorNodes }
                </div>
            </div >
        );
    }


    /**
     * テキスト変更時処理
     * @param event
     */
    private changeTitle(event) {
        this.setState({
            visible: true,
            groupname: event.target.value,
            stageid: this.state.stageid,
            stagesetid: this.state.info.stagesetid,
            info: this.state.info
        });
        this.props.info.name = event.target.value;

        ActorGroup.UpdateRecv(this.state.info);
    }


    /**
     * 「DELETE」クリック時処理
     * @param ev
     */
    private onClick(ev) {
        this.setState({
            visible: false,     //  グループデータは削除せず非表示とするだけ
            groupname: this.state.groupname,
            stageid: this.state.stageid,
            stagesetid: this.state.info.stagesetid,
            info: this.state.info
        });

        ActorGroup.DeleteRecv(this.state.info);
    }


    /**
     * ドロップ時イベント
     * @param ev
     */
    private onDrop(ev: DragEvent) {

        var pid = Number.parseInt(ev.dataTransfer.getData("text"));

        ActorGroup._groupList.forEach((info) => {

            var isRemove = false;
            var newlist = new Array<number>();

            info.pidlist.forEach((rpid) => {
                if (pid == rpid)
                    isRemove = true;
                else
                    newlist.push(rpid);
            });

            if (isRemove) {
                info.pidlist = newlist;
                ActorGroup.UpdateRecv(info);
            }
        });


        this.state.info.pidlist.push(pid);

        this.setState({
            visible: this.state.visible,
            groupname: this.state.groupname,
            stageid: this.state.stageid,
            stagesetid: this.state.info.stagesetid,
            info: this.state.info
        });

        ActorGroup.UpdateRecv(this.state.info);
    }

}


/**
 *  グループメンバープロパティ
 */
interface GroupMemberProp {
    profile: Profile;
    owner: Profile;
}


/**
 * グループメンバーコンポーネント
 */
class GroupMemberComponent extends React.Component<GroupMemberProp, any>{

    render() {

        var prof = this.props.profile
        var owner = this.props.owner;
        var name = prof.name;

        if (prof.pid != owner.pid)
            name += "（" + owner.name + "）";

        return (
            <div itemID='group-member' className='group-member mdl-button mdl-js-button mdl-button--raised mdl-button--colored' draggable='true' onDragStart={this.onDragStart.bind(this) }>
                <span className="mdl-tooltip" for="group-member">ドラック＆ドロップでグループを移動できます</span>
                {name}
            </div>
        );
    }

    private onDragStart(ev: DragEvent) {
        ev.dataTransfer.setData("text", this.props.profile.pid.toString());
    }

}