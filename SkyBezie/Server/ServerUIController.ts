﻿
class ServerUIController {


    /**
     * 初期化処理
     * @param server
     */
    public static Initialize(server: ServerController) {

        ServerConsole.Initilize();

        Util.StopPropagation();
        this.SetEvent();
    }


    /**
     * イベント処理の設定
     */
    public static SetEvent() {

        //  壁紙
        $('#navi-wallpaper').click((e) => {
            Wallpaper.OpenDialog();
        });

        //  エクスポート
        $('#navi-export').click((e) => { this.Export(); });

        //  メニューの表示有無
        $('#switch-disp-menu').click((e) => {
            this.ChangeLayout();
        });

        //  参加者パネルの表示有無
        $('#switch-disp-actor').click((e) => {
            this.ChangeLayout();
        });

        //  メッセージログ・メッセージ入力の表示有無
        $('#switch-disp-message').click((e) => {
            this.ChangeLayout();
        });

        //  音声ガイド
        $('#skybezie-add-guide').click(e => {
            GuideList.Create();
            GuideList.AddNew();
        });

        //  YouTubeリンク
        $('#skybezie-add-youtube').click(e => {
            YouTubeList.Create();
            YouTubeList.AddNew();
        });

        //  YouTubeの停止
        $('#skybezie-stop-youtube').click(e => {
            ServerDataStore.RemoveStageService(Resource.YOUTUBE);
        });

        //  グループ
        $('#skybezie-add-group').click(e => {
            ActorGroup.Create();
            ActorGroup.AddNew();
        });

    }


    /**
     * 
     * @param room
     */
    public static SetRoomInfo(room: Room) {

        //  タイトルの設定
        $('#room-title').val(room.title);

        //  表示が崩れるので、入力イベントを強制的に発生させる
        document.getElementById('room-title').dispatchEvent(new Event('input'));

        //  部屋名称
        $('#room-title').change((e) => {
            room.title = $('#room-title').val();
            ServerDataStore.SetRoomInfo();
        });

        //  メッセージ数（非表示項目）
        $('#msg-disp-count').val(room.msg_disp_count).change((e) => {
            room.msg_disp_count = $('#msg-disp-count').val();
            ServerDataStore.SetRoomInfo();
        });

    }


    /**
     * データのエクスポート処理
     */
    private static Export() {

        var now = new Date();

        var format = now.getFullYear() + "-"
            + ("0" + (now.getMonth() + 1)).slice(-2) + "-"
            + ("0" + now.getDate()).slice(-2) + "-"
            + ("0" + now.getHours()).slice(-2) + "-"
            + ("0" + now.getMinutes()).slice(-2) + "-"
            + ("0" + now.getSeconds()).slice(-2);

        var filename = $('#room-title').val() + format + ".skybezie";

        ServerDataStore.DB.ReadAllData((data) => {

            var str = JSON.stringify(data);
            str = this.JsonFormatter(str);

            this.str2bytes(str, (buffer: ArrayBuffer) => {

                if (window.navigator.msSaveBlob) {
                    window.navigator.msSaveBlob(new Blob([buffer], { type: "text/plain" }), filename);
                } else {
                    var a: any = document.createElement("a");
                    a.href = URL.createObjectURL(new Blob([buffer], { type: "text/plain" }));
                    a.download = filename;
                    document.body.appendChild(a) //  FireFox specification
                    a.click();
                    document.body.removeChild(a) //  FireFox specification
                }
            });

        });
    }

    private static _layout = new Layout();


    /**
     * レイアウト変更イベント
     */
    private static ChangeLayout() {

        if (!ServerDataStore.Layout)
            ServerDataStore.Layout = new Layout();

        ServerDataStore.Layout.dispMenu = $('#switch-disp-menu').prop('checked');
        ServerDataStore.Layout.dispActors = $('#switch-disp-actor').prop('checked');
        ServerDataStore.Layout.dispMessage = $('#switch-disp-message').prop('checked');

        WebRTCService.ChildSendAll(ServerDataStore.Layout);
    }


    /**
     * Json文字列の加工
     * 改行コードを入れテキストエディタで加工しやすい形にする
     * @param str
     */
    private static JsonFormatter(str: string): string {

        str = str.replace(/},/g, '},\r\n');
        str = str.replace(/],/g, '],\r\n');
        str = str.replace(/}],/g, '}\r\n],');
        str = str.replace(/\[/g, '\[\r\n');

        return str;
    }


    /**
     * 文字列をバイナリに変換
     */
    private static str2bytes(str, callback) {
        var fr = new FileReader();
        fr.onloadend = function () {
            callback(fr.result);
        };
        fr.readAsArrayBuffer(new Blob([str]));
    }

}
