﻿

class StageSetImage extends ImageDrop {

    static _stageSetArray: Array<StageSetInfo>;
    static _thumbnailMap: Map<string, ImageRec>;


    //
    static Initilize(StageSetList: Array<StageSetInfo>) {
        this._stageSetArray = StageSetList;
        this._thumbnailMap = new Map<string, ImageRec>();

        StageSetList.forEach(StageSet => {
            ServerDataStore.DB.Read<ImageRec, string>(SignpostDB.IMAGE, StageSet.thumbnail_uuid, (r: ImageRec) => {
                this._thumbnailMap.set(r.uuid, r);
            });
        });

        var mdiv = new ModalDiv('stageset-image', 'stageset-image-close');

        $('#icon_stageset_delete').click(() => StageSetImage.DeleteClose());
        $('#icon_stageset_update').click(() => StageSetImage.ChangeClose());

        $('#stageset-image-title').keypress((e) => {
            if (e.keyCode == 13)
                StageSetImage.ChangeClose();
        });

        new ImageDrop().InitilizeImageDrop($('#stageset-image-selector-drop'), $('#stageset-image-selector-file'), StageSetImage.OnDropImage);
    }


    //
    static GetStageSetInfoList(): StageSetInfoList {
        var result = new StageSetInfoList();
        result.List = this._stageSetArray;
        return result;
    }


    //
    static GetStageSetInfoArray(): Array<StageSetInfo> {
        return this._stageSetArray;
    }


    static Dispaly(redrow: boolean = false) {

        var element = document.getElementById('stageset-list');

        if (redrow)
            StageSetDisplay.Create(element, new Array<StageSetInfo>());

        StageSetDisplay.Create(element, this.GetStageSetInfoArray());
    }

    static Send() {
        WebRTCService.AllClientSend(this.GetStageSetInfoList());
    }


    //
    static OpenNewDialog() {

        $('#icon_stageset_delete').hide();
        $("#stageset-image-title").val("");
        this.Open(ImageType.StageSet, null, (imgRec) => {

            if (imgRec.visible)
                this.UpdateStageSet(null, imgRec);
        });
    }

    //
    static OpenDialog(StageSetid: string) {

        $('#icon_stageset_delete').show();

        ServerDataStore.DB.Read<StageSetInfo, string>(SignpostDB.STAGESET, StageSetid, (r) => {

            var StageSet: StageSetInfo = r;

            if (StageSet == null)
                return;

            ServerDataStore.DB.Read<ImageRec, string>(SignpostDB.IMAGE, StageSet.image_uuid_list[0], (imgRec) => {

                $("#stageset-image-title").val(StageSet.name);

                this.Open(ImageType.StageSet, imgRec, (r) => {

                    if (r.visible)
                        this.UpdateStageSet(StageSet, r);
                    else
                        this.DeleteStageSet(StageSet, r);
                });
            });
        });
    }


    static UpdateStageSet(StageSet: StageSetInfo, imgRec: ImageRec) {

        if (imgRec === null || imgRec.src === null || imgRec.src === undefined)
            return;

        if (StageSet == null) {

            //  ステージセット登録
            StageSet = new StageSetInfo();
            StageSet.stagesetid = Util.uuid();

            //  画像のuuid割当
            var uuid = Util.uuid();
            StageSet.image_uuid_list = new Array<string>();
            StageSet.image_uuid_list.push(uuid);

            //  サムネイルのuuid割当
            StageSet.thumbnail_uuid = Util.uuid();

            this._stageSetArray.push(StageSet);
        }
        else {
            //  ステージセットの更新
            this._stageSetArray.forEach(pre => {
                if (pre.stagesetid == StageSet.stagesetid)
                    StageSet = pre;
            });
        }

        StageSet.name = $("#stageset-image-title").val();

        if (StageSet.name.length == 0)
            StageSet.name = "No Title";

        //  画像の登録／更新
        imgRec.uuid = StageSet.image_uuid_list[0];

        if (imgRec.visible) {

            ServerDataStore.DB.Write(SignpostDB.IMAGE, imgRec.uuid, imgRec, () => {

                //  画像の圧縮
                ImageUtil.ImgB64Compress(imgRec, () => {

                    //  サムネイル画像の登録
                    imgRec.uuid = StageSet.thumbnail_uuid;
                    ServerDataStore.DB.Write(SignpostDB.IMAGE, imgRec.uuid, imgRec);

                    //  サムネイル画像のキャッシュ
                    this._thumbnailMap.set(imgRec.uuid, imgRec);

                    //  ステージセット情報の登録
                    ServerDataStore.DB.Write(SignpostDB.STAGESET, StageSet.stagesetid, StageSet);

                    //  ステージセットリストの再描画
                    this.Dispaly(true);

                    //
                    this.Send();

                });

            });
        }
    }


    /*
     *  ステージセット情報の再描画
     */
    static DeleteStageSet(StageSet: StageSetInfo, imgRec: ImageRec) {

        var newList = new Array<StageSetInfo>();
        this._stageSetArray.forEach((s) => {

            if (s.stagesetid !== StageSet.stagesetid)
                newList.push(s);
        });
        this._stageSetArray = newList;

        //  ステージセットの削除
        ServerDataStore.DB.Delete(SignpostDB.STAGESET, StageSet.stagesetid);

        //  サムネイル画像の削除
        ServerDataStore.DB.Delete(SignpostDB.IMAGE, StageSet.thumbnail_uuid);

        //  画像の削除
        StageSet.image_uuid_list.forEach((uuid) => {
            ServerDataStore.DB.Delete(SignpostDB.IMAGE, uuid);
        });

        //  ステージセットリストの再描画
        this.Dispaly(true);

        //
        this.Send();
    }


    static GetImageRec(uuid: string): ImageRec {

        var rec = this._thumbnailMap.get(uuid);

        if (rec == null)
            return new ImageRec(ImageType.StageSet);
        else
            return rec;
    }


    static _imageType: ImageType;
    static _imageRec: ImageRec;
    static _callback: OnChangeImage;

    //
    static Open(imageType: string, image: ImageRec, onchange: OnChangeImage) {

        this._imageType = new ImageType(imageType);
        this._callback = onchange;

        this.SetImage(image);

        document.getElementById('stageset-image').hidden = false;
    }


    static SetImage(image: ImageRec) {

        if (image == null) {
            image = this._imageType.CreateImageRec();
        }

        this._imageRec = image

        var div = $('#stageset-image-selector-drop');

        div.css("width", "100%").css("height", "100%")

        $('#image-dialog-size').val(image.backgroundsize);
        $('#image-background-repeat').val(image.backgroundrepeat);
        $('#image-background-position').val(image.backgroundposition);

        if (image.src == null || image.src.length == 0) {
            div.css("background", "");
        }
        ImageRec.setCss(div, image);
    }

    //
    static Claer() {
        this.SetImage(this._imageType.CreateImageRec());
    }

    //
    private static statusChange() {

        var dummy = this._imageType.CreateImageRec();
        dummy.src = this._imageRec.src;
        this.setBackGroundStatus(dummy);
        this.SetImage(dummy);
    }

    //
    private static setBackGroundStatus(rec: ImageRec) {
        rec.backgroundsize = $('#image-dialog-size').val();
        rec.backgroundrepeat = $('#image-background-repeat').val();
        rec.backgroundposition = $('#image-background-position').val();
    }


    //
    static DeleteClose() {

        this._imageRec.visible = false;
        this._callback(this._imageRec);
        this.Close();
    }


    //
    static ChangeClose() {
        this._callback(this._imageRec);
        this.Close();
    }


    //
    static Close() {
        document.getElementById('stageset-image').hidden = true;
    }


    //
    static CreateImageRec(src): ImageRec {
        var rec = StageSetImage._imageType.CreateImageRec();
        rec.src = src;
        this.setBackGroundStatus(rec);
        return rec;
    }


    //  画像ドロップ時イベント
    static OnDropImage(file: File, src) {
        var rec = StageSetImage.CreateImageRec(src);
        StageSetImage.SetImage(rec);
    }

}