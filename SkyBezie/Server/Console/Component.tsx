﻿/// <reference path="../../Lib/typings/tsd.d.ts" />


//  最大出力ログ件数
const MAX_LOG_COUNT = 128;

//  １行あたりの最大出力文字数
const MAX_LOG_LENGTH = 80;


/**
 * サーバーコンソール
 */
class ServerConsole implements ILogListener {

    //
    public static _log: Array<Log>;

    //
    public static _element: HTMLElement;


    /**
     * 初期化処理
     */
    public static Initilize() {
        this._element = document.getElementById('skybezie-console-log');
        this._log = new Array<Log>();
        LogUtil.AddListener(new ServerConsole());
    }


    /**
     * ログを設定する
     * @param value
     */
    private static SetLog(value: Log) {

        //  128行を超えるログは削除する
        if (ServerConsole._log.length > MAX_LOG_COUNT)
            ServerConsole._log.shift();

        ServerConsole._log.push(value);

        ReactDOM.render(<ConsoleComponent Logs={ServerConsole._log}/>, ServerConsole._element, () => { });

        //  最終行に移動する（JQueryでの操作であることに注意）
        if ($('#scroll-tab-log').hasClass("is-active")) {
            $('#skybezie-console').scrollTop(4294967295);
        }
    }


    /**
     * ログの書込み
     * @param value
     */
    public Write(value: Log) {
        ServerConsole.SetLog(value);
    }

}

interface LogListProp {
    Logs: Array<Log>
}


/**
 *  ログ表示処理
 */
class ConsoleComponent extends React.Component<LogListProp, any> {

    /**
     * ログの描画処理
     */
    public render() {

        var logNodes = this.props.Logs.map((cur) => {

            var msg = cur.Message;

            if (msg.length > MAX_LOG_LENGTH)
                msg = msg.substring(0, MAX_LOG_LENGTH) + " ...";

            return (<div className='console-log'><span className='console-time'>{cur.DispDateTime() }</span> {msg}</div>);
        });

        return (
            <div>
                {logNodes}
            </div>
        );

    }

}


