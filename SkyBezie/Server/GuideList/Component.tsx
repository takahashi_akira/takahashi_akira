﻿/// <reference path="../../Lib/typings/tsd.d.ts" />


/**
 * ガイド描画クラス
 */
class GuideList {


    public static _guideList: Array<Guide>;


    /**
     * 初期化処理
     * @param guideList
     */
    public static Initilize(guideList: Array<Guide>) {

        this._guideList = new Array<Guide>();

        guideList.forEach(n => {
            if (!n.isYouTube)
                this._guideList.push(n);
        });

        GuideList.Create();
    }


    /**
     * 
     */
    public static Create() {

        var element: HTMLElement = document.getElementById('guide-list');
        ReactDOM.render(<GuideComponent guides={this._guideList}/>, element, () => {
        });
    }


    /**
     * ガイドの新規追加
     */
    public static AddNew() {
        var guide = new Guide();
        guide.guideid = ServerDataStore.Room.max_guide_id++;

        ServerDataStore.SetRoomInfo();

        this._guideList.push(guide);
        this.UpdateRecv(guide);

        $('#voice-gide-list').scrollTop(4294967295);
    }


    /**
     * ガイドの更新
     * @param info
     */
    public static UpdateRecv(info: Guide) {
        ServerDataStore.DB.Write<Guide, number>(SkyBezieDB.GUIDE, info.guideid, info);
        GuideList.Create();
    }


    /**
     * ガイドの削除
     * @param info
     */
    public static DeleteRecv(info: Guide) {

        ServerDataStore.DB.Delete<Guide, number>(SkyBezieDB.GUIDE, info.guideid);
        GuideList.Create();
    }


    /**
     * ガイド情報の取得
     */
    public static GetGuideList(): Array<Guide> {
        return this._guideList;
    }

}

interface GuideProp {
    guides: Array<Guide>
}


/**
 * ガイド一覧コンポーネント
 */
class GuideComponent extends React.Component<GuideProp, any> {


    /**
     * 
     */
    public render() {

        var guideNodes = this.props.guides.map((cur) => {

            //  ガイドリストの表示
            return (<GuideItemComponent info={cur} />);

        });

        return (
            <ul className="mdl-list">
                {guideNodes}
            </ul>
        );
    }

}


/**
 * ガイド一覧プロパティ
 */
interface GuideItemProp {
    info: Guide;
}


/**
 * ガイド一覧ステータス
 */
interface GuideItemStat {
    visible: boolean;
    guide_id: string;
    guidekey: string;
    message: string;
    info: Guide;
}


/**
 * ガイドメンバーコンポーネント
 */
class GuideItemComponent extends React.Component<GuideItemProp, GuideItemStat>{

    /**
     * コンストラクタ
     * @param props
     * @param context
     */
    constructor(props?: GuideItemProp, context?: any) {
        super(props, context);

        //  初期化はコンストラクタで行う
        this.state = {
            visible: true,
            guide_id: Resource.GUIDE + this.props.info.guideid,
            guidekey: this.props.info.key,
            message: this.props.info.value,
            info: this.props.info
        };
    }


    /**
     * 
     */
    public render() {

        var GuideList = this.props.info;


        return (
            <li className="mdl-list__item" hidden={!this.state.visible}>
                <span className="mdl-list__item-primary-content">
                    <button className="mdl-button guide-button" tabIndex="-1" onClick={this.onMicClick.bind(this) } >
                        <i className="material-icons mdl-list__item-avatar">mic</i>
                    </button>
                    <div className='guide-div-box'>
                        <div className="mdl-textfield mdl-js-textfield guide-keyword" spellcheck="false">
                            <input className="mdl-textfield__input guide-keyword-font" placeholder="キーワード" type="text" value={this.state.guidekey} onChange={(event: React.FormEvent) => { this.changeKeyword(event); } }/>
                        </div>
                        <div className="mdl-textfield mdl-js-textfield guide-message">
                            <textarea className="mdl-textfield__input" placeholder="音声ガイドメッセージ" type="text" rows= "3" value={this.state.message} onChange={(event: React.FormEvent) => { this.changeMessage(event); } }></textarea>
                        </div>
                    </div>
                </span>
                <span className="mdl-list__item-secondary-content">
                    <button className="mdl-button mdl-js-button mdl-js-ripple-effect" tabIndex="-1" onClick={this.onDeleteClick.bind(this) } >
                        <i className="material-icons">delete_forever</i>
                    </button>
                </span>
            </li >
        );
    }


    /**
     * テキスト変更時イベント
     * @param event
     */
    private changeKeyword(event) {
        this.setState({
            visible: true,
            guide_id: this.state.guide_id,
            guidekey: event.target.value,
            message: this.state.message,
            info: this.state.info
        });
        this.props.info.key = event.target.value;

        GuideList.UpdateRecv(this.state.info);
    }


    /**
     * メッセージ変更時イベント
     * @param event
     */
    private changeMessage(event) {
        this.setState({
            visible: true,
            guidekey: this.state.guidekey,
            guide_id: this.state.guide_id,
            message: event.target.value,
            info: this.state.info
        });
        this.props.info.value = event.target.value;

        GuideList.UpdateRecv(this.state.info);
    }


    /**
     * 「Mic」クリック時処理
     * @param ev
     */
    private onMicClick(ev) {

        ServerDataStore.SetGuide(this.state.info);
        WebRTCService.ChildSendAll(ServerDataStore.MsgList);

    }


    /**
     * 「DELETE」クリック時処理
     * @param ev
     */
    private onDeleteClick(ev) {
        this.setState({
            visible: false,     //  ガイドデータは削除せず非表示とするだけ
            guide_id: this.state.guide_id,
            guidekey: this.state.guidekey,
            message: this.state.message,
            info: this.state.info
        });

        GuideList.DeleteRecv(this.state.info);
    }

}


