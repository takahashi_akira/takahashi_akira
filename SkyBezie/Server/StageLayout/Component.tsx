﻿/// <reference path="../../Lib/typings/tsd.d.ts" />


/*
 *  ステージ描画クラス
 */
class StageLayout {


    /**
     * 初期化処理
     * @param groupList
     */
    public static Initilize() {
        StageLayout.Create();
    }


    /**
     * 生成処理（描画処理）
     */
    public static Create() {

        var stageList: StageServiceList = ServerDataStore.GetStageService();

        var element: HTMLElement = document.getElementById('stage-layout');
        ReactDOM.render(<StageLayoutComponent stageList={stageList}/>, element, () => {

        });
    }

}

interface StageLayoutInfoListProp {
    stageList: StageServiceList
}


/**
 * ステージレイアウトコンポーネント
 */
class StageLayoutComponent extends React.Component<StageLayoutInfoListProp, any> {


    /**
     * ステージ一覧の描画
     */
    public render() {

        var groupNodes = this.props.stageList.List.map((cur) => {

            if (cur.enabled) {
                //  ステージリストの表示
                return (<StageLayoutItemComponent stage={cur} />);
            }

        });

        return (
            <div className='mdl-grid'>
                {groupNodes}
            </div>
        );
    }
}


/**
 * ステージ一覧プロパティ
 */
interface StageLayoutItemProp {
    stage: StageService
}


/**
 * ステージ一覧ステータス
 */
interface StageLayoutItemStat {
    clienthide: boolean;
    stage: StageService;
}


/**
 *  ステージアイテムコンポーネント
 */
class StageLayoutItemComponent extends React.Component<StageLayoutItemProp, StageLayoutItemStat>{


    /**
     * コンストラクタ
     * @param props
     * @param context
     */
    constructor(props?: StageLayoutItemProp, context?: any) {
        super(props, context);

        //  初期化はコンストラクタで行う
        this.state = {
            clienthide: this.props.stage.hide,
            stage: this.props.stage,
        };
    }


    /**
     * ステージリストの描画
     */
    public render() {

        //  ステージ情報
        var stage = this.props.stage;

        //  ステージオーナーのプロフ
        var pid = this.state.stage.pid;

        var profile = ServerDataStore.GetProfile(pid);
        var dispname = (profile === null ? stage.name : profile.name);
        var switchid = "switch-stage-visibled-" + pid;

        return (
            <div className='group-panel mdl-cell mdl-cell--6-col mdl-card mdl-shadow--3dp' onDrop={this.onDrop.bind(this) }>

                <h4>
                    {dispname}
                </h4>

                <button className="mdl-button mdl-js-button mdl-js-ripple-effect" hidden={!this.state.clienthide} tabIndex="-1" onClick={this.onViewOffClick.bind(this) } >
                    <i className="material-icons">videocam_off</i>
                    <span>クリックでステージを表示します</span>
                </button>

                <button className="mdl-button mdl-js-button mdl-js-ripple-effect" hidden={this.state.clienthide} tabIndex="-1" onClick={this.onViewOnClick.bind(this) } >
                    <i className="material-icons">videocam</i>
                    <span>クリックでステージを非表示にします</span>
                </button>

            </div>
        );
    }


    /**
     * 「表示切替」クリック時処理
     * @param ev
     */
    private onViewOnClick(ev) {

        this.setState({
            clienthide: true,
            stage: this.state.stage,
        });

        this.state.stage.hide = true;
        this.StageStatusChange(this.state.stage);
    }


    /**
     * 「表示切替」クリック時処理
     * @param ev
     */
    private onViewOffClick(ev) {

        this.setState({
            clienthide: false,
            stage: this.state.stage,
        });

        this.state.stage.hide = false;
        this.StageStatusChange(this.state.stage);
    }


    /**
     * ステージのステータス変更の
     * @param hide
     */
    private StageStatusChange(stage: StageService) {
        ServerDataStore.ChangeStageService();
    }


    /**
     * ドロップ時イベント
     * @param ev
     */
    private onDrop(ev: DragEvent) {

        var pid = Number.parseInt(ev.dataTransfer.getData("text"));

    }

}