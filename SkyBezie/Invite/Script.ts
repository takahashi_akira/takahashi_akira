﻿

$(function () {

    var id = Util.GetPeerID();
    var url: string = Util.CreateLink("../Client", id, null, false);
    $('#invite-url').val(url);

    var qrcode: any = $('#qrcode');
    qrcode.qrcode(url);


    $('#SkyBezie-client').click((e) => {
        window.open(url, '_blank');
    });

});