﻿
class ActorUIController {


    public static Profile: Profile;


    public static Initilize() {

        $('#act-edit-background').click((ev: JQueryEventObject) => {
            if (ev.target.id == 'act-edit-background')
                this.Close();
        });

        $('#edit-cancel').click(() => this.Close());

        $('#actor-delete').click(() => this.Delete());
        $('#actor-update').click(() => this.Update());

        $('#actor-name').keyup((e) => this.KeyEvent(e));
        $('#actor-memo').keyup((e) => this.KeyEvent(e));
        $('#actor-url').keyup((e) => this.KeyEvent(e));

    };


    private static KeyEvent(e: JQueryKeyEventObject) {

        if (e.keyCode == 13) {
            this.Update();
        }

        if (e.keyCode == 27) {
            this.Close();
        }

    }


    private static Close() {
        window.close();
    }


    ////
    //public static Open(pid: number) {


    //    $("div#act-edit-contnts").width($('div#contents_inputbox').width() - 25);
    //    var width = $("div#act-edit-contnts").width();

    //    var name = $('#act-edit-name');
    //    var status = $('#act-edit-status');
    //    var url = $('#act-edit-url');

    //    //  '0'が指定された場合は新規追加
    //    if (pid == 0) {
    //        name.prop('disabled', false);
    //        $('#act-edit-delete').hide();
    //        this._profile = new ActorProfile();
    //        this._profile.owner = Login.Profile.pid;
    //    }
    //    else {
    //        name.prop('disabled', true);
    //        this._profile = ActorList.FindProfile(pid);

    //        ////  プレイヤーの削除は不可
    //        ////  キャラクターのみ削除可能とする
    //        //if (this._profile.ptype == 'P')
    //        //    $('#act-edit-delete').hide();
    //        //else
    //        //    $('#act-edit-delete').show();
    //    }

    //    var input_width = width - 114;
    //    name.val(this._profile.name).width(input_width);
    //    status.val(this._profile.status).width(input_width);
    //    url.val(this._profile.URL).width(input_width);


    //    $('#act-edit-background').show();

    //    if (pid == 0)
    //        name.focus();
    //    else
    //        status.focus();
    //}


    private static Get(): Profile {
        var result = this.Profile;
        result.name = $('#actor-name').val();
        result.status = $('#actor-memo').val();
        result.URL = $('#actor-url').val();
        return result;
    };


    private static Update() {
        var pe = new ProfileEdit();
        pe.Profile = this.Get();
        pe.Profile.visible = true;

        WebRTCService.OwnerSend(pe);
        this.Close();
    };


    private static Delete() {
        var pe = new ProfileEdit();
        pe.Profile = this.Get();
        pe.Profile.visible = false;

        WebRTCService.OwnerSend(pe);
        this.Close();
    }


}