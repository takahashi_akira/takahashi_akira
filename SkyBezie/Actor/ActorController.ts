﻿

class ActorController extends ServiceController {


    /*
     *  コンストラクタ
     */
    constructor() {
        super();
        ActorUIController.Initilize();
        this.Receiver = new ActorReceiver(this);
    };


    //
    public OnOwnerConnection() {
        WebRTCService.OwnerSend(new Sender(SenderType.GetProfileList));
    }


}
