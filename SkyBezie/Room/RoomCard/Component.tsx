﻿/// <reference path="../../Lib/typings/tsd.d.ts" />


//
class RoomParam {
    Name: string;
    PeerID: string;
    CreateTime: any;
}


/*
 *  グループ描画クラス
 */
class RoomCard {


    //
    //  初期化処理
    //
    public static Initilize(roomList: Array<RoomParam>) {

        RoomCard.Create(roomList);
    }


    //
    //  生成処理（描画処理）
    //
    public static Create(roomList: Array<RoomParam>) {

        var element: HTMLElement = document.getElementById('room-list');
        ReactDOM.render(<RoomCardComponent RoomList={roomList}/>, element, () => {
        });
    }

}

interface RoomListProp {
    RoomList: Array<RoomParam>
}


/*
 *  グループ一覧コンポーネント
 */
class RoomCardComponent extends React.Component<RoomListProp, any> {

    render() {


        var roomNodes = this.props.RoomList.map((cur) => {

            var url = Util.CreateLink('../Client/', cur.PeerID, null, false);
            var dispDate = Util.ToDispDate(new Date(cur.CreateTime));

            return (
                <div className='group-panel mdl-cell mdl-cell--4-col mdl-card mdl-shadow--3dp'>
                    {dispDate}
                    <div>
                        <a href={url}>{cur.Name}</a>
                    </div>
                </div >
            );

        });

        return (
            <div className='mdl-grid'>
                {roomNodes}
            </div>
        );
    }

}
