﻿using System.Web.Http;
using System.Web.Http.Results;


namespace SkyBezie
{

    /// <summary>
    /// 作成した部屋を登録します
    /// </summary>
    public class RoomNameServerController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public JsonResult<RoomPushResult> PUT(RoomParam param)
        {
            return Json(RoomInfo.Push(param));
        }
    }

}
