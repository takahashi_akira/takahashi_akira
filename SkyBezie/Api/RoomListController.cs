﻿using System.Linq;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;


namespace SkyBezie
{

    /// <summary>
    /// 作成した部屋を登録します
    /// </summary>
    public class RoomListController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public JsonResult<List<RoomParam>> GET()
        {
            return Json(RoomInfo.GetRoomList().ToList());
        }
    }

}
