﻿using System.Collections.Generic;


namespace SkyBezie
{

    /// <summary>
    /// 
    /// </summary>
    public class RoomPushResult
    {

        /// <summary>
        /// 
        /// </summary>
        public RoomPushResult(bool succeed)
        {
            Succeed = succeed;
        }


        /// <summary>
        /// Room List
        /// </summary>
        public bool Succeed { get; set; }

    }

}