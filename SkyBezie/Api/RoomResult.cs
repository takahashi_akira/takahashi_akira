﻿using System.Collections.Generic;


namespace SkyBezie
{

    /// <summary>
    /// 
    /// </summary>
    public class RoomResult
    {

        /// <summary>
        /// 
        /// </summary>
        public RoomResult()
        {
            List = new List<RoomResult>();
        }


        /// <summary>
        /// Room List
        /// </summary>
        public List<RoomResult> List { get; set; }

    }

}