﻿
interface RoomListCallBack { (rooms: any): void }


class ApiUtil {


    public static RoomPush(name: string, peerid: string) {
        var param: any = {};
        param.Name = name;
        param.PeerID = peerid;
        $.ajax("/api/RoomNameServer", { type: "PUT", data: param }).then(this.ServerPush_CallBack, this.ApiCallError);
    }


    public static GetRoomList(callback: RoomListCallBack) {
        $.ajax("/api/RoomList", { type: "GET" }).then(callback, this.ApiCallError);
    }


    public static ServerPush_CallBack(result) {
        //
    }


    public static ApiCallError(e) {
        //  コールバックのエラー処理
    }


}