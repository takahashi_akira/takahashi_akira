﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace SkyBezie
{

    /// <summary>
    /// 
    /// </summary>
    public class RoomInfo
    {
        /// <summary>
        /// 部屋の最大数
        /// </summary>
        public static int MAX_ROOM_COUNT = 100;


        /// <summary>
        /// 部屋の生存時間
        /// </summary>
        public static int ALIVE_HOUR = 24;


        /// <summary>
        /// 
        /// </summary>
        public static List<RoomParam> _roomList = null;


        /// <summary>
        /// 
        /// </summary>
        public static List<RoomParam> Rooms
        {
            get
            {
                if (_roomList == null)
                    _roomList = new List<RoomParam>();

                return _roomList;
            }
            set
            {
                _roomList = value;
            }
        }


        /// <summary>
        /// 作成した部屋を登録します
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static RoomPushResult Push(RoomParam param)
        {
            param.CreateTime = DateTime.Now;

            Rooms.Insert(0, param);

            if (Rooms.Count > MAX_ROOM_COUNT)
                Rooms = Rooms.Take(MAX_ROOM_COUNT).ToList();

            return new RoomPushResult(true);
        }


        /// <summary>
        /// 部屋情報を取得します
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<RoomParam> GetRoomList()
        {
            var removeTime = DateTime.Now.AddHours(-ALIVE_HOUR);

            foreach (var room in Rooms)
            {
                if (room.CreateTime > removeTime)
                    yield return room;
            }

        }

    }

}